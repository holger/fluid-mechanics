% XeLaTeX can use any Mac OS X font. See the setromanfont command below.
% Input to XeLaTeX is full Unicode, so Unicode characters can be typed directly into the source.

% The next lines tell TeXShop to typeset with xelatex, and to open and save the source with Unicode encoding.

%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode

\documentclass[12pt]{article}
\usepackage[margin=3cm]{geometry}                % See geometry.pdf to learn the layout options. There are lots.
\geometry{letterpaper}                   % ... or a4paper or a5paper or ... 
%\geometry{landscape}                % Activate for for rotated page geometry
%\usepackage[parfill]{parskip}    % Activate to begin paragraphs with an empty line rather than an indent
\usepackage{graphicx}
\usepackage{amssymb}

% Will Robertson's fontspec.sty can be used to simplify font choices.
% To experiment, open /Applications/Font Book to examine the fonts provided on Mac OS X,
% and change "Hoefler Text" to any of these choices.

\usepackage{fontspec,xltxtra,xunicode}
\defaultfontfeatures{Mapping=tex-text}
\setromanfont[Mapping=tex-text]{Hoefler Text}
\setsansfont[Scale=MatchLowercase,Mapping=tex-text]{Gill Sans}
\setmonofont[Scale=MatchLowercase]{Andale Mono}\usepackage{multirow,booktabs}
\usepackage[table]{xcolor}
\usepackage{fullpage}
\usepackage{lastpage}
\usepackage{enumitem}
\usepackage{fancyhdr}
\usepackage{mathrsfs}
\usepackage{wrapfig}
\usepackage{setspace}
\usepackage{calc}
\usepackage{multicol}
\usepackage{cancel}
\usepackage[retainorgcmds]{IEEEtrantools}
\usepackage{sectsty}
%\usepackage[margin=3cm]{geometry}
\newlength{\tabcont}
\setlength{\parindent}{0.0in}
%\setlength{\parskip}{0.05in}


\title{Fluid Mechanics}


% Editar como se necesite para cambiar los títulos
\newcommand\course{Fluid Mechanics}	% <-- nombre del curso
\newcommand\semester{2019}  % <-- semestre
\newcommand\asgnname{2}         % <-- numero o subtítulo de la tarea
\newcommand\yourname{}  % <-- nombre
\newcommand{\vect}[1]{\overline{#1}} % si se quiere cambiar a vector con flecha solo hay que sustituir boldsymbol por vec.
\newcommand{\norm}[1]{\left\lVert#1\right\rVert}	% para denotar la norma euclidiana
%\theoremstyle{definition}
%\newtheorem{defn}{Definición}
%\newtheorem{reg}{Regla}
\newtheorem{exercise}{Exercise}
\pagestyle{fancyplain}
\headheight 36pt
\lhead{\yourname\ \vspace{0.1cm} \\ \course}
\chead{\textbf{\Large Problem Set 1}}
\rhead{Feb. 1, 2019}
\cfoot{Page \thepage \hspace{1pt} of \pageref{LastPage} \vspace{3mm} \\ \footnotesize \textcolor{gray}{MAUCA M1 Fundamental Course -- Université Côte d'Azur}}
\textheight 580pt
\headsep 10pt
\footskip 40pt
\topmargin = 7pt

\sectionfont{\fontsize{14}{15}\selectfont}
\renewcommand{\theenumi}{\alph{enumi}}
\renewcommand\thesection{Exercise \arabic{section}. }

\begin{document}

\section*{}
Solutions in written form are {\bf due before Friday Feb 15, 2019}. Send them latexed or scanned as a PDF by email to oliver.hahn@oca.eu. You are free to work alone or in groups of max. 3 members, clearly indicate all names.  
\vspace{0.7cm}

% Aquí empieza el contenido del documento

%\textbf{NOMENCLATURA ALGEBRAICA} %Baldor, 1983, pp. 14-15%\date{}                                           % Activate to display a given date or no date

\section{Mixing in phase space}

Investigate the evolution of $N$ particles $(x_i,p_i)$ with $i=1\dots N$ in 1+1 dimensional phase space $\mu$ using a numerical simulation. We can think of them having the distribution function $f(x,p,t)=\sum_{i=1}^N \delta_D(x-x_i(t))\,\delta_D(p-p_i(t))$ and we know that $f$ is conserved along the Hamiltonian trajectories, so that we can just evaluate those trajectories. Let us assume in all cases that the Hamiltonian is 
\[
H=\sum^n_{i=0}\left(\frac{p_i^2}{2m}+V(x_i,t)\right).
\]
Start all the particles at initial positions $x_i=0$ and give the particles energies between some $E_0$ and $E_0+\Delta E$ of your choice. How do these particles move in phase space $(x,p)$ over time for the following potentials? Plot their positions in phase space for various times. How much of phase space will be covered over time in the various cases? Try to use at least $N=1000$ particles.
\begin{enumerate}
\item { Harmonic potential:} First, let the particles move in a harmonic potential $V=m x^2$. 
\item { Non-harmonic potential:}
 Now add a non-harmonic perturbation to the potential, i.e. use $V=m(x^2 + \epsilon x^4)$ with some small $\epsilon>0$. 
 \item { Kicked (chaotic) potential:} Instead of the non-harmonic potential, let us use the harmonic potential, but with an additional periodic kick, so that the potential becomes time-dependent and has the form
 \[
 V = mx^2 - K \cos(2\pi x)\,\sum_{n=-\infty}^{\infty}  \delta_D(t-n\,\delta t),
 \]
 where the infinite sum just means that the additional kick occurs periodically at intervals of $\delta t$. Choose a few values of $K$ and $\delta t$, and report what you find.
 \end{enumerate}

{\it Hint: First, write down the equations of motion of the particles, i.e. $\dot{x}_i=\dots,\dot{p}_i=\dots$, then integrate them numerically and plot the points $(x_i,p_i)$ for all particles at a few select times. For the time-integration of Hamiltonian systems, it is best to use symplectic integrators, which preserve phase-space volume. The leap-frog algorithm is the simplest of this kind to advance $(x_i,p_i)$ from $t$ to $t+\Delta t$:
\begin{eqnarray*}
x_i(t+\Delta t/2) &=& x_i(t) + \frac{\Delta t}{2}\,\frac{p_i(t)}{m}\\
p_i(t+\Delta t) &=& p_i(t) + \Delta t\,\,F\left(x_i(t+\Delta t/2), t+\Delta t/2\right)\\
x_i(t+\Delta t) &=& x_i(t+\Delta t/2) + \frac{\Delta t}{2}\,\frac{p_i(t+\Delta t)}{m}.
\end{eqnarray*}
Here $F$ is the force generated by the potential, i.e. $F(x,t)=-m\nabla V(x,t)$.}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\vspace{1cm}
\section{Fermi-Dirac Statistics and White Dwarfs}

When quantum effects are important, the Maxwell-Boltzmann distribution function is not the equilibrium distribution function. In that case, the distribution function depends on quantum mechanical properties of the particle. If the particle is a spin-$\frac{1}{2}$ particle (i.e. a ``fermion'', e.g. a proton, neutron or electron), then {\em Fermi-Dirac statistics} applies and the distribution of energies is given by
\[
f(E) = \frac{1}{\exp\left[\frac{E-\mu}{kT}\right]+1},
\]
where $\mu$ is the so-called {\em chemical potential}. 
It reflects the fact that new particles cannot be added to the system at arbitrary energies.%, since energy states are discrete in quantum mechanics, and each state can at most be occupied by one spin-up and one spin-down particle. A newly added particle must thus be added to an unoccupied state. 
In the limit of very low temperatures, the chemical potential becomes the {\em Fermi energy} $E_F$, i.e. $\mu\to E_F$ as $T\to 0$. This is called the fully-degenerate limit. 
\begin{enumerate}
\item Verify whether quantum effects are important in a gas of electrons in a white dwarf, the core of the Sun, or the interstellar medium in our Milky Way. Find the numbers for the respective densities on the internet and use the simple relation we derived in the lecture to make the point.
\item How does the distribution function $f(E)$ behave in this limit for energies above and below the Fermi energy $E_F$? Can you explain why Fermions in the fully-degenerate case must have such a chemical potential $\mu=E_F$? How does the Maxwell-Boltzmann distribution $f(E)$ behave in the limit $T\to0$? Plot both distributions for a range of temperatures and discuss differences when approaching zero temperature.

\item The Fermi energy for $N$ spin-$\frac{1}{2}$ particles of mass $m$ in a volume $V$ can be calculated to be
\[
E_F = \frac{\hbar^2}{2m}\left(\frac{3\pi^2 N}{V}\right)^{2/3}.
\]

Electrons in white dwarf stars form such a degenerate electron gas due to the high density. Assume a white dwarf star of mass $M_{\rm WD}=0.5 M_{\odot}$ and radius $R_{\rm WD}=0.01 R_{\odot}$. What is its Fermi energy (give the result in units of ${\rm eV}$, i.e. electron volts). Estimate the velocity of the electrons with an energy equal to the Fermi energy in the white dwarf? Do you think that the magnitude of this velocity has any physical implications?
\end{enumerate}

{\it Hint: in order to perform this calculation, you need to calculate the electron number density $n_e=N/V$. Assume that you can convert as $n_e = \rho/(2m_H)$, between mass density and electron density with $m_H$ the mass of a hydrogen atom, assume also that the density is constant with radius.}

\end{document}  