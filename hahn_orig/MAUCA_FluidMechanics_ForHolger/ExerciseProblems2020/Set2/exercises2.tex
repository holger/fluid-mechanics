% XeLaTeX can use any Mac OS X font. See the setromanfont command below.
% Input to XeLaTeX is full Unicode, so Unicode characters can be typed directly into the source.

% The next lines tell TeXShop to typeset with xelatex, and to open and save the source with Unicode encoding.

%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode

\documentclass[12pt]{article}
\usepackage[margin=3cm]{geometry}                % See geometry.pdf to learn the layout options. There are lots.
\geometry{letterpaper}                   % ... or a4paper or a5paper or ... 
%\geometry{landscape}                % Activate for for rotated page geometry
%\usepackage[parfill]{parskip}    % Activate to begin paragraphs with an empty line rather than an indent
\usepackage{graphicx}
\usepackage{amssymb}

% Will Robertson's fontspec.sty can be used to simplify font choices.
% To experiment, open /Applications/Font Book to examine the fonts provided on Mac OS X,
% and change "Hoefler Text" to any of these choices.

\usepackage{fontspec,xltxtra,xunicode}
\defaultfontfeatures{Mapping=tex-text}
\setromanfont[Mapping=tex-text]{Hoefler Text}
\setsansfont[Scale=MatchLowercase,Mapping=tex-text]{Gill Sans}
\setmonofont[Scale=MatchLowercase]{Andale Mono}\usepackage{multirow,booktabs}
\usepackage[table]{xcolor}
\usepackage{fullpage}
\usepackage{lastpage}
\usepackage{enumitem}
\usepackage{fancyhdr}
\usepackage{mathrsfs}
\usepackage{wrapfig}
\usepackage{setspace}
\usepackage{calc}
\usepackage{multicol}
\usepackage{cancel}
\usepackage[retainorgcmds]{IEEEtrantools}
\usepackage{sectsty}
%\usepackage[margin=3cm]{geometry}
\newlength{\tabcont}
\setlength{\parindent}{0.0in}
%\setlength{\parskip}{0.05in}


\title{Fluid Mechanics}


% Editar como se necesite para cambiar los títulos
\newcommand\course{Fluid Mechanics}	% <-- nombre del curso
\newcommand\semester{2019}  % <-- semestre
\newcommand\asgnname{2}         % <-- numero o subtítulo de la tarea
\newcommand\yourname{}  % <-- nombre
\newcommand{\vect}[1]{\overline{#1}} % si se quiere cambiar a vector con flecha solo hay que sustituir boldsymbol por vec.
\newcommand{\norm}[1]{\left\lVert#1\right\rVert}	% para denotar la norma euclidiana
%\theoremstyle{definition}
%\newtheorem{defn}{Definición}
%\newtheorem{reg}{Regla}
\newtheorem{exercise}{Exercise}
\pagestyle{fancyplain}
\headheight 36pt
\lhead{\yourname\ \vspace{0.1cm} \\ \course}
\chead{\textbf{\Large Problem Set 2}}
\rhead{Feb. 12, 2019}
\cfoot{Page \thepage \hspace{1pt} of \pageref{LastPage} \vspace{3mm} \\ \footnotesize \textcolor{gray}{MAUCA M1 Fundamental Course -- Université Côte d'Azur}}
\textheight 580pt
\headsep 10pt
\footskip 40pt
\topmargin = 7pt

\sectionfont{\fontsize{14}{15}\selectfont}
\renewcommand{\theenumi}{\alph{enumi}}
\renewcommand\thesection{Exercise \arabic{section}. }

\begin{document}

\section*{}
Solutions in written form are {\bf due on Wednesday Feb 26, 2019}. Send them latexed or scanned as a PDF by email to oliver.hahn@oca.eu. You are free to work alone or in groups of max. 3 members, clearly indicate all names.  
\vspace{0.7cm}

% Aquí empieza el contenido del documento

%\textbf{NOMENCLATURA ALGEBRAICA} %Baldor, 1983, pp. 14-15%\date{}                                           % Activate to display a given date or no date

\section{The structure of a disk}
\begin{enumerate}
\item Write down the density structure $\rho(z,r)$ at height $z$ above the mid-plane ($z=0$) of a circumstellar disk at a radius $r$. For simplicity let's assume that the disk has constant temperature $T$ (i.e. is isothermal), and is entirely composed of neutral hydrogen. You can assume also that the gravitational field can be approximated by that of the central star of mass $M_\ast$ alone (i.e. ignore the self-gravity of the disk), and that the limit $z\ll r$ holds. Express your solution in terms of the mid-plane density $\rho_0(r):=\rho(z=0,r)$, and in terms of a length scale that is called the ``scale height'' of the disk
\[
h := \frac{\sqrt{kT}}{\Omega_K(r)},
\]
where $\Omega_K$ is the Keplerian angular frequency of the orbit. Show that $\sqrt{kT}$ is just the sound speed of the isothermal hydrogen gas so that $h=c_s/\Omega_K$.

\item A ``thin disk'' is a disk for which $h/r \ll 1$. What does this thin disk condition imply for the ratio of velocities $c_s/v_K$, where $v_K$ is the Keplerian orbital velocity? What does this mean physically?

\item Let us now exchange our circumstellar hydrogen gas disk by a very simplified disk galaxy, entirely composed of many stars of mass $m_\ast$. Discuss that the vertical velocity dispersion of stars $\sigma_z = \sqrt{\langle v_z^2 \rangle-\langle v_z \rangle^2}$ plays a role analogously to the sound speed $c_s$ in this case, by revisiting our derivation of the Euler equation from kinetic theory, but now thinking of collisionless stars rather than colliding atoms.
\end{enumerate}

{\it Hint: for a.: Find first the gravitational acceleration $g_z$ due to the star in the $z$-direction, then take the limit $z\ll r$, then relate this acceleration to the Keplerian angular frequency at the same radius.}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\vspace{1cm}


\section{The Lane-Emden equation}
The condition for hydrostatic equilibrium can be written as the dimensionless Lane-Emden equation
\begin{equation}
\frac{1}{\xi^2}\frac{d}{d \xi}\left( \xi^2 \frac{ d\,D_n}{d \xi}\right) = -\left(D_n\right)^n
\end{equation}
where $D_n$ represents a dimensionless density for a fixed adiabatic index $n$. This is a simple model for a star. The boundary $\xi_1$ is taken as the point where the density vanishes $D_n(\xi_1)=0$. The inner boundary condition must
be 
\begin{equation}
\frac{d D_n}{d \xi} = 0\qquad \textrm{at }\xi=0.
\end{equation}
and one also imposes the normalisation $D_n(0)=1$.
\begin{enumerate}
\item Derive the Lane-Emden equation from the condition for hydrostatic equilibrium in spherical symmetry
\item Show that the solution for $n=0$ is \[ D_0(\xi) = 1-\frac{\xi^2}{6}. \]
\item Using a numerical integration algorithm, such as Runge–Kutta, compute the density profile $D_n(\xi)$ for the $n = 1.5$ and $n = 3$ polytropes. Make sure to properly include the boundary conditions.
\item How do these numerical solutions compare to the only other known analytic solutions known for $n=1$ and $n=5$? \[ D_1(\xi) = \frac{\sin \xi}{\xi},\qquad D_5(\xi) = \left( 1+\frac{\xi^2}{3}\right)^{-1/2}\] Plot all solutions in one figure and discuss.
\end{enumerate}

{\it Hints: Show first that hydrostatic equilibrium implies
\[\left(\frac{n+1}{n}\right) \frac{K}{r^2} \frac{d}{dr}\left( r^2 \rho^\frac{1-n}{n} \frac{d\rho}{dr} \right) = -4\pi G\rho\]
for an adiabatic equation of state with $\gamma=(n+1)/n$. Then show that both the density and the radial coordinate can be written as dimensionless quantities by using
\[\rho(r)=\rho_c \left(D_n(r)\right)^n \qquad\textrm{and}\qquad r = \lambda_n \xi, \] where $\rho_c$ and $\lambda_n$ are constants.
}


%Write down an expression for the hydrostatic variation of gas density ρ with height z above the midplane of a circumstellar disk at radius r. As in (a), assume constant T and μ. Take the gravitational field to be that from the star alone (ignore the self-gravity of the disk). Work in the limit that z ≪ r. Express in terms of the density at the midplane ρ0 and the density scale height h ≡ (kT/μ)1/2Ω−1, where Ω is the Keplerian orbital angular frequency.
%
%
%The center of the Milky Way harbors a black hole of mass $M\approx10^6\,M_\odot$. Assume that the (infinite reservoir of) ambient gas has a number density $n = 1 \textrm{cm}^{-3}$, a mean particle mass of $m=1.2m_H$, and a temperature $T = 10^7\,K$. How long does it take for the black hole to double its mass due to steady, isothermal Bondi accretion of gas from its ambient medium? Show your derivation in detail and express your answer in units of the Hubble time $t_H \approx 10^{10}$ years, which is roughly the age of the Universe.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\vspace{1cm}
%\section{Emission from an accretion disk}
%The Stefan-Boltzmann law states that a blackbody of temperature $T$ radiates a flux
%\begin{equation}
%j = \sigma_r T^4
%\end{equation}
%of energy per unit time and unit surface area, where $\sigma_r=5.67\times10^{-8}\,{\rm Wm}^{-2}{\rm K}^{-4}$ is the Stefan-Boltzmann constant. In the lecture we derived the dissipated energy for a thin accretion disk per unit time and unit area to be
%\begin{equation}
%\frac{dE}{dt} = - \frac{3GM \dot{M}}{4\pi r^3} \left[1-\left( \frac{r_\ast}{r}\right)^{1/2} \right].
%\end{equation}
%\begin{enumerate}
%\item Assuming that the disk is a black body and keeping in mind that it has two sides, what is the temperature structure of such a disk?
%\item Using Wien's displacement law, that states that for a black body of temperature $T$, the spectrum peaks at the wavelength \[\lambda_{\rm max} = \frac{b}{T}\qquad\textrm{with}\quad b=2.90\times10^{-3}{\rm m K},\] derive the typical wavelength at each radius of the disk.
%\item For a black hole with mass $M=4M_\odot$ and an accretion rate of $\dot{M}\approx 10^{-9}M_\odot{\rm yr}^{-1}$, identify the regions of the disk that emit X-ray, ultraviolet, visible and infrared radiation. How much do these regions shift if the accretion rate is lower? Use that the inner radius $r_\ast$ of the disk can be assumed to be roughly the innermost stable orbit of a Schwarzschild black hole, which is at $3R_S$, where $R_S=2GM/c^2$ is the Schwarzschild radius.
%\end{enumerate}
\end{document}  
