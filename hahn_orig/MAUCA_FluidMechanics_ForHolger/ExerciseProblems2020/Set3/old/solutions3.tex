% XeLaTeX can use any Mac OS X font. See the setromanfont command below.
% Input to XeLaTeX is full Unicode, so Unicode characters can be typed directly into the source.

% The next lines tell TeXShop to typeset with xelatex, and to open and save the source with Unicode encoding.

%!TEX TS-program = xelatex
%!TEX encoding = UTF-8 Unicode

\documentclass[12pt]{article}
\usepackage[margin=3cm]{geometry}                % See geometry.pdf to learn the layout options. There are lots.
\geometry{letterpaper}                   % ... or a4paper or a5paper or ... 
%\geometry{landscape}                % Activate for for rotated page geometry
%\usepackage[parfill]{parskip}    % Activate to begin paragraphs with an empty line rather than an indent
\usepackage{graphicx}
\usepackage{amssymb}

% Will Robertson's fontspec.sty can be used to simplify font choices.
% To experiment, open /Applications/Font Book to examine the fonts provided on Mac OS X,
% and change "Hoefler Text" to any of these choices.

\usepackage{fontspec,xltxtra,xunicode}
\defaultfontfeatures{Mapping=tex-text}
\setromanfont[Mapping=tex-text]{Hoefler Text}
\setsansfont[Scale=MatchLowercase,Mapping=tex-text]{Gill Sans}
\setmonofont[Scale=MatchLowercase]{Andale Mono}\usepackage{multirow,booktabs}
\usepackage[table]{xcolor}
\usepackage{fullpage}
\usepackage{lastpage}
\usepackage{enumitem}
\usepackage{fancyhdr}
\usepackage{mathrsfs}
\usepackage{wrapfig}
\usepackage{setspace}
\usepackage{calc}
\usepackage{multicol}
\usepackage{cancel}
\usepackage[retainorgcmds]{IEEEtrantools}
\usepackage{sectsty}
%\usepackage[margin=3cm]{geometry}
\newlength{\tabcont}
\setlength{\parindent}{0.0in}
%\setlength{\parskip}{0.05in}


\title{Fluid Mechanics}


% Editar como se necesite para cambiar los títulos
\newcommand\course{Fluid Mechanics}	% <-- nombre del curso
\newcommand\semester{2020}  % <-- semestre
\newcommand\asgnname{2}         % <-- numero o subtítulo de la tarea
\newcommand\yourname{}  % <-- nombre
\newcommand{\vect}[1]{\overline{#1}} % si se quiere cambiar a vector con flecha solo hay que sustituir boldsymbol por vec.
\newcommand{\norm}[1]{\left\lVert#1\right\rVert}	% para denotar la norma euclidiana
%\theoremstyle{definition}
%\newtheorem{defn}{Definición}
%\newtheorem{reg}{Regla}
\newtheorem{exercise}{Exercise}
\pagestyle{fancyplain}
\headheight 36pt
\lhead{\yourname\ \vspace{0.1cm} \\ \course}
\chead{\textbf{\Large Solutions to Problem Set 3}}
\rhead{Mar. 8, 2020}
\cfoot{Page \thepage \hspace{1pt} of \pageref{LastPage} \vspace{3mm} \\ \footnotesize \textcolor{gray}{MAUCA M1 Fundamental Course -- Université Côte d'Azur}}
\textheight 580pt
\headsep 10pt
\footskip 40pt
\topmargin = 7pt

\sectionfont{\fontsize{14}{15}\selectfont}
\renewcommand{\theenumi}{\alph{enumi}}
\renewcommand\thesection{Exercise \arabic{section}. }

\begin{document}

\section*{}
Solutions in written form are {\bf due before Monday March 16, 2020}. Send them latexed or scanned as a PDF by email to oliver.hahn@oca.eu. You are free to work alone or in groups of max. 3 members, clearly indicate all names.  
\vspace{0.7cm}

% Aquí empieza el contenido del documento

%\textbf{NOMENCLATURA ALGEBRAICA} %Baldor, 1983, pp. 14-15%\date{}                                           % Activate to display a given date or no date
\section{Fragmentation limit}

\begin{enumerate}


\item For a homogeneous cloud of radius $R$, mass $M$ and density $\rho$, the free-fall time
is given by $t_{\rm ff} = \left( \frac{\pi^2 R^3}{8GM} \right)^{1/2}$ while the sound-crossing
time is $t_{\rm c} = R\sqrt{\frac{\bar{m}}{\gamma k T}}$. Equating the two yields 
\[
M_J = \frac{\pi^2 R}{8G}\,\frac{\gamma k T}{\bar{m}},
\]
which is up to numeric factors equal to the mass scale derived using internal and gravitational
energy which was performed in the lecture. This has an important meaning. The speed with which
density perturbations travel within the cloud is given by $c_s$. The instability to gravitational
collapse thus is equivalent to the observation that the time for free-fall collapse is too short 
for different parts of the cloud to communicate via sound waves and thus establish pressure support.

\end{enumerate}

\begin{enumerate}
\setcounter{enumi}{1}
\item From
\[
M_J = \left( \frac{3}{4\pi \rho}\right)^{1/2} \left( \frac{3 k T}{2 G \bar{m}}\right)^{3/2}.
\]
Plugging in $T\propto \rho^{2/3}$ for adiabatic changes of state in a non-relativistic gas,
gives 
\[
M_J^{\rm adiabatic} \propto \rho^{1/2},
\]
while in the isothermal case (cooling) $T={\rm const.}$ and thus
\[
M_J^{\rm isothermal} \propto \rho^{-1/2}.
\]
Thus the adiabatic Jeans mass grows with density, making clouds stable against collapse. 
The isothermal Jeans mass decreases with density and thus isothermal clouds are unstable.
Given any initial inhomogeneities in the density of the cloud, after collapse has begun, 
sections of cloud will independently satisfy the Jeans mass limit and begin to collapse
locally, producing smaller features within the original cloud. This cascading collapse
will lead to the formation of large numbers of smaller objects.
\end{enumerate}

If the cloud remains in thermal equilibrium at a temperature $T$, it will radiate at 
a maximal fraction $f$, with $0<f<1$, of the rate of a black body of the same temperature. 
The cloud will switch from isothermal to adiabatic contraction (and thus 
$T\neq {\rm const.}$) as soon as it reaches this maximum cooling rate.

\begin{enumerate}
\setcounter{enumi}{2}
\item A simple estimate for the rate at which a free-falling cloud has to radiate its energy
is given by the ratio of the total gravitational energy to its free-fall time. For a 
homogeneous cloud of radius $R$, mass $M$ and density $\rho$, the free-fall time
is given by $t_{\rm ff} = \left( \frac{\pi^2 R^3}{8GM} \right)^{1/2}$, thus the
average cooling rate is
\begin{eqnarray*}
A & \simeq & \frac{E_{\rm grav}}{t_{\rm ff}} = \frac{3}{5}\frac{GM^2}{R}\,\left( \frac{8GM}{\pi^2 R^3} \right)^{1/2}\\
 & = & \frac{6\sqrt{2}}{5\pi} \frac{G^{\frac{3}{2}} M^{\frac{5}{2}}}{R^{\frac{5}{2}}}.
\end{eqnarray*}
\end{enumerate}

\begin{enumerate}
\setcounter{enumi}{3}
\item The cooling rate due to blackbody radiation with an efficiency $f\in(0,1)$ is
\[
B = f\left(4\pi R^2\right)\left(\sigma T^4\right).
\]
The cloud can cool efficiently and thus contract isothermally while $B\gg A$. Cooling becomes
inefficient and the transition to adiabatic contraction will occur when $B\simeq A$ and thus
\[
M_{lim} = \left[ \frac{50\pi^4}{9} \frac{f^2 R^9 \sigma^2 T^8}{G^3} \right]^{1/5}.
\]
The fragmentation will reach its limit when the Jeans mass $M_J$ is equal to $M_{lim}$ at
which the transition to adiabaticity occurs. Eliminating the radius $R$ using the Jeans mass
then gives the fragmentation limit at masses
\[
M_J \simeq 0.52\frac{k^{9/4} }{\sigma^{1/2}G^{3/2}} \frac{T^{1/4}}{\bar{m}^{9/4}f^{1/2}}.
\]
For a radiative efficiency of $f=0.1$ and a temperature of $T=1000\,{\rm K}$ we find
the fragmentation limit to be at $M\simeq 0.02 {\rm M}_\odot$ in the case of pure hydrogen gas.
Thus, fragmentation in our simplified model leads to stellar masses which are at 
most one order of magnitude below the mass of the sun. For a primordial mixture of hydrogen
and helium ($\bar{m}=0.6 m_H$) we get $M\simeq 0.07 {\rm M}_\odot$. For an enriched gas with 
$\mu\simeq 1.4$ the limit would be $M\simeq 0.01 {\rm M}_\odot$.\\
This result implies that smaller object do not form by gravitational collapse. Thus other 
mechanisms need to be invoked to explain the formation of small planets. The current
paradigm is that planets form in the disks that surround stars after their gravitational
collapse.
\end{enumerate}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\vspace{1cm}
\section{Virial theorem}
This is a simple back-of-the-envelope exercise. They should find that $T$ is a few ${10^7}\,{\rm K}$ and cooling time is $\gtrsim$ Hubble time. Means that such objects and more massive one should have hot gas at virial temperature, while lower mass objects can contain significant amounts of cold gas.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\vspace{1cm}
\section{Bad estimates}
\begin{enumerate}

\item The ionisation fraction is given by Saha's equation
\[
\frac{n({\rm H^+})}{n({\rm H})} \simeq \frac{n_{Qe}}{n_e}\,\exp\left[-E_i/kT\right].
\]
At the centre of the sun $T\simeq1.5\times10^7\,{\rm K}$ so the quantum concentration
becomes $n_{Qe}\simeq10^{32}\,{\rm m}^{-3}$. The electron number density is given
by $n_e = \rho/2m_{\rm H} \simeq 3\times10^{31}\,{\rm m}^{-3}$. Plugging all 
numbers in, we find
\[
\frac{n({\rm H^+})}{n({\rm H})} \simeq 3.3,
\]
so the ionisation fraction is $0.77$ implying that 23 per cent should be neutral.
This seems utterly unrealistic. It is necessary to invoke a different mechanism in
order to account for the observed ionisation of all hydrogen atoms. Remember 
that the kinetic energy density for an ideal gas is $e=\frac{3}{2}kT$, where
$T=\frac{1}{k}\frac{p}{n}$, so $e=\frac{3}{2}p\,n\simeq 7150\,{\rm eV}$ assuming
$p=P_c\simeq2.3\times10^{16}\,{\rm Pa}$ and $n\simeq n_e$. Thus, the kinetic energy
of particles is by far enough to ionize all the atoms, hence {\em pressure ionization} 
dominates over thermal ionization (which is described by Saha's equation)
in the centre of the Sun.

\item For the intergalactic medium with $T=2.7\,{\rm K}$ (the temperature of the 
cosmic microwave background photons) and $\rho=10^{-29}\,{\rm g cm}^{-3}$ 
we find a quantum concentration $n_{Qe}\simeq1.7\times10^2\,{\rm cm}^{-3}$
and an electron number density $n_e = \rho/2m_{\rm H} \simeq 3\times10^{-16}\,{\rm cm}^{-3}$.
Again using Saha's equation, we find that 
\[
\frac{n({\rm H^+})}{n({\rm H})} \simeq 0.
\]
Thus, all the intergalactic medium should be completely neutral in contrast
to observations that show that it is almost completely ionized. The origin
of this discrepancy lies in yet another mechanism for ionization. High energy
photons ($>13.6\,{\rm eV}$) produced in young stars or quasars are also able
to ionize atoms by {\em photoionization}. This is what happened to the IGM:
the Universe remains neutral for roughly 0.5 billion years after recombination.
Then the stars and first quasars forming in the Universe are producing
ionized bubbles that grow and start to overlap so that the IGM is mainly 
re-ionized within another 0.5 billion years.


\end{enumerate}

\end{document}  