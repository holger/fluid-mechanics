\documentclass[12pt,a4]{article}
\usepackage{geometry}                % See geometry.pdf to learn the layout options. There are lots.
%\geometry{letterpaper}                   % ... or a4paper or a5paper or ... 
%\geometry{landscape}                % Activate for for rotated page geometry
%\usepackage[parfill]{parskip}    % Activate to begin paragraphs with an empty line rather than an indent
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{xfrac}

\title{{\small Final Exam, MAUCA M1 Fundamental Course 2017}\\ {\bf Fluid Mechanics for Astrophysicists}}
%\author{The Author}
%\date{}                                           % Activate to display a given date or no date
\date{Monday, June 26, 2017 -- 14h00-16h00}

\begin{document}
\maketitle

\subsection*{Problem 1: Derivation of the hydrodynamic equations}
\vspace{0.25cm}\noindent {\bf (a)} Give a brief summary (max. 1 page in your own words) of the logical steps one makes when going from the kinetic theory description to the hydrodynamic equations. [Hint: don't forget to mention moments, entropy and the Maxwell-Boltzmann distribution.]

\vspace{0.25cm}\noindent {\bf (b)} What is the physical meaning of a term of the form
\[
\frac{{\rm D}f}{{\rm D}t} = \frac{\partial f}{\partial t} + \mathbf{u} \cdot \nabla f\quad,
\]
where $\mathbf{u}$ is the fluid velocity and $f$ is some field $f(\mathbf{x},t)$ varying in space and time? What is the difference between an Eulerian and a Lagrangian fluid description?
\vspace{1cm}

\subsection*{Problem 2: Fluid velocity fields}
Let us consider a fluid with the following velocity field
\[
\mathbf{u}(\mathbf{x}) = \frac{1}{\tau}\left( 
\begin{array}{c}
 - x/2 +y\\
-x - y \\
-z
\end{array}
\right),
\]
where $\tau$ is a parameter with dimensions of time. Compute the symmetric $\Lambda_{ij}$ and anti-symmetric part $\Omega_{ij}$ of the velocity deformation tensor $D_{ij}=\frac{\partial u_i}{\partial x_j}$ of this velocity field and answer the following questions: (1) is this a convergent or divergent flow? (2) does this flow have vorticity? (3) does this flow have shear? Give the respective expressions for this field.

\subsection*{Problem 3: Fluid instabilities}
Consider the interface between two fluids in which the fluid 1 (at the top) has a density $\rho_1=\frac{1}{2}\rho_2$ and a velocity $u_1 = 2u_2$ of that of fluid 2 (which sits at the bottom). Is the interface stable and, if not, which instability do you expect to develop? With what wave number $k$ do you expect the instability to begin?

Hint: use the formula we derived for general fluid interface instabilities in the lecture, eq. (2.147): $\frac{\omega}{k} = \frac{\rho_1 u_1 + \rho_2 u_2}{\rho_1+\rho_2} \pm \left[\frac{g}{k}\frac{\rho_2-\rho_1}{\rho_1+\rho_2} - \frac{\rho_1\rho_2(u_1-u_2)^2}{(\rho_1+\rho_2)^2}\right]^{1/2}$

\vspace{0.7cm}

\subsection*{Problem 4: Hydrostatic Equilibrium in a Galaxy Cluster}
Here we will consider the hydrostatic equilibrium of a cluster of galaxies. We can safely assume that most of the mass of such a system is either in the form of hot gas or dark matter (DM); stars can be neglected. We will also assume that the system is perfectly spherically symmetric.

\vspace{0.5cm}\noindent {\bf (a)} Show that in hydrostatic equilibrium, the Euler equation, together with the ideal gas law, and Poisson's equation for the total mass density $\rho = \rho_{\rm gas} + \rho_{\rm DM}$ yields the simple relation for the total mass enclosed inside radius $r$:
\begin{equation}
M(<r) = - \frac{kTr}{G\mu m_p} \left( \frac{{\rm d}\ln \rho_{\rm gas}}{{\rm d}\ln r} + \frac{{\rm d}\ln T}{{\rm d}\ln r}\right), \nonumber
\end{equation}
where $T$ is the gas temperature, $\mu$ is the mean molecular weight and $m_p$ the proton mass. [Hint: remember that ${\rm d}\ln x = \frac{1}{x} {\rm d}x$ and that Poisson's equation for the potential $\phi(r)$ in spherical symmetry is $\frac{1}{r^2}\frac{\partial}{\partial r}\left(r^2\frac{\partial \phi}{\partial r}\right) = 4\pi G \rho(r)$.]

\vspace{0.5cm}\noindent {\bf (b)} Remember the relation between the random motion of gas particles and the thermodynamic temperature $T$ that we established in the lecture. Assume that instead of microscopic gas particles at temperature $T$, we want to consider a system of many galaxies in the cluster. By assuming that the galaxies have an isotropic dispersion of velocities $\sigma_{\rm gal}(r)$ and follow a density distribution $\rho_{\rm gal}(r)$, rewrite the equation of hydrostatic equilibrium above for galaxies. [Hint: the velocity dispersion is simply $\sigma^2 = \left<\mathbf{v}^2\right> - \left<\mathbf{v}\right>^2$, relate this to the temperature and assume that the velocity dispersion of galaxies is the same as that of gas particles. Why is the latter assumption reasonable?]

\vspace{0.5cm}\noindent {\bf (c)} Assuming that the velocity dispersion of the galaxies is identical to that of the hot gas particles, calculate the expected gas temperature if galaxies move with a velocity dispersion of $\sigma \simeq 1000\,{\rm km}/{\rm s}$. [Hint: for the numerical values, you can use that $k (T/1K) / m_p = 8.3\times 10^{-3}\, {\rm km}^2/{\rm s}^2$].

\vspace{0.5cm}\noindent {\bf (d)} Now let us assume that the galaxies do {\em not} have an isotropic velocity distribution, but that their velocity dispersion in the radial direction, $\sigma_r$, differs from that in the tangential direction, $\sigma_t$. Using the velocity anisotropy parameter, $\beta :=  1- \sigma_t^2/2\sigma_r^2$, how should the modified anisotropic Maxwell-Boltzmann distribution function $f(v_r, v_t)$ for the galaxies look like? [Hint: start with the Maxwell-Boltzmann distribution and find out how to separate the tangential and radial velocities.]

\vspace{0.7cm}
\subsection*{Bonus Problem: Pressureless Lagrangian flow}
In the absence of pressure and external forces, the Lagrangian (i.e. co-moving with the fluid) form of the fluid equations in one dimension takes the simple form
\[
\frac{{\rm D}\rho}{{\rm D} t} = -\rho \frac{\partial u}{\partial x},\quad \frac{{\rm D} u}{{\rm D} t} = 0.
\]
 Let us assume that at some initial time $t=t_0$, the velocity has the form $u(x,t_0) = \sin(k\, x)$ and the density is $\rho(x,t_0) = \rho_0 = {\rm const.}$

\vspace{0.5cm}\noindent {\bf (a)} For a fluid element starting at a position $x_0$ at $t_0$, what is its position at some later time $t>t_0$? [Hint: use here and below that $u(x,t_0) = u(x_0) = \sin(k\, x_0)$]

\vspace{0.5cm}\noindent {\bf (b)} Find the evolution of the density at the location of a fluid element starting at an initial position $x_0$, $\rho(x_0, t)$. [Hint: use that $\frac{\partial u}{\partial x} = \frac{\partial u}{\partial x_0}\left(\frac{\partial x}{\partial x_0}\right)^{-1}$].

\vspace{0.5cm}\noindent {\bf (c)} Show that this density can become infinite. When does this happen for the first time?

\vspace{0.5cm}\noindent {\bf (d)} Sketch very roughly the evolution of the velocity field up to the point the singularity appears. Explain what you expect to change if the system had pressure.


\end{document}  