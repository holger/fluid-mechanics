%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Plasmas and Magnetohydrodynamics}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Charged particles in Astrophysics}
\subsection{Astrophysical Plasmas}
In many cases in astrophysics, we are dealing with ionised atoms. In an ionised system, the constituent particles thus carry charge (the electrons a negative one and the ions a positive one). These charged particles will interact with each other through long-range electromagnetic interactions that can radically change the behaviour of the system. Whether we can treat an ionised gas as an ideal gas, or whether we have to treat it as a plasma thus depends on the relative importance of these long-range electromagnetic interactions.

\subsubsection{Plasma orbit theory}
In a neutral medium (or in an ionised medium where we can neglect electromagnetic fields), the motion of a particle of mass $m$ is given by Newton's law
\begin{equation}
m \frac{d\mathbf{u}}{dt} = \mathbf{F},
\end{equation}
where $\mathbf{F}$ is the gravitational force. A charged particle of charge $q$ will however move according to
\begin{equation}
m\frac{d\mathbf{u}}{dt} = q\left(\mathbf{E}+\frac{\mathbf{u}}{c}\times\mathbf{B}\right) + \mathbf{F}.
\end{equation}
We see that phenomenologically, the gravitational force and the acceleration due to the electric field are identical, but the effect of the magnetic field $\mathbf{B}$ dramatically enters the motion of a particle. To see this, let us assume the particle is moving with a velocity $\mathbf{u}_\perp$ perpendicular to $\mathbf{B}$ and we neglect $\mathbf{E}$ and $\mathbf{F}$. Then 
\begin{equation}
m \frac{d\mathbf{u}_\perp}{dt} = \frac{q}{c} \mathbf{u}_\perp \times \mathbf{B}.
\end{equation}
For simplicity, let $\mathbf{B}=B \mathbf{e}_z$ be directed along the $z$-axis. Then for $\mathbf{u}_\perp = (u_x,u_y,0)$, we find
\begin{equation}
 \frac{d}{dt}\left(\begin{array}{c} u_x \\ u_y \end{array}\right) = \frac{Bq}{mc} \left(\begin{array}{c} u_y \\ -u_x \end{array}\right).
\end{equation}
We see immediately that this is solved for $u_x = U_0 \cos(\omega_c t)$, $u_y = U_0 \sin(\omega_c t)$ with the \keyword{gyrofrequency}
\begin{equation}
\omega_c = \frac{qB}{mc}.
\end{equation}
The particle thus moves in a circle in the plane perpendicular  to $\mathbf{B}$. Motion along the direction $\mathbf{u}_\parallel$ that is parallel to $\mathbf{B}$ will not be affected by the magnetic field. The complete motion of the particle will thus be helical around the direction of the magnetic field with the two components:
\begin{enumerate}
\item circular motion around a moving central point, called the \keyword{guiding centre}, and
\item translatory motion of the guiding centre.
\end{enumerate}
Even for non-uniform magnetic fields, it is usually possible to write the motion in terms of these two components.

In the presence of either an electric field, a gravitational field, or both, the equation of motion is
\begin{equation}
m\frac{d\mathbf{u}}{dt} = \frac{q}{c}\mathbf{u}\times\mathbf{B} + \mathbf{F},
\end{equation}
where we have combined both gravity and electric force into the term $\mathbf{F}$. We can again split the velocity into two components $\mathbf{u}_\parallel$ and $\mathbf{u}_\perp$ parallel and perpendicular to the magnetic field. The parallel component will then again be unaffected by the magnetic field and simply obey
\begin{equation}
m\frac{d\mathbf{u}_\parallel}{dt} = \mathbf{F}_\parallel,
\end{equation}
where $\mathbf{F}_\parallel$ is the part of $\mathbf{F}$ that is parallel to $\mathbf{B}$. The perpendicular velocity $\mathbf{u}_\perp$, we can break up into the motion of the guiding centre $\mathbf{u}_\textrm{gc}$ and the the circular motion $\mathbf{u}_\circlearrowleft$
\begin{equation}
\mathbf{u}_\perp = \mathbf{u}_\circlearrowleft + \mathbf{u}_\textrm{gc}
\end{equation}
The circular component is unaffected by $\mathbf{F}_\perp$ and just follows (as before)
\begin{equation}
\frac{d\mathbf{u}_\circlearrowleft }{dt} = \frac{q}{mc}\mathbf{u}_\circlearrowleft \times\mathbf{B},
\end{equation}
while the guiding centre motion now has to be uniform and thus satisfy
\begin{equation}
0 = \mathbf{F}_\perp + \frac{q}{c}\mathbf{u}_\textrm{gc}\times\mathbf{B}
\end{equation}
in order for the equation of motion to be satisfied. Taking the cross-product with $\mathbf{B}$, we find
\begin{equation}
\mathbf{F}\times\mathbf{B} = \frac{q}{c}\mathbf{B}\times(\mathbf{u}_\textrm{gc}\times B) = \frac{q}{c}B^2 \mathbf{u}_\textrm{gc},
\end{equation}
so that
\begin{equation}
\mathbf{u}_\textrm{gc} = \frac{c}{q}\, \frac{\mathbf{F}_\perp\times\mathbf{B}}{B^2}\qquad\left[=c\, \frac{\mathbf{E}\times\mathbf{B}}{B^2},\quad\textrm{if }\mathbf{F}_\perp=q\mathbf{E}\right].\label{eq:guiding_centre_motion}
\end{equation}

\subsubsection{Gradient Drift}
Let us now consider a magnetic field which is changing in strength. For simplicity let us set $\mathbf{B}=B(y)\mathbf{e}_z$, i.e. it is unidirectional in the $z$-direction, but varying in strength in the $y$-direction. The $y$-component of the Lorentz force is then
\begin{equation}
F_y = -\frac{q}{c}u_x B(y).
\end{equation}
If we measure the $y$-coordinate relative to the guiding centre and assume that the variation of $B(y)$ is small over trajectory of the particle, then to first order
\begin{equation}
B(y) = B_0 + y \frac{d B}{dy}.
\end{equation}
This implies that the Lorentz force is 
\begin{equation}
F_y = -\frac{q}{c}u_x\left[B_0 + y\frac{dB}{dy}\right],
\end{equation}
which we can average over the circular motion (for which $\bar{u}_x=0$), but 
\begin{equation}
\bar{u_xy}=\frac{1}{2}u_\perp r_L,
\end{equation}
where $r_L$ is the gyroradius. Hence
\begin{equation}
\bar{\mathbf{F}} = \mp \frac{q}{2c} u_\perp r_L \boldsymbol{\nabla} B.
\end{equation}

\subsection{Particle Acceleration in Astrophysics}

\subsection{Jets}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Basic Magnetohydrodynamics}

\subsection{The Fundamental Equations}

\subsection{Hydromagnetic Waves}

\subsection{Parker Instability}

\subsection{Angular Momentum Transport}
