#include <boost/multi_array.hpp>
#include <cstdlib> // contains the definition of the type 'size_t' (== unsigned long int)
#include <fstream>
#include <cmath>
#include <iostream>
#include <string>
#include <stdio.h>      /* printf */
#include <math.h>       /* cos */

#define PI 3.14159265

class Vector // class
{
public:
  
  //constructor
  Vector(int n) {
    data_ = new double[n]; 
    n_ = n;
  }

  //destructor
  ~Vector() {
    delete [] data_; 
    
  }
  
  // copy-constructor
  Vector(Vector const& vec) {
    //std::cout << "Vector(Vector const& vec)\n";

    n_ = vec.n_; 
    data_ = new double[n_]; 
    for(int i=0; i<n_; i++) 
      data_[i] = vec.data_[i];
  }

  double& get(int i) { 
    return data_[i]; 
  }

  // operator overloading
  double& operator[](int i) { 
    if(i >= n_ || i < 0) {
      std::cout << "ERROR in double& operator[](int i): " << i << " out of bounds [0," << n_-1 << "]\n";
      exit(1);
    }
    return data_[i]; 
  }
  

  Vector operator*=(int a) {
    for(int i=0; i<n_; i++)
      data_[i] *= a;

    return *this; 
  }

  Vector const& operator=(Vector const& vec) {
    for(int i=0; i<n_; i++) 
      data_[i] = vec.data_[i]; 

    return *this; 
  }

  int size() {
    return n_;
  }
  
private:
  double* data_;
  int n_;
};


//=========================================================================================


void write(std::ofstream& file, double t, Vector& x, Vector& u) {
  file << "#time = "<< t <<std::endl;
  int nx = x.size();
  for(int ix = 0; ix < nx; ++ix){
    file << x[ix] << " " << u[ix] << std::endl; 
  }
  file << std::endl << std::endl;
}


//===========================================================================================

int main() {
    
  int object;  
  std::cout << "Computing a scheme (1) or the errors (2) ? ";
  std::cin >> object;
  std::cout << "Your choice: " << object << std::endl;

  if(object == 1){

    int N = 512;
    Vector x(N);
    Vector un(N);
    Vector un_1(N);
    double xmin = 0;
    double xmax = 2*PI;
    double dx = (xmax - xmin)/(N-2);
    double nt = 1000;
    int snapshot = 10;
    double t = 0.0;
    
    int array;
    std::cout << "Initial profile : rectangle (1) or a sine (2) ? ";
    std::cin >> array; 
    std::cout << "Your choice: " << array << std::endl;
  
    int user;
    std::cout << "Scheme : Lax-Friedrich scheme (1) or Upwind scheme (2) ? ";
    std::cin >> user;
    std::cout << "Your choice: " << user << std::endl;



  
    if (array == 1) {

      for(int ix = 1; ix < N; ix++) {
	x[ix] = xmin + ix * dx;
      }
    
      double size = (xmax-xmin)*0.1;
      double xc = (xmax+xmin)/2;
      double half_size = size*0.5;
      double height = 1.0;
      for(int ix = 0; ix<N; ++ix){
	if(std::abs(x[ix]-xc)<=half_size){
	  un[ix]=height;
	}
	else{
	  un[ix]=0.;
	}
      }
    }

  
    if (array == 2){
  
      for(int ix = 1; ix < N; ix++) {
	x[ix] = xmin + ix * dx;
      }
      for(int ix = 1; ix < N-1 ; ++ix) {
	un[ix] = -1/dx*(cos(x[ix+1]) - cos(x[ix]));
      }
      un[0]=un[N-2];
      un[N-1] = un[1];
    }


    //======================================================

    
    // Lax-Friedrich with a flux F(u)
  
    if (user == 1) {
      int choice;
      std::cout << "Equation : Advection equation (1) or Burger equation (2) ? ";
      std::cin >> choice ;
      std::cout << "Your choice: " << choice << std::endl;
   

      std::ofstream file1("Lax_Friedrich_moov.txt");
      file1 << "#title = Lax-Friedrich scheme" << std::endl;
      file1 << "#ymin = " << -1 << std::endl;
      file1 << "#ymax = " << 1 << std::endl;
      file1 << "#  = " << xmax-xmin << std::endl;
      write(file1,0.,x,un);

      
      // linear flux
      
      if(choice == 1){
	std::cout << " V = 1 " << std::endl;
	double alpha = 0.5;
	double V = 1.;
	double dt = 2*alpha*dx/V;
	for(size_t it = 0; it <= nt; ++it){
	  t = it*dt;
	  for(int ix=1; ix<N-1; ++ix) {
	    un_1[ix] = 0.5*(un[ix+1] + un[ix-1])  - alpha*(un[ix+1] - un[ix-1]);
	  }
	  un_1[0] = un_1[N-2];
	  un_1[N-1] = un_1[1];

	
	  if(!(it % snapshot)){
	    write(file1, t, x, un_1);
	  }
	  un = un_1;
	}
	
      }

      // Non linear flux
    
      if(choice == 2){
	Vector f(N);
	Vector Fi_p(N);
	Vector Fi_m(N);
	double alpha = 0.5;
	double dt = 2*alpha*dx;

	std::ofstream file2("burger_moov.txt");
	file2 << "#title = Burger equation" << std::endl;
	file2 << "#ymin = " << -1. <<std::endl;
	file2 << "#ymax = " << 1. <<std::endl;
	file2 << "# = " << xmax-xmin << std::endl;
	write(file2, 0.,x,un);


	for(size_t it = 0; it <= nt; ++it){
	  t = it*dt;
	  for(int ix = 0; ix < N; ++ix){
	    f[ix] = 0.5*un[ix]*un[ix];
	  }
	
	  for(int ix=1; ix<N-1; ++ix) {
	    Fi_p[ix] = 0.5*(f[ix] + f[ix+1]) - 0.5/alpha * (un[ix+1] - un[ix]);
	    Fi_m[ix] = 0.5*(f[ix-1] + f[ix]) - 0.5/alpha * (un[ix] - un[ix-1]);
	    un_1[ix] = un[ix] - alpha*(Fi_p[ix] - Fi_m[ix]);
	  }
	  un_1[0] = un_1[N-2];
	  un_1[N-1] = un_1[1];

	  if(!(it % snapshot)){
	    write(file2, t, x, un_1);
	  }
	  un = un_1;
	
	}
      }
    }
  

    // upwind scheme ===========================================


    if (user == 2) {
      int user1;
      std::cout << "Moving to the right (1) or to the left (2) ? ";
      std::cin >> user1; 
      std::cout << "Your choice is : " << user1 << std::endl;
      std::cout <<" V = 1 " << std::endl;

      std::ofstream file3("upwind_scheme_moov.txt");
      file3 << "#title = Upwind scheme " << std::endl;
      file3 << "#ymin = " << -1 <<std::endl;
      file3 << "#ymax = " << 1 <<std::endl;
      file3 << "# = " << xmax-xmin << std::endl;
      write(file3, 0.,x,un);
  

      if(user1==1){
	double alpha = 0.5;
	double V = 1.;
	double dt = 2*alpha*dx/V;
	for(size_t it = 0; it <= nt; ++it) {
	  t = it*dt;
	  for(unsigned int ix = 1; ix <N-1; ++ix) {
	    un_1[ix] = un[ix] - alpha*(un[ix] - un[ix-1]);
	  }
	  un_1[0]=un_1[N-2];
	  un_1[N-1] = un_1[1];
	  if(!(it % snapshot)){
	    write(file3, t, x, un_1);
	  }
	  un=un_1;
	}
      }



      if(user1==2){
	double alpha = 0.5;
	double V = 1.;
	double dt = 2*alpha*dx/V;
	for(size_t it = 0; it <= nt; ++it) {
	  t = it*dt;
	  for(unsigned int ix = 1; ix <N-1; ++ix) {
	    un_1[ix] = un[ix] + alpha*(un[ix+1] - un[ix]);
	  }
	  un_1[0] = un_1[N-2];
	  un_1[N-1] = un_1[1];
   
	  if(!(it % snapshot)){
	    write(file3, t, x, un_1);
	  }
	  un=un_1;
	}
      }
    }

  }


  //=================================================================================

  // Errors for the advection problem

  if(object==2){
    std::cout << "The error is based on an initial condition of a sine \n";
    int choice;
    std::cout << "Errors for the Upwind scheme (1) or the Lax-Friedrich scheme (2) ? ";
    std::cin >> choice;
    std::cout << "Your choice : " << choice << std::endl;

    double xmin = 0;
    double xmax = 2*PI;
    double V = 1.;
    


    double n_err = 200;
    Vector Error(n_err);
    Vector cells(n_err);

    for(size_t i = 0; i < n_err; ++i){
      Error[i] = 0;
      cells[i] = 0;

    }
    

    //advection upwind


    if(choice==1){

      for(size_t ie = 1; ie< n_err; ++ie){

	double nx = ie*10;
	double dx = (xmax-xmin)/(nx-2.);
	double dt = dx/V;
	double t = 0.;
	double alpha = (V*dt)/dx;
	double nt = round((xmax-xmin)/dt) ;
	double error = 0;
	Vector x(nx);
	Vector un(nx);
	Vector un_1(nx);
	Vector usin(nx);


	for(size_t ix = 0; ix < nx; ++ix){
	  x[ix] = xmin + ix*dx;
	}

	for(size_t ix = 1; ix < nx-1; ++ix){
	  un[ix] = (-cos(x[ix+1])+cos(x[ix]))*1/dx;
	  un_1[ix]= 0.;
	}
	un[0]=un[nx-2];
	un[nx-1] = un[1];
	usin = un;



	for (size_t it = 0; it<= nt; ++it){
	  t=it*dt;
   
	  for (size_t ix =1 ; ix<nx-1 ; ++ix){
	    un_1[ix] = un[ix] - alpha*(un[ix]-un[ix-1]);
	  }
   
	  un_1[0]=un_1[nx-2];
	  un_1[nx-1]=un_1[1];
    
	  un=un_1;
	}

	for (size_t j = 0; j<nx; ++j){
	  error = error+ (un_1[j]-usin[j])*(un_1[j]-usin[j]);
	}
	double error_ = sqrt(error*dx);
	Error[ie] = error_;
	cells[ie] = 10*ie;

	std::ofstream fileerror("errorU.txt");
  
	for(size_t iplot = 0; iplot < n_err; ++iplot){
	  fileerror << cells[iplot]<< " " << Error[iplot] << std::endl;
	}

      }
    }


    if(choice==2){

      for(size_t ie = 1; ie< n_err; ++ie){

	double nx = ie*10;
	double dx = (xmax-xmin)/(nx-2.);
	double dt = 0.5*dx/V;
	double t = 0;
	double alpha = 0.5*(V*dt)/dx;
	double nt = round((xmax-xmin)/dt) ; 
	double error = 0;
	Vector x(nx);
	Vector un(nx);
	Vector un_1(nx);
	Vector usin(nx);


	for(size_t ix = 0; ix < nx; ++ix){
	  x[ix] = xmin + ix*dx;
	}

	for(size_t ix = 1; ix < nx-1; ++ix){
	  un[ix] = (-cos(x[ix+1])+cos(x[ix]))*1/dx;
	  un_1[ix]= 0.;
	}
	un[0]=un[nx-2];
	un[nx-1] = un[1];
	usin = un;

	//Lax-Friedrich advection


	for (size_t it = 0; it<= nt; ++it){
	  t=it*dt;
   
	  for (size_t ix =1 ; ix<nx-1 ; ++ix){
	    un_1[ix] = 0.5*(un[ix+1] + un[ix-1]) - alpha*(un[ix+1] - un[ix-1]);
	  }
   
	  un_1[0]=un_1[nx-2];
	  un_1[nx-1]=un_1[1];
    
	  un=un_1;
	}

	for (size_t j = 0; j<nx; ++j){
	  error = error + (un_1[j]-usin[j])*(un_1[j]-usin[j]);
	}
	double error_ = sqrt(error*dx);
	Error[ie] = error_;
	cells[ie] = 10*ie;

	std::ofstream fileerror("errorLF.txt");
  
	for(size_t iplot = 0; iplot < n_err; ++iplot){
	  fileerror << cells[iplot]<< " " << Error[iplot] << std::endl;
	}

      }
    }
  }
  
}
