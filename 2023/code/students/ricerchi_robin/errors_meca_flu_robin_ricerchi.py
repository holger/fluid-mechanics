#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 12 14:00:50 2023

@author: gricerchi
"""

import numpy as np
import matplotlib.pyplot as plt

path = '/home/gricerchi/Desktop/meca_flu_codes/'
fileLF = path + 'errorLF.txt'
fileU = path + 'errorU.txt'

erreurLF = np.loadtxt(fileLF,unpack = True)
erreurU = np.loadtxt(fileU,unpack = True)

n = np.linspace(0, 1000, 100)

plt.figure()
plt.plot(np.log10(erreurLF[0]), np.log10(erreurLF[1]), label = 'Error Lax-Friedrich')
plt.plot(np.log10(erreurU[0]), np.log10(erreurU[1]), label = 'Error Upwind')
plt.plot(np.log10(n), np.log10(1/n), label = '1/n')
plt.xlabel('Log(n)', fontsize = 15)
plt.ylabel('Log(Error)', fontsize = 15)
plt.legend()