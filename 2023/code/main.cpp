// g++ -o main main.cpp

#include <iostream>

int main()
{
  std::cout << "Let us initialize our data\n";

  int N = 10;

  double a = 2;
  std::cout << "&a = " << &a << std::endl;
  std::cout << "a = " << a << std::endl;
  
  double* ap = &a;
  std::cout << "ap = " << ap << std::endl;
  std::cout << "*ap = " << *ap << std::endl;
  std::cout << "&ap = " << &ap << std::endl;

  double* u = new double[N];

  u[0] = 1; // equivalent to *u = 1;
  u[1] = 2;
    
  std::cout << "u[0] = " << u[0] << std::endl;
  std::cout << "u[1] = " << u[1] << std::endl;
  std::cout << "*(u+1) = " << *(u+1) << std::endl;
  
  std::cout << "u[11] = " << u[11] << std::endl; // allowed but dangerous !

  for(int i=0; i<N; i++) {
    u[i] = i;
  }

  double* du = new double[N];
  double dx = 1;
  
  for(int i=1; i<N; i++) {
    du[i] = (u[i] - u[i-1])/dx ;
  }

}
