// g++ -o main main.cpp

#include <iostream>

class Vector
{
public:

  //constructor
  Vector(int n) {
    data_ = new double[n];
    n_ = n;
  }

  // copy-constructor
  Vector(Vector const& vec) {
    std::cout << "Vector(Vector const& vec)\n";

    n_ = vec.n_;
    data_ = new double[n_];
    for(int i=0; i<n_; i++)
      data_[i] = vec.data_[i];
  }

  double& get(int i) {
    return data_[i];
  }

  // operator overloading
  double& operator[](int i) {
    if(i >= n_ || i < 0) {
      std::cout << "ERROR in double& operator[](int i): " << i << " out of bounds [0," << n_-1 << "]\n";
      exit(1);
    }
    return data_[i];
  }

  Vector operator*=(int a) {
    for(int i=0; i<n_; i++)
      data_[i] *= a;

    return *this;
  }

  Vector const& operator=(Vector const& vec) {
    for(int i=0; i<n_; i++)
      data_[i] = vec.data_[i];

    return *this; // this is the adresse of the object
  }

  int size() {
    return n_;
  }
  
private:
  double* data_; // member
  int n_;
};

int main()
{
  std::cout << "Let us think about object-oriented programming\n";

  int N = 10;

  std::cout << "N = " << N << std::endl;
  
  Vector vector(N);

  // initialization
  for(int i=0; i<vector.size(); i++) {
    vector[i] = i;
  }

  // writing elements to terminal
  for(int i=0; i<vector.size(); i++) {
    std::cout << "i: " << i << "\t" << vector[i] << std::endl;
  }

  Vector vector2(N);

  vector2 = vector;

  std::cout << "vector = \n";
  // writing elements to terminal
  for(int i=0; i<vector.size(); i++) {
    std::cout << "i: " << i << "\t" << vector[i] << std::endl;
  }

  vector[0] = 11;
  
  std::cout << "vector2 = \n";
  // writing elements to terminal
  for(int i=0; i<vector2.size(); i++) {
    std::cout << "i: " << i << "\t" << vector2[i] << std::endl;
  }

  Vector vector3(N);
  
  vector3 = vector2 = vector;

  std::cout << "vector3 = \n";
  // writing elements to terminal
  for(int i=0; i<vector3.size(); i++) {
    std::cout << "i: " << i << "\t" << vector3[i] << std::endl;
  }

}
