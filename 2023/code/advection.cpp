// g++ -o advection advectoin.cpp

#include <iostream>
#include <fstream>
#include <cmath>

class Vector
{
public:

  //constructor
  Vector(int n) {
    data_ = new double[n];
    n_ = n;
  }

  //destructor
  Vector() {
    delete [] data_;
  }

  // copy-constructor
  Vector(Vector const& vec) {
    std::cout << "Vector(Vector const& vec)\n";

    n_ = vec.n_;
    data_ = new double[n_];
    for(int i=0; i<n_; i++)
      data_[i] = vec.data_[i];
  }

  double& get(int i) {
    return data_[i];
  }

  // operator overloading
  double& operator[](int i) {
    if(i >= n_ || i < 0) {
      std::cout << "ERROR in double& operator[](int i): " << i << " out of bounds [0," << n_-1 << "]\n";
      exit(1);
    }
    return data_[i];
  }

  Vector operator*=(int a) {
    for(int i=0; i<n_; i++)
      data_[i] *= a;

    return *this;
  }

  Vector const& operator=(Vector const& vec) {
    for(int i=0; i<n_; i++)
      data_[i] = vec.data_[i];

    return *this; // this is the adresse of the object
  }

  int size() {
    return n_;
  }
  
private:
  double* data_; // member
  int n_;
};

void boundaryCondition(Vector & u) {
  u[0] = u[u.size()-2];
  u[u.size()-1] = u[1];
}

int main()
{
  std::cout << "advection test starts\n";

  // number of grid cells
  int n = 20;

  // advection speed
  double a = 1;

  // parameter
  double lambda = 2*M_PI;
  double dx = lambda/(n-2);
  double L = n*dx;

  // cfl number
  double cfl = 0.1;
  double dt = cfl*dx/a;

  // number of time steps
  int nt = lambda/a/dt;

  std::cout << "dt = " << dt << std::endl;
  std::cout << "nt = " << nt << std::endl;

  // data vectors
  Vector u0(n);
  Vector u1(n);
  
  // initialization
  for(int i=1;i<u0.size()-1;i++) {
    u0[i] = (-cos(2*M_PI/lambda*(i+1)*dx) + cos(2*M_PI/lambda*i*dx))/dx;
  }

  boundaryCondition(u0);
  
  std::ofstream out("u0.txt");
  for(int i=0;i<u0.size();i++) {
    out << i << "\t" << (i+0.5)*dx << "\t" << u0[i] << std::endl;
  }
  out.close();

  // upwind scheme

  for(int step=0;step<nt;step++) {

    for(int i=1;i<u0.size()-1;i++) 
      u1[i] = u0[i] + a*dt/dx*(u0[i-1] - u0[i]);

    boundaryCondition(u1);
    u0 = u1;
  }

  out.open("u1.txt");
  for(int i=0;i<u1.size();i++) {
    out << i << "\t" << (i+0.5)*dx << "\t" << u1[i] << std::endl;
  }
  out.close();  
}
