%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Plasmas and Magnetohydrodynamics}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Basic Plasma Physics}
In many cases in astrophysics, we are dealing with ionised atoms. In
an ionised system, the constituent particles thus carry charge (the
electrons a negative one and the ions a positive one). These charged
particles will interact with each other through long-range
electromagnetic interactions that can radically change the behaviour
of the system. The plasma particles show collective behaviour such as
waves. They are coupled over large distances, but this coupling is
weak so that plasma particles can move rather freely. Whether we can
treat an ionised gas as an ideal gas, or whether we have to treat it
as a plasma thus depends on the relative importance of these
long-range electromagnetic interactions. Let us be more precise and
define what is a \keyword{plasma} is by mean of the following three
criteria:

\begin{itemize}
\item A plasma is \keyword{quasi-neutral}, i.e. electric charges are
  shielded so that influence only the close neighboorhood.
\item The kinetic energy of the particles is much larger than the
  potential energy from electromagnetic interactions
\item Plasma has a \keyword{collective behaviour}. Particles are
  (weakly) coupled over long distances.
\end{itemize}

Let us first discuss the point of quasi-neutrallity in detail.

\subsection{Debye shielding}
An interesting phenomenon occurs for the electric field in a plasma
which is not present for gravity. Imagine an ionised gaz with a number
density $n_i$ of positively charged particles and $n_e$ of negatively
charged particles. We now want to locally introduce an additional
charge $Q$ that perturbes the homogeneous distribution of positiv and
negativ charges. The question is: Over which distance will we feel
this charge $Q$? If there is a characteristic length-scale over which
the effect of $Q$ is shielded then we know that the plasma at larger
distances is more or less neutral. We will call it quasi-neutral.

To adresse this question we need to compute the electrostatic
potential $\phi$ from Poisson's equation
\begin{equation}
\nabla^2 \phi = -4\pi \rho = -4\pi[(n_i-n_e)e - Q \delta(\br)],
\end{equation}
where $\br$ is the position of the test charge $Q$. The plasma will
recover the thermodynamic equilibrium, the electron and ion densities
should follow Boltzmann distributions
\begin{equation}
n_e = \bar{n} \exp\left[\frac{e\phi}{kT_e}\right],\quad\textrm{and}\quad
n_i = \bar{n} \exp\left[-\frac{e\phi}{kT_i}\right].
\end{equation}
Please note, that often electrons and ions have very different
temperatures $T_e$ and $T_i$. This is because electrons are much
lighter and therefore much more mobile. If we substitute this into
Poisson's equation, we find (away from $\br=0$)
\begin{equation}
\nabla^2 \phi = 4\pi \bar{n}e \left(
\exp\left[\frac{e\phi}{kT_e}\right]-\exp\left[-\frac{e\phi}{kT_i}\right]
\right).
\end{equation}
In the case that the potential energy is much smaller than the kinetic
energy, $e\phi/kT_e\ll 1$ and $e\phi/kT_i\ll 1$, we can write this to
first order as
\begin{equation}
\nabla^2 \phi = 4\pi \bar{n}e^2 \left( \frac{1}{kT_e} +
\frac{1}{kT_i}\right)\phi = \lambda_D^{-2}\phi
\end{equation}
with the characteristic length $\lambda_D^{-2}=\lambda_{D_e}^{-2} +
\lambda_{D_i}^{-2}$,
\begin{equation}
  \lambda_{D_{e,i}}=\sqrt{\frac{kT_{e,i}}{4\pi \bar{n} e^2}}
\end{equation}
that is the \keyword{Debye length}. This means that the solution for
the gravitational potential is not $\phi = Q/r$ but takes instead the
form of a \keyword{screened potential}
\begin{equation}
\phi = \frac{Q}{r} \exp\left[-\frac{r}{\lambda_D}\right].
\end{equation}
This means that a charge perturbation from charge neutrality is only
felt to a distance $\sim \lambda_D$.

\subsection{Plasma parameter}
We can derive another important parameter that characterizes the
plasma form the criterion that the kinetic energy must be much larger
than the potential energy, $E_p \ll E_c$. The potential of two charges
in three-dimensional space is
\begin{equation*}
  |\phi| \sim \frac{e^2}{r}\sim \bar{n}^{1/3}e^2
\end{equation*}
where $\bar{n}^{1/3}$ is the typical distance between two charges. The
kinetic energy is $E_c= 3/2 k T\sim T$. The criterion now reads
\begin{equation*}
  \bar{n}^{1/3}e^2 \ll T
\end{equation*}
Let us write this with the Debye length $\lambda_D$
\begin{equation*}
  \Lambda:=\bar{n}\lambda_D^3 \gg 1,
\end{equation*}
where we defined the \keyword{plasma parameter} $\Lambda$. It
estimates the number of particles in a Debye sphere (sphere of Debye
size). We see that it has to be large in a plasma, which means that a
charge is shielded by many particles.

\subsection{Single Particle motion}
A plasma is a many (charged) particle system but it is nevertheless
instructive to understand some typical and idealized trajectories of
individual particles. The motion of a particle of charge $q$ and mass
$m$ is given by Newton's law
\begin{equation}
m\frac{d\bm{u}}{dt} =
q\left(\bm{E}+\frac{\bm{u}}{c}\times\bm{B}\right) +
\bm{F_g}.
\end{equation}
where $\bm{F_g}$ is the gravitational force and $q\bm{E} +
\frac{q}{c}\bm{u}\times\bm{B}$ the \keyword{Lorentz force}. We see
that the gravitational force $\bm{F_g}=m\bm{g}$ and the electric force
$q\bm{E}$ are identical, but the effect of the magnetic field $\bm{B}$
dramatically alters the motion of a particle.

\subsubsection{Gyromotion}

To see this, let us assume the particle is moving with a velocity
$\bm{u}_\perp$ perpendicular to $\bm{B}$ and we neglect
$\bm{E}$ and $\bm{F_g}$. Then
\begin{equation}
  \label{eq:gyromotion}
m \frac{d\bm{u}_\perp}{dt} = \frac{q}{c} \bm{u}_\perp \times \bm{B}.
\end{equation}
For simplicity, let $\bm{B}=B \bm{e}_z$ be directed along the
$z$-axis. Then for $\bm{u}_\perp = (u_x,u_y,0)$, we find
\begin{equation}
 \frac{d}{dt}\left(\begin{array}{c} u_x \\ u_y \end{array}\right) =
 \frac{Bq}{mc} \left(\begin{array}{c} u_y \\ -u_x \end{array}\right).
\end{equation}
We see immediately that this is solved for $u_x = U_0 \cos(\omega_c
t)$, $u_y = U_0 \sin(\omega_c t)$ with the \keyword{gyrofrequency}
\begin{equation}
\omega_c = \frac{qB}{mc}.
\end{equation}
The particle thus moves in a circle in the plane perpendicular to
$\bm{B}$ with a radius
\begin{equation}
r_g = \frac{u_\perp}{\omega_c} = \frac{mc}{q B}u_\perp,
\end{equation}
which is called the \keyword{gyroradius}, or also sometimes called the
\keyword{Larmor radius}. Motion along the direction $\bm{u}_\parallel$
that is parallel to $\bm{B}$ will not be affected by the magnetic
field. The complete motion of the particle will thus be helical around
the direction of the magnetic field with the two components:
\begin{enumerate}
\item circular motion around a moving central point, called the
  \keyword{guiding centre}, and
\item translatory motion of the guiding centre.
\end{enumerate}
Even for non-uniform magnetic fields, it is usually possible to write
the motion in terms of these two components.

\begin{figure}
\begin{center}
\includegraphics[width=0.85\textwidth]{Chapter3/bubble_chamber.jpg}
\end{center}
\caption{\label{fig:bubble_chamber}The tracks of charged particle in a
  so-called ``bubble chamber'' of a superheated liquid with a magnetic
  field applied. This photo was taken of a bubble chamber at SLAC in
  1967. [Photo courtesy SLAC National Accelerator Laboratory]}
\end{figure}

This gyro-motion of charged particles around magnetic field lines can
be used to detect particles in a \keyword{bubble chamber} containing a
superheated liquid (see Figure~\ref{fig:bubble_chamber}).

\subsubsection{$\bm{E}\times\bm{B}$ Drift}
In the presence of either an electric field (or a gravitational field)
the equation of motion is
\begin{equation}
  \label{eq:e_cross_b}
m\frac{d\bm{u}}{dt} = \frac{q}{c}\bm{u}\times\bm{B} + q\bm{E},
\end{equation}
If the electric field was parallel to $\bm{B}$ is would just
accelerate the particle along the magnetic field line. We are thus
interested in the effect of an electric field perpendicular to the
magnetic field.

If we compare (\ref{eq:e_cross_b}) to the equation without electric
field equation (\ref{eq:gyromotion}) we recognize that the character
of the equation has changed. Now we deal with an inhomogeneous
differential equation. From math courses we know that the solution to
such an equation can be written as
\begin{equation}
  \bm{u}=\bm{u}_H + \bm{u}_D
\end{equation}
that is the sum of the solution $\bm{u}_H$ to the homogeneous
equation
\begin{equation*}
m\frac{d\bm{u}_H}{dt} = \frac{q}{c}\bm{u}_H\times\bm{B} 
\end{equation*}
and a particular solution $\bm{u}_D$ to
\begin{equation}
  \label{eq:particular_solution}
0 = \frac{q}{c}\bm{u}_D\times\bm{B} + q\bm{E}.
\end{equation}
The homogeneous solution $\bm{u}_H$ is just the gyromotion. The
modification due to the electric field is entering through the
particular solution in $\bm{u}_D$.  Taking the cross-product of
(\ref{eq:particular_solution}) with $\bm{B}$, we find
\begin{equation}
q\bm{E}\times\bm{B} = \frac{q}{c}\bm{B}\times(\bm{u}_D\times \bm{B}) = \frac{q}{c}B^2 \bm{u}_D,
\end{equation}
so that
\begin{equation}
  \label{eq:e_cross_b_drift}
\bm{u}_D =c\, \frac{\bm{E}\times\bm{B}}{B^2}.
\end{equation}
The particle is performing its gyromotion while it is drifting into
the direction that perpendicular to both $\bm{B}$ and
$\bm{E}$ (see Fig.~\ref{}).

Here, we assumed a force $\bF=q\bE$, but we can write the solution
(\ref{eq:e_cross_b_drift}) for a general force $\bF$

\begin{equation}
  \label{eq:guiding_centre_motion}
  \bm{u}_D =\frac{c}{q}\frac{\bm{F}\times\bm{B}}{B^2}.
\end{equation}

We see that a force that is perpendicular to the magnetic field is
giving rise to a particle drift perpendicular to the magnetic
field. This finding is one reason while humanity is still struggling
to build a fusion reactor. A fusion reactor aims to gain energy at
earth from nuclear fusion that happens in stars. In stars, hydrogen is
confined by self-gravity. On earth, the hot hydrogen plasma needs to
be confined in a different way and one idea is to use magnetic fields
because of the gyro motion of charged particles. However, the observed
drifts make the design of fusion reactors very complicated. Many
decades ago, researchers and politiciens claimed to build soon a
fusion power plant. At the moment, a new attempt is made in Cadarach
(France) where an international collaboration builds a test fusion
reactor, called \keyword{Iter} (International Thermonuclear
Experimental Reactor) that shall for the first time produce more
energy than it consumes. However, it will not produce electric energy
as the conversion from heat to electric currents still poses severe
problems. Imagine a million degree hot plasma that needs to transfer
its heat to a wall in a controlled way. The overall costs for this
test (assembly and maintenance) are now estimated to 22 billion
dollar. Probably, this enormous sum does not include the funding of
many research projects that justify their relevance by their
contribution to the Iter project.

\subsubsection{Gradient Drift}

There are different situations that give rise to a drift. Another
example is an imhomogeneous magnetic field. Let us thus consider a
magnetic field which is changing in strength. You will see in the
exercices that we find the expression for the gradient drift
\begin{equation}
\bm{u}_{\boldsymbol{\nabla} B} = \pm \frac{1}{2} u_\perp r_g
\frac{\bm{B}\times \boldsymbol{\nabla}B}{B^2}.
\end{equation}
It is important to note that the respective signs $\pm$ apply to
negative and positive charges respectively, so that gradients in the
transverse magnetic field lead to electrons and ions drifting in
opposite directions, which gives rise to an \keyword{electric
  current}.

\subsubsection{Magnetic Mirrors}


\begin{figure}
\begin{center}
\includegraphics[width=0.5\textwidth]{Chapter3/magnetic_mirror.pdf}
\end{center}
\caption{\label{fig:magnetic_mirror}A basic magnetic mirror machine:
  the convergence of fields lines at the ends, and the associated
  increase in magnetic field strength leads to a reflection of charged
  particles and thus to a confinement of most particles to the
  interior. Such magnetic mirrors can be used to confine plasma in
  fusion reactors. }
\end{figure}

We have allready stated that confining a plasma is an important
problem. The plasma needs to stay hot so that any contact to device
boundaries is problematic. A simple confinement machine is illustrated
in Fig.~\ref{fig:magnetic_mirror}. Charged particles are bouncing back
and forth inside the widened volume. The reason is a conserved
quantity $\mu$ that is involving the magnetic field strength as
$\mu=T_\perp/B$, $T_\perp$ being the transverse kinetic energy of the
particle. Evidentially, the magnetic field $B$ changes amplitude
inside the machine. It is stronger at the edges and weaker in the
middel. If the magnetic field is changing and the conservered quantity
$\mu$ remains constant the other ingredient $T_\perp$ must
change. This ingredient is, however, bounded by the total kinetic
energy so that the motion is also bounded.

Let us understand the mechanism now in detail. For this, we consider a
magnetic field with a gradient in field strength in the $z$-direction
($\bB = B_r(z)\be_r + B_z(z)\be_z$). When working with a magnetic
field, we always have to make sure that the $\bm{B}$-field is
divergence-free. Working in cylindrical coordinates, this constraint
equation $\boldsymbol{\nabla}\cdot\bm{B}=0$ becomes
\begin{equation}
\frac{1}{r}\frac{\partial}{\partial r}\left(r B_r\right) +
\frac{\partial B_z}{\partial z} = 0.
\end{equation}
We can integrate this equation to find (assuming $B_z$ is constant
with radius $r$)
\begin{equation}
r B_r = - \int_0^r r' \frac{\partial B_z}{\partial z} \,dr' =\simeq
-\frac{1}{2} r^2 \frac{\partial B_z}{\partial
  z}\quad\Leftrightarrow\quad B_r = -\frac{1}{2}r\frac{\partial
  B_z}{\partial z}.
\end{equation}
The Lorentz-force in cylindrical coordinates is given by
\begin{equation}
\left(\begin{array}{c} F_r \\ F_\phi \\ F_z \end{array}\right) =
\frac{q}{c} \left(\begin{array}{c} u_\phi B_z - u_z B_\phi \\ u_z B_r
  - u_r B_z \\ u_r B_\phi - u_\phi B_r \end{array}\right),
\end{equation}
so that in our case $F_z = -\frac{q}{c}u_\phi B_r$ since we have set
$B_\phi=0$ and we can write $u_\phi = \mp u_\perp$. We thus get
\begin{equation}
F_z = \mp \frac{q}{2c} u_\perp r_g \frac{\partial B_z}{\partial z} = -
\mu \frac{\partial B_z}{\partial z},
\end{equation}
where we have defined the \keyword{magnetic moment}
\begin{equation}
  \label{eq:magnetic_moment}
\mu = \pm \frac{q}{2c} u_\perp r_g = \frac{\frac{1}{2}m u_\perp^2}{B}.
\end{equation}
Alternatively one can express the magnetic moment by eliminating
$u_\perp$ instead of $r_g$ as
\begin{equation}
\mu = \pm \frac{\omega_c}{2\pi} \frac{q}{c} \,\pi r_g^2.
\end{equation}

The $z$-component of the particle motion is given by 
\begin{equation}
m\frac{d u_\parallel}{d t} = F_z = -\mu \frac{\partial B_z}{\partial z},
\end{equation}
and the rate of change of the longitudinal kinetic energy of a
particle is given by
\begin{equation}
  \frac{d}{dt}\left(\frac{1}{2}m u_\parallel^2\right) = u_\parallel m \frac{d u_\parallel}{dt}.
\end{equation}
We can use that along a particle trajectory in $z$- direction one has
\begin{equation}
  \frac{d B_z(z(t))}{dt} = \frac{\partial B_z}{\partial z}\frac{dz}{dt} = \frac{\partial B_z}{\partial z}u_\parallel
\end{equation}
We therefore get for the longitudinal kinetic energy
\begin{equation}
  \label{eq:mirror_long_kin}
  \frac{d}{dt}\left(\frac{1}{2}m u_\parallel^2\right) = -\mu \frac{d B}{dt},
\end{equation}

%  = -\mu \frac{dB}{dt}.

Since obviously the sum of the transverse and longitudinal kinetic energies cannot change, we must also have
\begin{equation}
\frac{d}{dt}\left(\frac{1}{2}m u_\parallel^2 + \frac{1}{2}m u_\perp^2\right) = 0,
\end{equation}
now we use (\ref{eq:mirror_long_kin}) and (\ref{eq:magnetic_moment})
to get
\begin{align}
  -\mu \frac{dB}{dt} + \frac{d(B\mu)}{dt} &= 0\\
\Rightarrow  -\mu \frac{dB}{dt} + B\frac{d\mu}{dt} + \mu\frac{dB}{dt} &= 0\\
\Rightarrow  \frac{d\mu}{dt} = 0.
\end{align}
which means that the magnetic moment $\mu$ is a conserved quantity. This leads to an interesting effect called a \keyword{magnetic mirror}: The transverse kinetic energy $\frac{1}{2}mu_\perp^2$ has to increase when the particle moves into a region of stronger magnetic field $B$. However, it can never exceed the total kinetic energy, so that there will be a limiting $B$ into which the particle cannot penetrate. Instead it will be reflected back!

\section{Many particles in a plasma}
\subsection{The Vlasov-Maxwell equations}
When we consider many particles in a plasma, we can apply the same
reasoning as for uncharged particles. As when we derived the Boltzmann
equation, we can express the phase space density of electrons and ions
as $f_e(\bm{x},\bm{p},t)$ and $f_i(\bm{x},\bm{p},t)$, respectively. In
the limit of no binary collisions, we then obtain the so called
\keyword{Vlasov equation} of the form
\begin{equation}
\frac{\partial f_k}{\partial t} +
\frac{\bm{p}}{m_k}\cdot\boldsymbol{\nabla_x}f_k+q_k\left(\bm{E}+\frac{\bm{p}}{m_kc}\times\bm{B}\right)\cdot\boldsymbol{\nabla_p}f_k=0\qquad
k\in\left\{ i,e\right\},
\end{equation}
where the subscript $k$ is $e$ for electrons and $i$ for ions, with
the respective charges $q_i=+e$, $q_e=-e$ (if the ions are singly
ionized) and masses $m_k$. Again, we can define the respective number
densities by integrating over momentum space, i.e.
\begin{equation}
n_e(\bm{x},t) = \int d^3p\,
f_e(\bm{x},\bm{p},t),\quad\textrm{and}\quad n_i(\bm{x},t) = \int
d^3p\, f_i(\bm{x},\bm{p},t),
\end{equation}
and take the first moment to obtain the respective velocity fields
\begin{equation}
\bm{v}_e(\bm{x},t) = \frac{1}{m_e n_e}\int d^3p\,
\bm{p}\,f_e(\bm{x},\bm{p},t),\quad\textrm{and}\quad \bm{v}_i(\bm{x},t)
= \frac{1}{m_i n_i}\int d^3p\,\bm{p}\, f_i(\bm{x},\bm{p},t).
\end{equation}
If we neglect any external magnetic or electric field (otherwise they
have to be added separately), the charge density $(n_i-n_e)e$ and the
current density $(n_i\bm{v}_i-n_e\bm{v}_e)e$ will self-consistently
generate the electric and magnetic fields by virtue of Maxwell's
equations. This means that we have the additional set of equations
\begin{eqnarray}
\boldsymbol{\nabla}\cdot \bm{E} & = &
4\pi(n_i-n_e)e, \label{eq:maxwell1}\\ \boldsymbol{\nabla}\times\bm{E}
& = & - \frac{1}{c}\frac{\partial \bm{B}}{\partial
  t},\label{eq:maxwell2}\\ \boldsymbol{\nabla}\cdot \bm{B} & = &
0,\label{eq:maxwell3}\\ \boldsymbol{\nabla}\times \bm{B} & = &
\frac{4\pi}{c}(n_i\bm{v}_i-n_e\bm{v}_e)e +
\frac{1}{c}\frac{\partial\bm{E}}{\partial t}\label{eq:maxwell4}.
\end{eqnarray}
We stress that the electromagnetic fields are computed from the
averaged densitites $n_e(\bm{x},t)$ and $n_i(\bm{x},t)$, so that no
individual short distance Coulomb forces enter the description. The
latter forces lead to strong two particle interactions that we would
call a collision. The Vlasov description neglects these collisions.

The above equations give the full set of equations for the
collisionless kinetic plasma model, the \keyword{Vlasov-Maxwell
  equations}. It is an accurate descriptoin of dilute collisionless
plasmas, i.e. for plasmas in which two-particle interactions are
negligible compared to the long-range collective forces. 

\subsection{The two-fluid model}
If we take moments of the Vlasov-Maxwell system and assume a local
thermodynamic equilibrium, we will, just as in the hydrodynamic case,
end up with a set of equations which describe the evolution of the
moments of the distribution function. Since we have separate phase
space distribution functions for electrons and ions, we will naturally
end up with the equations for two fluids, one representing the
electrons and one the ions. This is the \keyword{two-fluid model}. We
will not repeat in depth the derivation of the moment equations as it
is completely analogous to the hydrodynamic case. We end up with the
following set of equations
\begin{eqnarray}
 \label{eq:twofluid_cont}
\frac{\partial n_k}{\partial t} +
\boldsymbol{\nabla}\cdot(n_k\bm{v}_k) & = & 0\\
\label{eq:twofluid_momentum}
m_k n_k \left[
  \frac{\partial \bm{v}_k}{\partial t} +
  (\bm{v}_k\cdot\boldsymbol{\nabla})\bm{v}_k \right] & = &
-\boldsymbol{\nabla}P_k + q_k n_k\left(\bm{E} +
\frac{\bm{v}_k}{c}\times\bm{B}\right),
\end{eqnarray}
which represent the continuity and Euler equation which are fulfilled
separately by electrons and ions. We note that naturally we also have
a distinct pressure $P_k$ for the two types of particles. Three cases
of equations of state are most important. These are
\begin{eqnarray}
P_k = 0 &\quad& \textrm{for \keyword{cold collisionless plasmas},} \\ 
P_k = k n_k T_k &\quad& \textrm{for constant but distinct temperatures,}\\
P_k = K n_k^\gamma &\quad&\textrm{for adiabatic processes.}
\end{eqnarray}
Naturally, to have a closed set of equations, one needs to supplement
these equations with Maxwell's equations
(\ref{eq:maxwell1}-\ref{eq:maxwell4}).

\subsubsection{Electromagnetic oscillations in cold plasmas}
We saw in Section~\ref{sec:acoustic_waves} that in a hydrodynamic
fluid, small perturbations lead to acoustic waves. This was due to the
pressure force propagating the perturbation at the sound speed through
the fluid. We will now show that a plasma can sustain oscillations
even in the absence of a thermodynamic pressure, i.e. we set $P_k=0$
and thus consider cold collisionless plasmas. Analogous to our
discussion of acoustic waves, we can linearise the Euler equation of
the electrons for velocity perturbations $\bm{v}_1$ around a mean
electron density $n_0$. In addition, we will also have to perturb
Maxwell's equations so that $\bm{E}=\bm{E}_0 +\bm{E}_1$, where
$\bm{E_0}=0$ and $\bm{B}=\bm{B}_0 +\bm{B}_1$, where $\bm{B_0}=0$,
i.e. we set the unperturbed fields to zero. We then have at first
order for the Euler equation (\ref{eq:twofluid_momentum})
\begin{equation}
m_e n_0 \frac{\partial \bm{v}_1}{\partial t} = -en_0 \bm{E}_1.
\end{equation}
The Lorentz force $\bm{v}_1\times\bm{B}_1$ is a second order term and
can thus be neglected at linear order in the perturbations. From
Maxwell's equations we get in addition
\begin{eqnarray}
\frac{1}{c}\frac{\partial \bm{E}_1}{\partial t} &=&
\boldsymbol{\nabla}\times\bm{B}_1 +
\frac{4\pi}{c}n_0e\bm{v}_1\\
\label{eq:maxwell_pert_b}
\frac{1}{c}\frac{\partial
  \bm{B}_1}{\partial t} &=& -\boldsymbol{\nabla}\times\bm{E}_1.
\end{eqnarray}
We can now perform directly a Fourier transform in the time domain, so
that we can replace all $\partial/\partial t$ with multiplications by
$-i\omega$. We then get for the linearised Euler equation
\begin{equation}
\bm{v}_1 = \frac{e}{i\omega m_e}\bm{E}_1
\end{equation}
We can then directly insert this into the remaining equations to
obtain the following relation between the perturbations to the
magnetic and electric fields
\begin{equation}
  \label{eq:plasma_wave_1}
\boldsymbol{\nabla}\times \bm{B}_1 = -\frac{i\omega}{c} \epsilon
\bm{E}_1,
\end{equation}
where $\epsilon$ is the \keyword{dielectric constant} of the plasma
and is defined as
\begin{equation}
\epsilon := 1-\frac{\omega_p^2}{\omega^2},\quad\textrm{with}\quad\omega_p^2 := \frac{4\pi n_0 e^2}{m_e}.
\end{equation}
The frequency $\omega_p$ is called the \keyword{plasma
  frequency}. Equation (\ref{eq:plasma_wave_1}) is coupling the
perturbations of the electric and magnetic field. In order to derive
an equation (it will be a wave equation) for one of the fields we take
its time derivative and use (\ref{eq:maxwell_pert_b}) for the
appearing term $\partial_t \bm{B}_1$:
\begin{align}
  \frac{\partial}{\partial t}\boldsymbol{\nabla}\times \bm{B}_1 &=
  \frac{\partial}{\partial t} \left(-\frac{i\omega}{c} \epsilon
  \bm{E}_1\right)\\ \Rightarrow
  \boldsymbol{\nabla}\times\frac{\partial \bm{B}_1}{\partial t} &=
  -\frac{i\omega}{c} \epsilon \frac{\partial \bm{E}_1}{\partial t}
  \\ c\boldsymbol{\nabla}\times(\boldsymbol{\nabla}\times\bm{E}_1)
  &= \frac{\omega^2}{c}\epsilon\bm{E}_1.
\end{align}
We can now perform a Fourier transform in space (we insert all
perturbations of the form $\exp(i(\bm{k}\cdot \bx - \omega\,t)$) and
we obtain
\begin{align}
c\bm{k}\times(\bm{k}\times\bm{E}_1) &=
-\frac{\omega^2}{c}\epsilon\bm{E}_1\\
\Rightarrow c^2\bm{k}\times(\bm{k}\times\bm{E}_1) &=
-\omega^2\left(1-\frac{\omega_p^2}{\omega^2}\right)\bm{E}_1\\
\Rightarrow c^2\bm{k}\times(\bm{k}\times\bm{E}_1) &=(
\omega_p^2- \omega^2 )\bm{E}_1
\end{align}
This is a wave equation which describes two distinct waves. If we
consider a single mode of this wave, which we set along the $z$-axis
($\bm{k} = k\, \be_z)$, we can write the wave equation for this single
mode as
\begin{equation}
\left( \begin{array}{ccc}
\omega^2-\omega_p^2-k^2c^2 & 0 & 0 \\
0 & \omega^2-\omega_p^2-k^2c^2 & 0 \\
0 & 0 & \omega^2-\omega_p^2
\end{array}\right) \left(\begin{array}{ccc}E_{1x}\\E_{1y}\\E_{1z}\end{array}\right) = \left(\begin{array}{ccc}0\\0\\0\end{array}\right).
\end{equation}

There are two fundamentally different waves:
\begin{enumerate}
\item {\bf plasma oscillations}: One solution is $E_{1x}=E_{1y}=0$,
  with $\omega^2=\omega_p^2$. This corresponds to non-propagating
  longitudinal oscillations with a frequency equal to the plasma
  frequency. You can think of having displaced a little bit the
  electrons with respect to the ions. The induced electric field
  drives back the electrons that will overshoot and thus perform
  harmonic oscillations with frequence $\omega_p$.
\item {\bf electromagnetic waves:} The other solution is $E_{1z}=0$,
  with $\omega^2=\omega_p^2+k^2 c^2$. These are propagating
  transversal waves with a phase velocity $v_{\rm ph} = \omega/k =
  c/\sqrt{\epsilon}$ and group velocity $v_{\rm gr} = d\omega/dk =
  c\sqrt{\epsilon}$. If $\omega \gg \omega_p$, then we obtain the
  limit $\omega^2=k^2 c^2$ which is the dispersion relation for
  electromagnetic waves in vacuum. This means that only for low enough
  $\omega>\omega_p$, the plasma will affect the propagation of the
  electromagnetic wave. In the case $\omega<\omega_p$,
  $\sqrt{\epsilon}$ becomes imaginary which means that electromagnetic
  waves with $\omega<\omega_p$ cannot pass through the plasma and are
  instead reflected.
\end{enumerate}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Basic Magnetohydrodynamics}
An important simplification occurs when we consider only length scales much larger than the Debye length and time scales much larger than the inverse of the plasma frequency. In that regime, it is allowed to assume that electron and ion densities and velocities follow each other. In this case, we can treat the entire plasma as a single fluid. This allows to combine the equations of the two-fluid system into the equations of \keyword{ideal magnetohydrodynamics}.

\subsection{The Fundamental Equations}
The fundamental, {\em ideal}, MHD equations are very similar to the equations of ideal hydrodynamics and take the form
\begin{eqnarray}
\frac{\partial \rho}{\partial t} + \boldsymbol{\nabla}\cdot(\rho\bm{v}) & = & 0 \\
\frac{\partial v_i}{\partial t} + v_j \frac{\partial v_i}{\partial x_j} & = & F_i - \frac{1}{\rho}\frac{\partial}{\partial x_j}\left(P_{ij} + M_{ij}\right) \label{eq:MHD_euler} \\ 
\frac{\partial \bm{B}}{\partial t} - \boldsymbol{\nabla}\times\left(\bm{v}\times\bm{B}\right) & = & 0,
\end{eqnarray}
where $P_{ij}$ is the pressure tensor ($P_{ij}=P\delta_{ij}$ if we only have thermodynamic pressure, otherwise it also includes viscous terms), and 
\begin{equation}
M_{ij} := \frac{B^2}{8\pi}\delta_{ij} - \frac{B_i B_j}{4\pi}
\end{equation}
is the \keyword{magnetic stress tensor}. We thus gained one additional vector equation, the \keyword{induction equation}, which couples the evolution of the magnetic field to the fluid velocity field. The magnetic field acts back on the fluid velocity through the magnetic stress. We can understand its meaning if we look at $M_{ij}$ for a magnetic field that is uniform in the $z$-direction, i.e. $\bm{B}=B\bm{e}_z$. Then
\begin{equation}
M_{ij} = \frac{B^2}{8\pi} \left(  \begin{array}{ccc} 1 & 0 & 0 \\ 0 & 1 & 0 \\ 0 & 0 & 1 \end{array}\right) - \frac{B^2}{4\pi} \left(  \begin{array}{ccc} 0 & 0 & 0 \\ 0 & 0 & 0 \\ 0 & 0 & 1 \end{array}\right),
\end{equation}
which shows that the magnetic field produces an isotropic pressure in the first term, and a negative pressure  only in the $z$-direction. This negative anisotropic pressure corresponds to a tension of the magnetic field. While the positive isotropic part opposes compression of magnetic fields, the anisotropic second term opposes stretching along magnetic field lines. The isotropic magnetic pressure is
\begin{equation}
P_{\rm mag} = \frac{B^2}{8\pi},
\end{equation}
and it can be directly compared to the thermodynamic pressure of a gas to determine whether magnetic fields are dynamically important in a fluid. This means one can define a dimensionless number, the \keyword{plasma-$\beta$}, which is defined as
\begin{equation}
\beta = \frac{P}{B^2 / 8\pi}.
\end{equation}
In the corona of the sun, one has $\beta\sim 0.01$, 

\subsection{Hydromagnetic Waves}
We remember that the hydrodynamic equations allowed wave solution with a sound speed $c_s^2 = {\partial P}/{\partial \rho}$. These waves will of course still exist also in the MHD case but they will split into two different waves. The tension of magnetic fields lines gives rise to an additional wave. In total, we have the following different waves:

\subsubsection{ Alfv\'en waves} 
These are waves that result from the restoring force due to the tension of the magnetic field lines. They are thus propagating parallel to the magnetic field and have a velocity 
\begin{equation}
\bm{v}_A = \frac{1}{\sqrt{4\pi\rho_0}} \bm{B}_0,
\end{equation}
where $\rho_0$ and $\bm{B}_0$ are the unperturbed mean density and magnetic field in which the waves propagate.

\subsubsection{Slow and fast magnetosonic waves} 
These are the MHD equivalent of sound waves and they come as a slow and a fast wave. If $\theta$ is the angle between the wave vector $\bm{k}$ and the magnetic field $\bm{B}$, the magnetosonic wave velocities are
\begin{equation}
v_{ms}^2 = \frac{1}{2}(c_s^2+v_A^2) \pm \frac{1}{2}\sqrt{(c_s^2+v_A^2)^2-4c_s^2v_A^2\cos^2\theta}.
\end{equation}

\begin{figure}
\begin{center}
\includegraphics[width=0.75\textwidth]{Chapter3/IMG_2311.jpg}\\
\vspace{0.5cm}
\includegraphics[width=0.9\textwidth]{Chapter3/s2-1280.jpg}
\end{center}
\caption{\label{fig:mag_buoyancy}{\bf Top:} Magnetic buoyancy of a flux tube originally parallel to the surface of the sun. (a) Some part of the flux tube becomes buoyantly unstable and begins to rise. (b) The tube buckles out of the surface, leaving two sunspots and a magnetic loop. [From: The Physics of Fluids and Plasmas by Choudhuri]. {\bf Bottom:} A coronal loop on the sun. Plasma is flowing along the flux tube in an arc. [Photo by NASA].}
\end{figure}

\subsubsection{Magneto-hydrostatic configurations}
Just as we considered hydrostatic configurations for non-magnetized fluids as the balance between the gravitational force and pressure gradients, we can consider fluid configurations in MHD, where the thermal pressure balances magnetic forces. The MHD Euler equation~(\ref{eq:MHD_euler}) in the absence of gravity can also be written as
\begin{equation}
\frac{{\rm D}\bm{v}}{{\rm D}t} =-\frac{1}{\rho}\bnabla P + \frac{1}{4\pi \rho}\left( \bnabla \times \bm{B} \right)\times \bm{B}.
\end{equation}
This means that the condition for magnetohydrostatic equilibrium in the absence of gravity is
\begin{equation}
\left( \bnabla \times \bm{B} \right)\times \bm{B} = 4\pi\bnabla P.
\end{equation}
A magnetic field that satisfies this equation, is called a {\em pressure balanced field}. If the plasma is a low-$\beta$ plasma, i.e. one in which the gas pressure is negligible compared to magnetic pressure, then magnetic field lines will arrange themselves in order to yield a static configuration in which
\begin{equation}
\left( \bnabla \times \bm{B} \right)\times \bm{B}  = 0.
\end{equation}
A field which obeys this equation is called a {\em force-free} field. We can also note that this equation implies that $\bm{B}$ is parallel to $\bnabla\times\bm{B}$, i.e. for a force-free field on has
\begin{equation}
\bnabla\times\bm{B} = \mu \bm{B},\qquad\mu\in\mathbb{R}. \label{eq:force_free_B}
\end{equation}



\subsection{Magnetic Buoyancy}
Based on the MHD equations, it is an easy exercise to argue that sun-spots should come in pairs, connected by magnetic field lines. For this, one can assume a magnetic flux tube parallel to the sun's surface, i.e. a tube of parallel magnetic field lines. If we assume the flux tube is in pressure balance with its environment, we have
\begin{equation}
P_e = P_i + \frac{B^2}{8\pi},
\end{equation}
where $P_e$ is the exterior thermodynamic pressure, $P_i$ the interior one, and $B$ the magnetic field strength inside the tube. This already implies that $P_i<P_e$. If we additionally assume that the temperature in the exterior equals the temperature inside the flux tube, then
\begin{equation}
\frac{k}{m}\rho_e T = \frac{k}{m}\rho_i T + \frac{B^2}{8\pi},
\end{equation}
which can be rewritten as
\begin{equation}
\frac{\rho_e-\rho_i}{\rho_e} = \frac{B^2}{8\pi P_e} > 0.
\end{equation}
We see immediately that in this case, the interior of the flux tube must be lighter and can thus buoyantly rise. If the condition is not everywhere fulfilled, then we expect some parts of the flux tube to become unstable before others (details are of course complicated). The general picture of such a configuration that emerges is however that shown in Figure~\ref{fig:mag_buoyancy}.


\begin{figure}
\begin{center}
\includegraphics[width=\textwidth]{Chapter3/untwist.pdf}
\end{center}
\caption{\label{fig:untwist}{\bf (a)} Untangling of twisted field lines requires vortical motion, so that in the process of untwisting vorticity is generated in the flow. {\bf (b)} Topological changes in the magnetic field by magnetic reconnection, which however is only possible in non-ideal MHD.}
\end{figure}

\subsection{Vorticity}
We can again consider the \keyword{vorticity equation}, that we have already encountered for non-magnetized fluids in eq.~(\ref{eq:vorticity_eq}). In MHD flow, it takes the form
\begin{equation}
\frac{\partial \omega}{\partial t} + \boldsymbol{\nabla}\times(\boldsymbol{\omega}\times\bm{v}) = \frac{\boldsymbol{\nabla}\rho\times\boldsymbol{\nabla}P}{\rho^2} + \frac{\boldsymbol{\nabla}\times\left(\left( \boldsymbol{\nabla}\times\bm{B}\right)\times\bm{B}\right)}{4\pi \rho},
\end{equation}
and we have on the right-hand-side in addition to the usual baroclinic term a new term that indicates that vorticity can be generated from the magnetic field. As it turns out, this is e.g. the case when magnetic field lines untwist and create vorticity in the process (cf. Figure~\ref{fig:untwist}a).

\subsection{Magnetic topology, non-ideal MHD and reconnection}
Two magnetic field configurations are said to be topologically identical, if they can be differentiably transformed into one another, without having to cut and re-glue field lines. If a magnetofluid is ideal, i.e. it has no resistivity, then its magnetic topology is conserved. On the other hand, for a strong magnetic field in equilibrium, the field must obey eq.~(\ref{eq:force_free_B}). It might well be that this configuration is forbidden by topological considerations (the equation is only a local, not a global topological statement). It would then mean that no force-free configuration can exist and that the magnetized fluid would move ad infinitum.  This is of course no physical. As it turns out, in a realistic, non-ideal MHD fluid, one has diffusive terms just as in non-ideal fluids. In this case, the induction equation takes the form 
\begin{equation}
\frac{\partial \bm{B}}{\partial t} = \bnabla\times\left(\bm{v}\times\bm{B}\right) + \lambda \nabla^2 \bm{B},\qquad\lambda = \frac{c^2}{4\pi\sigma},
\end{equation}
where $\lambda$ is called the \keyword{magnetic diffusivity} and $\sigma$ is the \keyword{electrical conductivity}. Ideal MHD corresponds to the limit of infinite conductivity, i.e. $\sigma\to\infty$. In the presence of such a non-ideal term, in regions where the magnetic field gradient is large, reconnection can occur that changes the topology of magnetic field lines in order to reduce their curvature (cf. Figure~\ref{fig:untwist}b). When reconnection happens, the magnetic stress energy changes rather drastically. This mechanism is thought to be responsible for \keyword{coronal mass ejections} on the Sun.

%\section{Angular Momentum Transport}
%\section{Jets}
