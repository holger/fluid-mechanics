#include <iostream>
#include <cmath>
#include <fstream>
#include "vector.hpp"
#include <algorithm>
//DE ARAUJO PEREIRA Eva
//ETCHECHOURY Lea
//TORTI Aurelie

using namespace std;

void output(Vector& u,double dx,double t,string filename){
  //ecrire avec le temps nouvelle boucle
  ofstream out(filename,fstream::app);
  for(int i=0;i<u.size();i++){
    double x=i*dx;
    out << i << "\t" << t << "\t" << x << "\t" << u[i] << endl;
  }
  out.close();
}

//I will slove the lineqr qdvection equation equation !
int main(){
  cout << "here we go ... \n";
  //number of cells
  int n=20;

  Vector u0(n,"u0"); //inital vector

  //initial parameters
  double L=1;
  double dx=L/u0.size();
  double lambda=L-2*dx;
  //initilization of the initial vector
  for(int i=0;i<u0.size();i++){
    double x=i*dx;
    u0[i]=sin(2*M_PI/lambda*x);
  }
  //writing it for the plots
  ofstream out("u0_burger.data");
  output(u0,dx,0,"u0_burger.data");
  out.close();

  //initialisation for the time boucle
  double dt=0.025;
  int a=1;
  int tmax=500; //max of iteration
  Vector u02(n,"u02"); //vector in time
  out.open("u02_burger.data");
  output(u0,dx,0,"u02_burger.data"); //give t=0
  double t=0;
  Vector Fplus(n,"Fplus");
  Vector Fmoins(n,"Fmoins");
  double alpha1;
  double alpha2;

  for(int j=1;j<tmax;j++){
    t=j*dt;
    for(int i=1;i<u0.size()-1;i++){
      alpha1=max(abs(u0[i]),abs(u0[i-1]));
      alpha2=max(abs(u0[i+1]),abs(u0[i]));
      Fplus[i]=(1./2*((u0[i+1]*u0[i+1]/2)+(u0[i]*u0[i]/2)))-(1./2*alpha2*(u0[i+1]-u0[i]));
      Fmoins[i]=(1./2*((u0[i-1]*u0[i-1]/2)+(u0[i]*u0[i]/2)))-(1./2*alpha1*(u0[i]-u0[i-1]));
      u02[i]=u0[i]+dt/dx*(Fmoins[i]-Fplus[i]);
    }
    //boundary conditions
    u02[0]=u02[n-2];
    u02[n-1]=u02[1];
    output(u02,dx,j,"u02_burger.data");
    u0=u02; // to have u0 as the previous vector
  }
  out.close();
  cout << "Programme terminer" << endl;
}
