#include <iostream>
#include <cmath>
#include <fstream>
#include "vector.hpp"
//DE ARAUJO PEREIRA Eva
//ETCHECHOURY Lea
//TORTI Aurelie

using namespace std;

void output(Vector& u,double dx,double t,string filename){
  ofstream out(filename,fstream::app);
  for(int i=0;i<u.size();i++){
    double x=i*dx;
    out << i << "\t" << t << "\t" << x << "\t" << u[i] << endl;
  }
  out.close();
}

//I will slove the linear advection equation equation !
int main(){
  //hear I am trying to compure the error
  int o; //number of cells
  o=50;
  Vector err(o-10,"err"); //number of cell iteration
  Vector erreur(o,"erreur");
  for(int p=10;p<=o; p++){
    //because I have a problem I print to se each iteration one by one
    cout << p << " ici p" << endl;
    //number of cells
    int n=p;

    //initial condition
    Vector u0(n,"u0");
    double L=1;
    double dx=L/u0.size();
    double lambda=L-2*dx;
    //initialisation for the vector
    for(int i=0;i<u0.size();i++){
      double x=i*dx;
      u0[i]=sin(2*M_PI/lambda*x);
    }
    //write it for the plots
    ofstream out("u0_.data");
    output(u0,dx,0,"u0_.data");
    out.close();

    //initial condition for the temporal boucle
    double dt=0.025;
    int a=1;
    int tmax=500;
    double T;
    // periode lambda/(a*dt) thanks Sacha (did not have time)
    T=lambda/(a*dt);
    Vector u02(n,"u02");
    out.open("u02_.data");
    //needed for the plots but here we want the first periods
    //output(u0,dx,0,"u02_.data");
    double t=0;

    //vector to save the initial one (Lea idea to correct the error)
    Vector lea(n,"lea");
    lea=u0;

    //time loop
    for(int j=0;j<=T;j++){
      t=j*dt;
      for(int i=1;i<u0.size();i++){
        u02[i]=u0[i]+dt*a/dx*(u0[i-1]-u0[i]);
      }
      //boundary conditions
      u02[0]=u02[n-2];
      u02[n-1]=u02[1];
      //write it for the later
      output(u02,dx,j,"u02_.data");
      u0=u02;
    }
    out.close();

    //error computation
    double sum;
    sum=0.0;
    for(int i=1;i<lea.size();i++){
      sum=sum+((u02[i]-lea[i])*(u02[i]-lea[i]));
    }
    //computation of the error
    err[p]=sqrt(dx*(sum));
    //print the error but not good size problem with the vector lea
    //should have decreasing errors
    cout << err[p] << "  err " << endl;
    output(err,0,p,"err_adv.data");

  }
  cout << "Programme terminer" << endl;
}
