#include <iostream>
#include <cmath>
#include <fstream>
#include "vector.hpp"
//DE ARAUJO PEREIRA Eva
//ETCHECHOURY Lea
//TORTI Aurelie

using namespace std;

void output(Vector& u,double dx,double t,string filename){
  ofstream out(filename,fstream::app);
  for(int i=0;i<u.size();i++){
    double x=i*dx;
    out << i << "\t" << t << "\t" << x << "\t" << u[i] << endl;
  }
  out.close();
}

//I will slove the linear advection equation equation !
int main(){
  cout << "here we go ... \n";
  //number of cells
  int n=20;
  //initial parameters
  Vector u0(n,"u0");
  double L=1;
  double dx=L/u0.size();
  double lambda=L-2*dx;
  //initialisation
  for(int i=0;i<u0.size();i++){
    double x=i*dx;
    u0[i]=sin(2*M_PI/lambda*x);
  }
  //keeping it for the plots
  ofstream out("u0_lax.data");
  output(u0,dx,0,"u0_lax.data");
  out.close();

  //initialisation for the temporal boucle
  double dt=0.025;
  int a=1;
  int tmax=500; //max for the time iteration
  Vector u02(n,"u02");
  out.open("u02_lax.data");
  output(u0,dx,0,"u02_lax.data");
  int t;
  t=0;
  
  for(int j=1;j<tmax;j++){
    t=j*dt; //time properly written
    for(int i=1;i<u0.size()-1;i++){
      u02[i]=((u0[i+1]+u0[i-1])/2)-(dt*a/(dx*2)*(u0[i+1]-u0[i-1]));
    }
    //boundary condition
    u02[0]=u02[n-2];
    u02[n-1]=u02[1];
    output(u02,dx,j,"u02_lax.data");
    u0=u02; //to have u0 as the previous vector
  }
  out.close();
  cout << "Programme terminer lax" << endl;
}
