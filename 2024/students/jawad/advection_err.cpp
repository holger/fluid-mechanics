#include <iostream>
#include <cmath>
#include <fstream>
#include <iomanip> // For std::setprecision
#include "vector.hpp"

using namespace std;

void output(Vector& u, double dx, string filename) {
    ofstream out(filename);
    for(int i = 0; i < u.size(); i++) {
        double x = i * dx;
        out << x << "\t" << u[i] << endl;
    }
    out.close();
}

int main() {
    cout << "here we go ...\n";

    // Constants
    int n = 2000; // number of cells
    Vector u0(n, "u0"); // create vector for initial condition
    double L = 1; // length of domain
    double dx = L / u0.size(); // cell size
    double lambda = L - 2 * dx; // wavelength
    double a = 1;
    double CFL = 0.5; // should be < 1 for stability 
    double dt = (CFL * dx) * (1 / a); // time step size
    double nt = 1000; // number of time steps
    Vector x(n,"x.data");
    Vector u(n, "u.data");
    Vector uj(n, "uj.data");
    Vector fl(n, "fl.data");
    Vector fr(n, "fr.data");
    Vector uexact(n,"uexact");
    Vector error(n,"error");
    double t = 0.0;
    size_t snapshot = 20;
    double T = lambda / a / dt;	

    // initialize u0
    for(int i = 0; i < u0.size(); i++) {
        double x = i * dx;
        u0[i] = sin(2 * M_PI / lambda * x);
    }

    output(u0, dx, "u0.data");
    
    u0[0] = u0[n - 2];
    u0[n - 1] = u0[1]; 
    u = u0;
    uexact = u0;
    
    ofstream file_upwind("fileupwind.txt");
    ofstream errorfile("error_upwind.txt");
    
    for(int ix = 1; ix < n - 1; ix++){
        error[ix] = 0;
    }
    
      file_upwind << "#title = Upwind Scheme" << endl;
      file_upwind << "#dim = 1" << endl;
    
    // Iterate over each time step
    for(int it = 1; it < nt + 1; it++) {
        t = it * dt;
        double sum = 0;

        for(int ix = 1; ix < n - 1; ix++) {
          fl[ix] = u[ix-1];
          fr[ix] = u[ix];
   
        }

        for(int ix = 1; ix < n - 1; ix++) {
            x[ix] = ix * dx;
            uj[ix] = u[ix] + a * dt / dx * (fl[ix] - fr[ix]);
            sum = sum + (uj[ix] - uexact[ix]) * (uj[ix] - uexact[ix]);
            sum = sqrt(dx * sum);
            error[ix] = sum;
       
        }
 

        // Apply periodic boundary conditions
        uj[0] = uj[n - 2];
        uj[n - 1] = uj[1];

        // Output the data for each time step
        string filename = "uj_" + to_string(it) + ".data";
        output(uj, dx, filename);
        
	if(!(it%snapshot)){
	  file_upwind << "#time = " << t << endl;
	  for(int ix = 0; ix < n; ++ix) {
	    file_upwind << x[ix] << " " << uj[ix] << endl;
	  }
	  file_upwind << endl << endl;
	}
	
	
        // Copy the contents of uj to u
        u = uj;
    }
    
    
    for(int ix = 1; ix < n - 1; ix++){
    
	errorfile << ix << " " << error[ix] << endl;
	
	}

	
    return 0;
}
