#include <iostream>
#include <cmath>
#include <fstream>
#include <iomanip>
#include "vector.hpp"

using namespace std;

void output(Vector& u, double dx, string filename) {
  ofstream out(filename);
  for(int i=0; i<u.size(); i++) {
    double x = i*dx;
    out << x << "\t" << u[i] << endl;
  }
  out.close();
 
}

// Lax-Friedrichs linear scheme
int main()
{
  
  // Constants
  
  int n = 2000; // number of cells
  Vector u0(n,"u0"); // create vector for initial condition
  double L = 1; // length of domain
  double dx = L/u0.size(); // cell size
  double lambda = L-2*dx; // wavelength
  double a = 1;
  double CFL = 0.5;
  double dt = (CFL * dx) * (1 / a);
  double nt = 1000;
  Vector x(n,"x.data");
  Vector u(n,"u.data");
  Vector uj(n,"uj.data");
  double t = 0.0;
  size_t snapshot = 30;
  
  // initialize u0
  for(int i=0;i<u0.size();i++) {
    double x = i*dx;
    u0[i] = sin(2*M_PI/lambda*x);
  }
  
  output(u0,dx,"u0.data_lax");
  

  
  //scheme
 
  
  u = u0;
  
  ofstream file_lax("file_lax.txt");
  
   file_lax << "#title = Lax-Friedrich linear Scheme" << endl;
   file_lax << "#dim = 1" << endl;
  
   // time loop
  
  for(int it = 1; it < nt + 1; it++){
    t = it*dt;
    
    for(int ix = 1; ix < n - 1; ix++){
    x[ix] = ix * dx;
    uj[ix] = 0.5 * (u[ix+1] + u[ix-1]) - CFL * 0.5 * (u[ix+1] - u[ix-1]);
    }
    
    uj[0] = uj[n - 2];
    uj[n - 1] = uj[1];
    
    if(!(it%snapshot)){
    	output(uj,dx,"uj.data_lax"); }
    	
    if(!(it%snapshot)){
    	file_lax << "#time = " << t << endl;
    	for(int ix = 0; ix < n; ++ix) {
		file_lax << x[ix] << " " << uj[ix] << endl;
	  }
     	file_lax << endl << endl;
    }
    
    u = uj; 
  }
    
  return 0; 
}


