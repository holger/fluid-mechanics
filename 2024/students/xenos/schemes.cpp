//////////////////////////////////////////////////////////////////////////
// Code to solve the advection and Burger's equation,
// with an upwind and Lax - Friedrich's scheme
//////////////////////////////////////////////////////////////////////////
// XENOS Konstantinos Odysseas
// LEFEBVRE Elora
// DUCHAMPS Aurelie
//////////////////////////////////////////////////////////////////////////
// u0_ : Vector corresponding to the initial condition for _ scheme
// un : Vector corresponding to the upwind scheme
// ub:  Vector corresponding to the L-F scheme - advection
// uf:  Vector corresponding to the L-F scheme - Burger's equation

#include <iostream>
#include <cmath>
#include <fstream>
#include <algorithm>
#include "vector.hpp"

using namespace std;

void output(Vector& u, double dx, string filename) {
  ofstream out(filename,fstream::app);
  for(int i=0;i<u.size();i++) {
    double x = i*dx;
    out << i << "\t" << x << "\t" << u[i] << endl;
  }
}
int main()
{
  //To run code without deleting files each time
  ofstream out("un.data");
  out.close();

  out.open("ub.data");
  out.close();

  out.open("uf.data");
  out.close();

  out.open("u0.data");
  out.close();

  out.open("u0b.data");
  out.close();

  out.open("u0f.data");
  out.close();

  // Number of cells
  int n = 50;

  // Create vector for initial condition
  Vector u0(n,"u0");

  // Length of domain
  double L = 1;

  // Cell size
  double dx = L/u0.size();

  // Wave length
  double lambda = L-2*dx;

  // For the second and third scheme scheme
  Vector u0b(n,"u0b");
  Vector u0f(n,"u0f");

  // Initialize u0 u0b and u0f as sine waves
  for(int i=0;i<u0.size();i++) {
    double x = i*dx;
    u0[i] = sin(2*M_PI/lambda*x);
  }
  u0b = u0;
  u0f = u0;

  //Vector at time n+1
  Vector un(n,"un");

  double a = 1;

  //CFL  number a*Dt/Dx = 0.5 for stability
  double dt = 0.5*dx/a;
  double T=lambda/(a*dt);
  T = round(T);

  double fac0 = dt*a/dx;

  // Upwind scheme for advection equation
  output(u0,dx,"u0.data");
  for (int j=0;j<T;j++){
    for(int i=1;i<u0.size()-1;i++) {
      un[i] = u0[i] + fac0*(u0[i-1] - u0[i]);
      }

  //Periodic boundary conditions
    un[0] = un[un.size()-2];
    un[un.size()-1] = un[1];

    u0 = un;
    output(un,dx,"un.data");
  }

    //Lax-Friedrichs scheme

    Vector ub(n,"ub");

    output(u0b,dx,"u0b.data");

    double facb = (dt*a)/(2*dx);

    for (int j=0;j<T;j++){
      for(int i=1;i<u0b.size()-1;i++) {
        ub[i] = 0.5*(u0b[i+1] + u0b[i-1]) - facb*(u0b[i+1] - u0b[i-1]);
      }

    ub[0] = ub[ub.size()-2];
    ub[ub.size()-1] = ub[1];
    u0b = ub;

    output(ub,dx,"ub.data");
    }

    //Burger's equation, Lax-Friedrichs scheme. Here Fj = uj^2/2
    Vector uf(n,"uf");
    Vector Fp(n,"Fp");
    Vector Fn(n,"Fn");

    output(u0f,dx,"u0f.data");

    for (int j=0;j<T;j++){
      for(int i=1;i<u0f.size()-1;i++) {
        Fn[i] = 0.25*(u0f[i-1]*u0f[i-1] + u0f[i]*u0f[i]) -
                0.5*max(abs(u0f[i]), abs(u0f[i-1]))*(u0f[i]-u0f[i-1]);  //Fj-1/2

        Fp[i] = 0.25*(u0f[i]*u0f[i] + u0f[i+1]*u0f[i+1]) -
                0.5*max(abs(u0f[i+1]), abs(u0f[i]))*(u0f[i+1]-u0f[i]);  //Fj+1/2

        uf[i] = u0f[i] + dt/dx*(Fn[i]-Fp[i]);
      }
    u0f = uf;

    uf[0] = uf[uf.size()-2];
    uf[uf.size()-1] = uf[1];

    u0f = uf;
    output(uf,dx,"uf.data");
    }
}
