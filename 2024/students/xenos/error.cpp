//////////////////////////////////////////////////////////////////////////
// Code to compute the error in the upwind and Lax-Friedrich's schemes
//////////////////////////////////////////////////////////////////////////
// XENOS Konstantinos Odysseas
// LEFEBVRE Elora
// DUCHAMPS Aurelie
//////////////////////////////////////////////////////////////////////////
// u0_ : Vector corresponding to the initial condition for _ scheme
// un : Vector corresponding to the upwind scheme
// ub:  Vector corresponding to the L-F scheme - advection
// uconst:  Vector corresponding to the unchanged initial condition, used
// in the error calculation

#include <iostream>
#include <cmath>
#include <fstream>
#include <algorithm>
#include "vector.hpp"

using namespace std;

void output(double size, double error, string filename) {
  ofstream out(filename,fstream::app);
    out << size << "\t" << error << endl;
}
int main()
{
  ofstream out("errorn.data");
  out.close();

  out.open("errorb.data");
  out.close();

  out.open("uconst.data");
  out.close();

  // We shall implement the two schemes for 10 - 500 cells in order to look
  // at how the error changes with the number of cells

  for (int l = 10; l <= 500; l++){

    int n = l;

    Vector u0(n,"u0");

    double L = 1;

    double dx = L/u0.size();

    double lambda = L-2*dx;

    Vector u0b(n,"u0b");
    Vector uconst(n,"const");

    // Our initial condition is a sine

    for(int i=0;i<u0.size();i++) {
      double x = i*dx;
      u0[i] = sin(2*M_PI/lambda*x);
    }

    u0b = u0;
    uconst = u0;

    //Upwind scheme

    Vector un(n,"un");

    double a = 1.;
    double dt = 0.5*dx/a;
    double T=lambda/(a*dt);
    T = round(T);

    double fac0 = dt*a/dx;

    for (int j=0;j<=T;j++){
      for(int i=1;i<u0.size()-1;i++) {
        un[i] = u0[i] + fac0*(u0[i-1] - u0[i]);
        }

      un[0] = un[un.size()-2];
      un[un.size()-1] = un[1];
      u0 = un;
    }

    // The error is calculated as the difference between the initial condition
    // and the last form of the sine after being propagated for one wavelength
    double sum = 0;
    double err = 0;

    for(int i=0;i<u0.size();i++) {
      sum = sum + (un[i] - uconst[i])*(un[i] - uconst[i]);
      }
    err = sqrt(dx*sum);
    output(uconst.size(),err,"errorn.data");

    //Lax-Friedrich's scheme

    Vector ub(n,"ub");

    double facb = (dt*a)/(2*dx);

    for (int j=0;j<=T;j++){
      for(int i=1;i<u0b.size()-1;i++) {
        ub[i] = 0.5*(u0b[i+1] + u0b[i-1]) - facb*(u0b[i+1] - u0b[i-1]);
      }

    ub[0] = ub[ub.size()-2];
    ub[ub.size()-1] = ub[1];
    u0b = ub;
    }

      double sum2 = 0;
      double err2 = 0;

      for(int i=0;i<u0.size();i++) {
        sum2 = sum2 + (ub[i] - uconst[i])*(ub[i] - uconst[i]);
        }

      err2 = sqrt(dx*sum2);
      output(uconst.size(),err2,"errorb.data");
    }
}
