// to compile: g++ -o data data.cpp

#include <iostream>

using namespace std;

// function to check for out of bounds access
double& access(double* data, int n, int i) {
  if(i<0 || i >= n) {
    cout << "PROBLEM\n";
  }
  return data[i];
}

// small programm to allocate and access memory for storing numbers in an array
int main()
{
  // choose to have 10 elements in array
  int n = 10;
  
  // allocate memory for a double precicion floating point value,
  // give it the name "a" and assign 1.1 to it.
  double a = 1.1;

  // print the value in the terminal
  cout << "a = " << a << endl;
  // print the memory address of a
  cout << "&a = " << &a << endl;

  // allocate the memory an adresse to a double value and assign the
  // address of a
  double* ap = &a;
  cout << "ap = " << ap << endl;
  // get the value stored at the address of a
  cout << "*ap = " << *ap << endl;

  // create an array of n double values
  double* data = new double[n];

  // initialize the array (caution: it should be i < n -> ERROR)
  for(int i = 0; i <= n ; i++)
    data[i] = i;

  // test access functions
  for(int i = 0; i <= n ; i++) {
    cout << i << "\t" << data[i] << endl;
    cout << i << "\t" << access(data,n,i) << endl;
  }
}
