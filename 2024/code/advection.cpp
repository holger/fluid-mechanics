#include <iostream>
#include <cmath>
#include <fstream>

#include "vector.hpp"

using namespace std;

void output(Vector& u, double dx, string filename) {
  std::cout << "output begins\n";
  ofstream out(filename);
  for(int i=0;i<u.size();i++) {
    double x = i*dx;
    out << i << "\t" << x << "\t" << u[i] << endl;
  }
  out.close();
  std::cout << "output ends\n";
}

// I will solve the linear advection equation!
int main()
{
  cout << "here we go ...\n";
  
  // number of cells
  int n = 20;

  // create vector for initial condition
  Vector u0(n,"u0");

  // length of domain
  double L = 1;
  
  // cell size
  double dx = L/u0.size();

  // wave length
  double lambda = L-2*dx;
  
  // initialize u0
  for(int i=0;i<u0.size();i++) {
    double x = i*dx;
    u0[i] = sin(2*M_PI/lambda*x);
  }

  output(u0,dx,"u0.data");
}
