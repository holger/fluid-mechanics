#ifndef vector_hpp
#define vector_hpp

using namespace std;

// object oriented programming
// idea: put data and (access)-functions into ONE object
class Vector
{
  // public section of class: members and functions are accessible in main
public:
  // constructor (called during object creation)
  Vector(int n, string name) {
    cout << "Vector(" << n << "," << name << ")\n";
    data_ = new double[n];
    n_ = n;
    name_ = name;
  }

  // copy-constructor for Vector a(b);
  Vector(const Vector& vector) {
    n_ = vector.n_;
    name_ = vector.name_;
    data_ = new double[n_];
    for(int i=0; i<n_; i++)
      data_[i] = vector.data_[i];
  }

  // destructor
  ~Vector() {
    cout << "~Vector()\n";
    delete [] data_;
  }

  string get_name() {
    return name_;
  }

  // simple member-function to access data_ elements
  double& access(int i)  {
    if(i<0 || i >= n_) {
      cout << "PROBLEM\n";
    }
    return data_[i];
  }

  int size() {
    return n_;
  }

  // copy operator
  const Vector& operator=(const Vector& vector) {
    if(n_ != vector.n_) exit(0);
    
    name_ = vector.name_;

    for(int i=0; i<n_; i++)
      data_[i] = vector.data_[i];

    return *this;
  }
  
  // operator to conveniently access data_ 
  double& operator[](int i)  {
    if(i<0 || i >= n_) {
      cout << "PROBLEM\n";
    }
    return data_[i];
  }

  // private section of class: members and functions are only
  // accessible from class Vector
private:
  double* data_;
  int n_;
  string name_;
};

#endif 
