// to compile: g++ -o vector vector.cpp

#include <iostream>
#include <fstream>

#include "vector.hpp"

int main()
{
  // choose to have 10 elements in array
  int n = 10;

  // create object of class Vector with name vector1. This line calls
  // the constructor of the Vector class
  Vector vector1(n, "vector1");

  cout << vector1.get_name() << endl;

  // access data via member function access or via operator[]
  vector1.access(7) = 1;
  vector1[6] = 1;

  // initialize vector1 data
  for(int i=0; i<n; i++)
    vector1[i] = i;
  
  for(int i=0; i<n; i++)
    cout << i << "\t" << vector1[i] << endl;

  ofstream out("vector1.data");
  for(int i=0; i<n; i++)
    out << i << "\t" << vector1[i] << endl;
  out.close();
  
  // test out of bounds checking
  vector1[12] = 1;
}

// gnuplot
// plot "vector1.data" w lp
