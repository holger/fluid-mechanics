set term epslatex standalone color ',,12'	# output to tex format
set output "flow_over_hill_plot.tex"		# set file name

set contour					# show contours
unset surface					# remove graph -> show only contours
set view map					# show only x-y plane
set cntrparam levels incremental 1,1,8		# choose contour levels
unset key     	     		 		# show no legend

set multiplot					# allow multiple plots
set xlabel 'x'					# axis label x
set ylabel 'y'					# axis label y
set yra [0:]					# show only upper half

set title 'Flow over hill visualized by stream function $\Psi$'				# set title
splot (sqrt(x*x+y*y)-16./sqrt(x*x+y*y))*(y/sqrt(x*x+y*y)) t '$\Psi(x,y)$'		# plot Psi function

set cntrparam levels incremental 0.01,1,1		    				# choose single level=0.01 for the bottom flow
splot (sqrt(x*x+y*y)-16./sqrt(x*x+y*y))*(y/sqrt(x*x+y*y)) t '$\Psi(x,y)$' lw 10 lc 0	# plot bottom flow
