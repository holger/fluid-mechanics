%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{First steps in Computational Fluid Dynamics}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Discretising the fluid equations -- Eulerian and Lagrangian schemes}
In the astrophysical community, predominantly two rather different approaches are used to simulate the dynamics of fluids on computers. In the first, the Eulerian schemes, the equations are discretised on a regular grid structure that remains fixed in physical space (but can be locally refined e.g.) and the evolution of the fundamental fluid quantities, i.e. either the primitive variables $(\rho,\mathbf{v},e)$ or the conservative quantities $(\rho,\rho \mathbf{v},\rho \mathbf{v^2}+e)$ are evolved at the grid point locations. In the second class of schemes, the Lagrangian schemes, the equations are discretised in terms of characteristic points that move with the fluid. Typically, the main advantage of Lagrangian schemes is that they are self-adaptive, i.e. the points where the solution is known move into regions of high density, therefore preserving information about the flow. The usual price to pay in Lagrangian schemes is that it is much harder to correctly treat mixing of different fluid phases correctly. On the other hand, numerical errors are usually much better controlled in Eulerian schemes, but sophisticated schemes have to be employed (adaptive mesh refinement) in order to introduce new grid points in regions of convergent flow, in order to not wash out details of the flow and preserve information. Here, we will just attempt to give a flavour of both approaches using {\sc Python} notebooks. For simplicity, we resort here to a set of two equations, the continuity and Euler equation, closed with a barotropic equation of state. This set of equations is by definition isentropic, but allows us to illustrate the fundamental features.

\subsection{The fluid equations in an Eulerian frame}
We just summarise here once again the relevant simplified fluid equations in an Eulerian frame (cf. Section~\ref{sec:hydro_equations}), which are in primitive form in one spatial dimension given by
\begin{eqnarray}
\frac{\partial \rho}{\partial t} + \frac{\partial}{\partial x}\left( \rho v \right) & = & 0 \\
\frac{\partial v}{\partial t}  + v\frac{\partial v}{\partial x} & = & -\frac{1}{\rho}\frac{\partial P}{\partial x},
\end{eqnarray}
while the equivalent equations in conservative form are
\begin{eqnarray}
\frac{\partial \rho}{\partial t} + \frac{\partial}{\partial x}\left( \rho v \right) & = & 0 \\
\frac{\partial \left(\rho v\right)}{\partial t}  + \frac{\partial}{\partial x}\left(\rho v^2 \right) & = & -\frac{\partial P}{\partial x}.
\end{eqnarray}

\subsection{The fluid equations in a Lagrangian frame}
We recall the Lagrangian derivative of some quantity $q(\mathbf{x}(t))$ along a characteristic $x(t)$, for which $\dot{\mathbf{x}} = \mathbf{v}(\mathbf{x}(t))$ (i.e. it is moving along with the flow), is given by
\begin{equation}
\frac{{\rm D} q}{{\rm D} t} = \frac{\partial q}{\partial t} + \mathbf{v}\cdot \nabla q.
\end{equation}
This means that we can write the continuity and Euler equation in the form
\begin{eqnarray}
\frac{{\rm D}\rho}{{\rm D}t} & = & -\rho \nabla\cdot\mathbf{v}, \\
\frac{{\rm D}\mathbf{v}}{{\rm D}t} & = & -\frac{1}{\rho}\nabla P.
\end{eqnarray}
And the fluid parcel of course follows the characteristic  $\dot{\mathbf{x}} = \mathbf{v}$ by definition. We see that in principle the fluid equations take a very simple form in terms of the characteristic curves $(\mathbf{x}_i,\mathbf{v}_i)$,
\begin{eqnarray}
\dot{\mathbf{x}}_i &=& \mathbf{v}_i, \\
\dot{\mathbf{v}}_i &=& -\left. c_s^2 \frac{\nabla\rho}{\rho}\right|_{x_i}.
\end{eqnarray}
If we have a way to estimate the density $\rho$ and its gradient $\nabla \rho$ from the characteristics, we can solve these equations. We will see how to do this later. Note that in the second equation, we have exploited that $c_s^2 = \partial P/ \partial \rho$.


\section{Smoothed-particle Hydrodynamics (SPH)}
In our little excursion into the numerical world, we will first turn to a Lagrangian method, the so-called \keyword{smoothed-particle hydrodynamics}, or SPH, method. As we have seen above, the Lagrangian equations of motion for characteristics are particularly simple, but their use is hampered by the fact that we need an estimate of the density. This is what SPH is all about. The idea is to use the local density of characteristics (or fluid parcels) as an estimate of the density by means of so-called \keyword{kernel density estimation}.

\subsection{Kernel density estimation}
Kernel density estimation is a widely used method in statistics to obtain a non-parametric estimate of a probability density function of a random variable. The basic idea is that for a sample $(x_1,x_2,\dots,x_n)$ that is fairly drawn from an unknown density $f$, one can use the kernel density estimator
\begin{equation}
\hat{f}_h(x) = \frac{1}{n} \sum_{i=1}^n K_h(x-x_i) = \frac{1}{nh} \sum_{i=1}^n K\left(\frac{x-x_i}{h}\right),
\end{equation}
where $K(\cdot)$ is the kernel function and $h>0$ is a smoothing parameter (also called ``bandwidth'' sometimes), which one wants to choose as small as possible while still averaging over enough points to obtain a low variance estimate. Assuming we know the density $\rho_i$ at every point $x_i$, then the general kernel estimate of a variable $A$, that is known at those points, is given by
\begin{equation}
A(x) = \sum_{i=1}^n m_i \frac{A_i}{\rho_i} K\left(\frac{x-x_i}{h}\right),
\end{equation}
where the term $\rho_i^{-1}$ appears to avoid weighting the estimate by the density of points. For $A_i=\rho_i$, this trivially reduces to
\begin{equation}
\rho(x) = \sum_{i=1}^n m_i K\left(\frac{x-x_i}{h}\right).
\end{equation}
The gradient of a density weighted kernel estimate, assuming that $h$ is spatially constant can also be easily calculated to be
\begin{equation}
\nabla A(x) = \sum_{i=1}^n m_i \frac{A_i}{\rho_i} \nabla K\left(\frac{x-x_i}{h}\right),
\end{equation}
which means that one only has to replace the kernel $K$ with the gradient of the kernel $\nabla K$. A natural choice for a kernel would be e.g. a Gaussian kernel, but more commonly a function with finite support is used so that points at distances larger than the support of the kernel can be neglected in the sum, making the calculation more efficient. The traditionally used kernel in SPH is the B-spline kernel (Monaghan 1992) of the form
\begin{equation}
K(q) = \left\{ 
\begin{array}{ll}
\sigma_h \left[1-\frac{3}{2}q^2\left(1-\frac{q}{2}\right)\right], &  \textrm{for } 0\le q \le 1\\
\sigma_h \frac{3}{4} \left(2-q\right)^3, &  \textrm{for } 1 < q \le 2\\
0, &  \textrm{for } q>2
\end{array}
\right.
\end{equation}
with a normalisation constant which has in $d$ spatial dimensions the value
\begin{equation}
\sigma_h = \left\{ 
\begin{array}{ll}
\frac{2}{3h}, & \textrm{for } d=1 \\
\frac{10}{7\pi h^2}, & \textrm{for } d=2 \\
\frac{1}{\pi h^3}, &\textrm{for } d=3
\end{array}
\right. .
\end{equation}
The gradient of the kernel can be readily calculated as
\begin{equation}
\nabla K = \left\{ 
\begin{array}{ll}
\sigma_h \left[ 3q\left(\frac{q}{2}-1\right)+\frac{3}{4}q^2\right], & \textrm{for } 0\le q \le 1 \\
-\sigma_h \frac{9}{4}\left(2-q\right)^2, & \textrm{for } 1 < q \le 2 \\
0, & \textrm{for } q>2
\end{array}
\right. .
\end{equation}
The shape of the B-spline kernel and its gradient are shown in Figure~\ref{fig:SPH_kernel}.

\begin{figure}
\begin{center}
\includegraphics[width=0.67\textwidth]{Chapter4/kernels.pdf}
\end{center}
\caption{\label{fig:SPH_kernel}The B-spline density estimation kernel and its gradient which are commonly employed in the estimation of density and density weighted quantities in SPH.}
\end{figure}

\subsection{Pressure force in SPH}
Using these kernels, we can now finally write down the pressure force term that appears in the Lagrangian Euler equation. There is some ambiguity how exactly the pressure gradient should be formulated and in fact a lot of recent debate about new developments in SPH has gone in this direction. Here, we show two different kernel based discretisations. The first is due to Katz~\&~Hernquist~1989 and has the form
\begin{equation}
\frac{{\rm D} v_i}{{\rm D}t} = -\sum_j m_j \left( 2\frac{\sqrt{P_i P_j}}{\rho_i \rho_j}\right) \nabla_i K\left(\frac{x_j - x_i}{h}\right),
\end{equation}
where $\nabla_i$ indicates that the derivative is taken with respect to $x_i$ and not $x_j$, and the sum is taken over all points $x_j$ in the support of the kernel, i.e. with a distance smaller than $2h$. An alternative formulation is due to Morris~1996 and has the form
\begin{equation}
\frac{{\rm D} v_i}{{\rm D}t} = \sum_j m_j \left( \frac{P_i -P_j}{\rho_i \rho_j}\right) \nabla_i K\left(\frac{x_j - x_i}{h}\right).
\end{equation}

\subsubsection{A 1D SPH method}
A full scheme is obtained by using the velocity/momentum updates as above, and the density can be computed by kernel estimation at any desired time. In principle, one could also evolve the density at the particle locations:
\begin{equation}
\frac{{\rm D}\rho_i}{{\rm D}t} = \sum_j m_j \left(v_i-v_j\right) \nabla_i K\left(\frac{x_j - x_i}{h}\right),
\end{equation}
but the mass is trivially conserved in Lagrangian schemes by just keeping the mass of the SPH particles fixed. With all the equations at hand, we can now turn all that we have so far into a small {\sc Python} program that evaluates the pressure force based on the locations of the characteristics $x_i$. First the code for the kernels in 1D:

\begin{pythoncode}
def W_1D(q,h):
    q = np.abs(q)/h
    
    i1 = np.where(np.logical_and(q>=0,q<=1))
    i2 = np.where(np.logical_and(q>1,q<=2))
    i3 = np.where(q>2)
    
    sigma = 2.0/(3.0*h)
    ret = np.zeros_like(q)
    ret[i1] = sigma*(1-1.5*q[i1]**2*(1-q[i1]/2))
    ret[i2] = sigma*0.25*(2-q[i2])**3
    ret[i3] = 0.0
    
    return ret
\end{pythoncode}

\begin{pythoncode}
def grad_W_1D(q,h):
    s = np.sign(q)
    q = np.abs(q)/h
    
    i1 = np.where(np.logical_and(q>=0,q<=1))
    i2 = np.where(np.logical_and(q>1,q<=2))
    i3 = np.where(q>2)
    
    sigma = 2.0/(3.0*h)
    ret = np.zeros_like(q)
    ret[i1] = sigma*(3*(q[i1]/2-1.0)*q[i1]+0.75*q[i1])
    ret[i2] = -3*sigma*0.25*(2-q[i2])**2
    ret[i3] = 0.0
    
    return s*ret
\end{pythoncode}

In order to compute the pressure force, we will for simplicity just loop over all particles and zero the contribution due to the particle itself. We also have to take into account periodic boundaries when evaluating the kernel: let's assume our domain has an extent $L$. For two points $x_i$ and $x_j$, under periodic boundary conditions, the distance vector $d$ is $d=x_i - x_j$ if $-L/2 < x_i - x_j \le L/2$. If however $x_i - x_j > L/2$, then $d = x_i - x_j - L$, and if $x_i - x_j < -L/2$, then $d = x_i - x_j + L$. Putting this together we can calculate the pressure force in a periodic domain using a kernel estimate as

\begin{pythoncode}
def compute_pressure_force( x, rhop, L, pmass, h, K, gamma ):
    pressure = K * rhop ** gamma
    
    ipart = 0
    pressure_force = np.zeros_like(x)
    # loop again over all particles
    for xp in x:
        # compute distance obeying periodic boundaries
        d = x - xp
        ii1 = np.where( d > 0.5*L )
        ii2 = np.where( d < -0.5*L)
        d[ii1] = d[ii1] - L
        d[ii2] = d[ii2] + L
    
        # Morris (1996) SPH
        force_i = -(pressure[ipart] - pressure)/(rhop[ipart]*rhop) \
        		* grad_W_1D( d, h ) / h
        
        # zero self-force        
        force_i[ipart] = 0.0
        pressure_force[ipart] = np.sum(force_i)
        
        ipart = ipart + 1
        
    return pmass * pressure_force
\end{pythoncode}

We also need to evaluate the density, if we are interested in it, which we can write as follows:

\begin{pythoncode}
def compute_density( x, L, pmass, h ):
    rho = np.zeros_like( x )
    ipart = 0
    for xp in x:
        d = x - xp
        ii1 = np.where( d > 0.5*L )
        ii2 = np.where( d < -0.5*L)
        d[ii1] = d[ii1] - L
        d[ii2] = d[ii2] + L
        rho_i = pmass * W_1D( d, h )
        rho[ipart] = np.sum(rho_i)
        ipart = ipart + 1
    return rho
\end{pythoncode}

\subsection{Test problem: a convergent flow developing a reflected shock wave}
Once again we consider the test problem from above, an initially uniform density and a sinusoidal velocity perturbation. Instead of the first order time integration method we used for the finite difference scheme, we will here use a \keyword{leap-frog integration} scheme which is second order accurate in time: we first update the density and particle positions by a half time-step $\Delta t/2$, then we perform a full time-step for the velocities, followed by a final half time-step for positions and densities using the new velocities. The code fragment that sets up the initial conditions and performs the time integration should then look like this:

\begin{pythoncode}
Lbox = 1.0
Npart = 256
h = 0.01
totmass = 1.0
gamma = 5.0/3.0
K = 0.3
pmass = totmass / Npart
dx = Lbox / Npart
tend = 0.25

x = np.arange(Npart) * dx
rho = np.ones_like(x) * totmass / Lbox
v = np.sin(2.0*np.pi * x)

dt = 0.01
t = 0
while t<tend:
    # drift
    x = x + 0.5*dt * v
    
    # kick
    rho = compute_density( x, Lbox, pmass, h )
    acc = compute_pressure_force( x, rho, Lbox, pmass, h, K, gamma )
    v = v + dt * acc
    
    #drift
    x = x + 0.5*dt * v
    
    t = t + dt
\end{pythoncode}

The final solution can be seen in Figure~\ref{fig:planewave_SPH}. We recover a solution that develops a shock develops but produces oscillations.These can be reduced by adding artificial viscosity (as we will below). The main advantage of Lagrangian methods such as SPH is that they are very simple to implement and solve advection problems trivially since they have a built-in Galilean invariance. They do have problems with correct entropy production however, since they do not correctly represent mixing in fluids in their simpler formulations. For these reasons, this old school SPH approach is no longer used and instead ``moving mesh'' methods have become more fashionable. In these methods, one still considers Lagrangian fluid elements, but allows for fluxes between them, similar to the finite volume method that we will discuss below. 

\begin{figure}
\begin{center}
\includegraphics[width=0.75\textwidth]{Chapter4/planewave_SPH.pdf}
\end{center}
\caption{\label{fig:planewave_SPH}Numerical evolution using the SPH method of an initially homogeneous density under a sinusoidal velocity perturbation evolving into an outward propagating shock. The solution has oscillatory features. They can be suppressed by adding an artificial viscosity.}
\end{figure}

\subsection{Collisionless flow}
Since SPH is essentially an $N$-body method, one can model collisionless fluids simply by omitting the pressure force. Since we already used a symplectic kick-drift-kick (leapfrog) method to advance our system in time, the pressure-free case in fact corresponds simply to free Hamiltonian trajectories. Instead of forming a shock, we will then get a region where particles start to cross each other. This phenomenon is called ``multi-streaming''.


\section{Finite difference and finite volume Eulerian methods}
\subsection{Finite difference discretisation}
One way to discretise the Eulerian equations above is by means of \keyword{finite differences}, i.e. we approximate the temporal and spatial derivatives of a quantity $q$ by
\begin{equation}
\frac{\partial q}{\partial t} \simeq \frac{q(x,t+\Delta t)-q(x,t)}{\Delta t}\quad\textrm{and}\quad\left.\frac{\partial q}{\partial x}\right|_x \simeq \frac{q(x+\Delta x,t)-q(x-\Delta x,t)}{2\Delta x},
\end{equation}
which can be easily shown to be accurate at first order in $\Delta t$ and to second order in $\Delta x$ (i.e. coincide with the respective Taylor expansions up to that order). For simplicity we will assume that we know the quantities $q(x)$ at uniformly spaced discrete locations $i \Delta x$, $i\in \mathbb{Z}$, so that we can simply write $q_i = q(i\Delta x)$. We will do likewise in time and write $q^n = q(t_0 + n\Delta t)$ so that $q^0 = q(t_0)$. 

Putting everything together, we can thus write down a finite difference version of the conservative form of the equations as
\begin{eqnarray}
\rho_i^{n+1} &=& \rho_i^{n} - \frac{\Delta t}{2 \Delta x}\,\left[ (\rho v)_{i+1}^n - (\rho v)_{i-1}^n\right], \\
(\rho v)_i^{n+1} &=& (\rho v)_i^{n} - \frac{\Delta t}{2 \Delta x}\,\left[ (\rho v^2+P)_{i+1}^n - (\rho v^2+P)_{i-1}^n\right],
\end{eqnarray}
with a polytropic equation of state $P=K\,\rho^\gamma$ to close the system.

%We will assume a polytropic equation of state here, i.e. $P=K \rho^\gamma$. This allows us to write the adiabatic sound speed as $c_s^2 = \partial P/\partial \rho = \gamma K \rho^{\gamma-1} = \gamma P/\rho$ and thus $P = \rho c_s^2 / \gamma$.

In order to avoid dealing with boundary conditions, we will use a periodic grid of length $N$, i.e. for an index $i$ the relation $i+N=i$ holds. 
Let's code this up in {\sc Python}, try using a {\sc Jupyter} notebook so that you can directly visualise the results. Dealing with periodic boundaries is particularly simple if we use the {\sc numpy} function {\sc roll}, which can shift the cells by a specified amount and automatically wraps around the boundaries.


\begin{pythoncode}
import numpy as np
import matplotlib.pyplot as plt
%matplotlib notebook

# compute the right-hand side of the FD equations
def RHS( rho, rhou, K, gamma ):
    pressure = K * rho**gamma
    rhou2 = rhou**2 / rho
    drho = -0.5 * (np.roll(rhou,-1)-np.roll(rhou,+1))
    drhou = -0.5 * (np.roll(rhou2+pressure,-1)-np.roll(rhou2+pressure,+1))
    
    return (drho,drhou)

# perform a single time step
def step( rho, rhou, dt, dx, K, gamma ):
    (drho,drhou) = RHS(rho,rhou,K,gamma)
    rho = rho + dt/dx * drho
    rhou = rhou + dt/dx * drhou
    
    return (rho,rhou)

\end{pythoncode}

\subsection{Test problem: a convergent flow developing a reflected shock wave}
Now, all we need is a main routine that sets the initial conditions, parameters and calls the time-stepping function {\sc step}. As our first initial conditions, we will assume a periodic domain $0\le x\le 1$, on which we have an initially uniform density $\rho(x,t_0) = 1$ and we put a sinusoidal velocity perturbation $v(x) = \sin( 2\pi x,t_0)$.

\begin{pythoncode}
# initialise homogeneous particle distribution
Lbox = 1.0
Ngrid = 256
gamma = 5.0/3.0
K = 0.3
totmass = 1.0
dx = Lbox / Ngrid

x = np.arange(Ngrid) * dx
rho = np.ones_like(x) * totmass / Lbox
u = np.sin(2.0*np.pi * x)
rhou = rho * u

dt = 0.001
t = 0
tend = 0.25
while t<tend:    
    (rho,rhou) = step( rho, rhou, dt, dx, K, gamma )
    t = t+dt
\end{pythoncode}

\begin{figure}
\begin{center}
\includegraphics[width=0.75\textwidth]{Chapter4/planewave_FD.pdf}
\end{center}
\caption{\label{fig:planewave_FD}Numerical evolution using finite differences of an initially homogeneous density under a sinusoidal velocity perturbation evolving into an outward propagating shock. The shock develops but the 2nd order finite difference discretisation leads to oscillatory features.}
\end{figure}

The results of this little numerical experiment are shown in Figure~\ref{fig:planewave_FD}. We see nicely how the flow that is convergent towards $x=0.5$ increases the density at that location until the pressure force has grown large enough to push back the material. This outward travelling material quickly develops into two shocks, which are however not well reproduced by the finite difference scheme since a strongly oscillatory feature develops along with the shock. There are two problems with our simple scheme. First, the shock is a discontinuity, and so we should not be using a second order spatial approximation, since it will almost certainly over- or undershoot the true solution in a bad way. 

The second problem is that our finite difference discretisation does not respect the causal structure of the problem. When calculating the updated solution at a location $x_i$, neighbouring locations in both directions, i.e. $i+1$ and $i-1$, are used. However, the pre-shock material should not be allowed to have information about the shock before it actually arrives. It can be shown in a simple stability analysis that the simple finite difference scheme is unstable for this reason. Incorporating the causal structure properly into such schemes can be achieved by the usage of so-called \keyword{Riemann solvers}, which are explicitly built to reproduce correctly the causal wave structure of discontinuous solutions. We will look into this later. First, we will exploit a physical mechanism to prevent oscillations at discontinuities, since the terms that are causing the instability can be shown to contain terms of the form $\nabla^2 v$ and $\nabla^2 \rho$, the first clearly being of the form of viscosity and the second of diffusion, albeit with a coefficient with the wrong sign so that oscillations grow and are not damped out.

\subsection{Artificial viscosity}
In order to both get a more intuitive understanding of the role of viscosity, as well as to exploit it as a mechanism to damp oscillations at high spatial frequency and produce a stable scheme, we make use of what is called \keyword{artificial viscosity}. Since the leading unstable terms causing the oscillations are of the same form as a negative viscosity, we should be able to eliminate them with a large enough {\em positive} viscosity. 

We remember that the momentum equation with non-zero bulk viscosity $\zeta$ takes the form
\begin{equation}
\frac{\partial \left(\rho v\right)}{\partial t}  + \frac{\partial}{\partial x}\left(\rho v^2 \right) = -\frac{\partial P}{\partial x} + \zeta \rho \frac{\partial^2 v}{\partial x^2}.
\end{equation}
While in a physical medium the value of $\zeta$ is of course given by the material properties, we will treat it here as a parameter that we are free to adjust. In order to include it along with our finite-difference solver, we just have to understand how to express it as a finite difference. We remember that at second spatial order, the second derivative is given by
\begin{equation}
\left.\frac{\partial^2 v}{\partial x^2}\right|_i \simeq \frac{v_{i+1}-2v_i+v_{i-1}}{\Delta x^2}.
\end{equation}
All we need to do is thus to modify the right-hand-side function with the new term:

\begin{pythoncode}
def RHS( rho, rhou, K, gamma, zeta ):
    pressure = K * rho**gamma
    rhou2 = rhou**2 / rho
    u = rhou / rho
    
    drho = -0.5 * (np.roll(rhou,-1)-np.roll(rhou,+1))
    drhou = -0.5 * (np.roll(rhou2+pressure,-1)-np.roll(rhou2+pressure,+1))
    
    drhou = drhou + zeta * rho * (np.roll(u,1) - 2.0*u + np.roll(u,-1))/dx
    
    return (drho,drhou)
    
\end{pythoncode}

The results for $\zeta=5\times10^{-3}$ ($\approx \Delta x$) in code units can be seen in Figure~\ref{fig:planewave_FD_viscous}. The artificial viscosity has indeed completely removed the oscillatory features. Of course this comes at the price of smeared-out shocks, which haven been significantly broadened. This is exactly the effect of bulk viscosity. Note that the viscosity term imposes additional time-step constraints. With a proper Riemann-solver based method, we would be able to obtain a non-oscillatory solution with very sharp shocks -- the reason why such methods are generally preferred, as we will see next. {\em The main problem of finite difference schemes is however that they are not guaranteed to be conservative! So mass, momentum and energy will not remain constant in the system.} This problem can be completely circumvented in finite volume methods which we discuss next.

\begin{figure}
\begin{center}
\includegraphics[width=0.75\textwidth]{Chapter4/planewave_FD_viscous.pdf}
\end{center}
\caption{\label{fig:planewave_FD_viscous}Same problem and numerical integration scheme (finite difference) as in Figure~\ref{fig:planewave_FD} but with added artificial viscosity. This produces a stable scheme, but the solution is smoothed out.}
\end{figure}


\subsection{Godunov's method: Finite Volume and Riemann solvers}

Above, we tried to discretise the conservative form of the hydrodynamic equations:
\begin{eqnarray}
\frac{\partial \rho}{\partial t} &=& - \frac{\partial}{\partial x}\left( \rho v \right)  \\
\frac{\partial \left(\rho v\right)}{\partial t}  &=& - \frac{\partial}{\partial x}\left(\rho v^2 + P\right)
\end{eqnarray}
and ran into problems with our finite difference approach because the flow can develop discontinuities, at which we cannot easily evaluate derivatives. One solution was to smooth out discontinuities with artificial viscosity, but another solution can be found by focusing directly on weak solutions of the equations. If we integrate these equations over a small volume (really a line segment between $x_{i-1/2}$ and $x_{i+1/2}$, as we are in 1D), we have
\begin{eqnarray}
\frac{\partial }{\partial t} \int_{x_{i-1/2}}^{x_{i+1/2}} \rho {\rm d}x&=& - \int_{x_{i-1/2}}^{x_{i+1/2}} \frac{\partial}{\partial x}\left( \rho v \right)  {\rm d}x = \left. (\rho v) \right|_{x_{i-1/2}} -   \left. (\rho v) \right|_{x_{i+1/2}} \nonumber\\
\frac{\partial }{\partial t} \int_{x_{i-1/2}}^{x_{i+1/2}} \left(\rho v\right) {\rm d}x  &=& - \int_{x_{i-1/2}}^{x_{i+1/2}} \frac{\partial}{\partial x}\left(\rho v^2 + P\right) {\rm d}x= \left. (\rho v^2+P) \right|_{x_{i-1/2}} -   \left. (\rho v^2+P) \right|_{x_{i+1/2}}  \nonumber.
\end{eqnarray}
Obviously, if we define a ``cell average'' 
\begin{equation}
\left< q \right>_i := \frac{1}{\Delta x} \int_{x_{i-1/2}}^{x_{i+1/2}} q\,{\rm d}x
\end{equation}
to be the volume average of a quantity $q$ (where $\Delta x = x_{i+1/2}-x_{i-1/2}$), this is simply
\begin{equation}
\frac{\partial \left<\mathbf{U}_i \right>}{\partial t} = -\frac{1}{\Delta x}\,\left( \mathbf{F}_{i+1/2} - \mathbf{F}_{i-1/2}\right)\label{eq:FV_update}
\end{equation}
where $\mathbf{U}=(\rho,\rho v)$ and $\mathbf{F}=(\rho v,\rho v^2+P)$ are the conserved quantities and the flux function respectively. 
%
\begin{figure}
\begin{center}
\includegraphics[width=0.95\textwidth]{Chapter4/finite_volume.pdf}
\end{center}
\caption{\label{fig:finite_volume}The finite volume method. (a) After integrating over the fluid equations, one can represent the conserved variables $\mathbf{U}$ in terms of an average over a cell volume $\left<\mathbf{U}\right>_i$ whose value can only change by computing the fluxes $\mathbf{F}_{i\pm 1/2}$ between adjacent cells. (b) The flux between cells can be obtained by considering the discontinuous Riemann problem at each cell interface. In its simplest flavour, we can reconstruct the left and right interface values $\mathbf{U}^{\rm L}$ and $\mathbf{U}^{\rm R}$ by assuming that the conserved variables are constant inside of each cell.}
\end{figure}
%
A visual depiction of this \keyword{finite volume approach} can be found in Fig.~\ref{fig:finite_volume}. {\em This means that the average value of the conserved quantity in a cell changes according to the net influx minus outflux into the cell.} The problem can thus be reduced to two steps: (1) reconstructing the left and right states at the interface between cells, and (2) computing the flux across the boundary. Since we are dealing with discontinuous solutions, a reasonable assumption is that $\mathbf{U}$ is constant inside of each cell. Then we have a discontinuity between $\mathbf{U}^{\rm L}$ and $\mathbf{U}^{\rm R}$ at each cell interface. Such a problem is a \keyword{Riemann problem} and can be solved either exactly, or approximately using so-called approximate Riemann solvers. A simple choice is the Haarten-Lax-Van~Leer (HLL) approximate \keyword{Riemann solver}, which states that for input states $\mathbf{U}^{\rm L}$ and $\mathbf{U}^{\rm R}$ and left and right fluxes $\mathbf{F}^{\rm L}=\mathbf{F}(\mathbf{U}^{\rm L})$ and $\mathbf{F}^{\rm R}=\mathbf{F}(\mathbf{U}^{\rm R})$, the correctly averaged flux is given by
\begin{equation}
\mathbf{F}(\mathbf{U}^{\rm L},\mathbf{U}^{\rm R}) = \frac{1}{2}\left(\mathbf{F}^{\rm L} + \mathbf{F}^{\rm R}\right) - \frac{S^\ast}{2}\left( \mathbf{U}^{\rm R} - \mathbf{U}^{\rm L}\right),
\end{equation}
where
\begin{equation}
S^\ast = \max\left( |v^L| + c^L,\,|v^R|+c^R \right)
\end{equation}
is the maximum signal speed, $c^L$ and $c^R$ the left and right sound speeds, respectively. We can thus use eq.~(\ref{eq:FV_update}) to obtain a full expression for an update
\begin{equation}
\left<\mathbf{U}_i \right>(t+\Delta t) = \left<\mathbf{U}_i \right>(t) - \frac{\Delta t}{\Delta x}  \left[ \mathbf{F}(\mathbf{U}^{\rm L}_{i+1},\mathbf{U}^{\rm R}_{i}) - \mathbf{F}(\mathbf{U}^{\rm L}_{i},\mathbf{U}^{\rm R}_{i-1}) \right].
\end{equation}


We can implement this easily in our previous scheme by just replacing the function {\sc RHS} to 
\begin{pythoncode}
# compute the right-hand side of the FD equations
def RHS( rho, rhou, K, gamma ):
    
    rhou2 = rhou**2 / rho
    u = rhou/rho
    
    pressure = K * rho**gamma
    cs = np.sqrt(gamma * pressure/rho)
    a = np.abs(u)+cs
    
    # Uleft
    rhoL = rho
    rhouL = rhou
    
    # Uright
    rhoR = np.roll(rho,-1)
    rhouR = np.roll(rhou,-1)
    
    # Fleft
    FrhoL = rhou
    FrhouL = rhou2 + pressure
    
    FrhoR = np.roll(rhou,-1)
    FrhouR = np.roll(rhou2+pressure,-1)
    
    # Sstar
    Sstar = np.maximum(a,np.roll(a,-1))
    
    # compute flux
    FHLLrho = 0.5*(FrhoL+FrhoR) - 0.5 * Sstar *(rhoR-rhoL)
    FHLLrhou = 0.5*(FrhouL+FrhouR) - 0.5 * Sstar *(rhouR-rhouL)
    
    # compute flux difference
    drho = -(FHLLrho - np.roll(FHLLrho,+1))
    drhou = -(FHLLrhou - np.roll(FHLLrhou,+1))
    
    return (drho,drhou)
\end{pythoncode}
This scheme can be shown to be stable if the \keyword{CFL-condition} (Courant-Friedrichs-Lewy) is fulfilled:
\begin{equation}
\Delta t < \frac{\Delta x}{2\max{(|v|+c)} }.
\end{equation}

The solution for our test problem is shown in Figure~\ref{fig:planewave_FV}, where we see that the result is qualitatively similar to the finite difference solution with artificial viscosity. The shock is not perfectly sharp, but the solution has no oscillations and the scheme is stable as long as the CFL condition is fulfilled.

Typical improvements to this method can be made by (1) increasing the order of the reconstruction step to either piecewise linear or piecewise parabolic reconstructions of the interface states (away from discontinuities), (2) increasing the order of the time integration. Multi-dimensional solvers can be constructed easily by directional operator splitting. Here one solves sequentially one-dimensional problems. In 3D, one would solve first along the x-direction, then along y, then z, followed by a step in the reverse order: first z, then y, then x. Also unsplit methods exist, but they are somewhat more involved in the multi-dimensional flux calculation. These methods are very robust and are among the workhorses in computational astrophysics from planet formation to cosmology.

\begin{figure}
\begin{center}
\includegraphics[width=0.75\textwidth]{Chapter4/planewave_FV.pdf}
\end{center}
\caption{\label{fig:planewave_FV}Numerical evolution using Godunov's method with an HLL approximate Riemann solver. The shock develops without oscillations and the scheme is stable. It is moderately diffusive itself (the shock is less sharp than in the finite difference method). }
\end{figure}


