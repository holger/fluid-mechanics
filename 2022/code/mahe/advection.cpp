#include <iostream>
#include <math.h>
#include <fstream>    // pour creer des fichiers externes


class Array
{
public:

  Array(int n){
    n_ = n;
    dataP = new float[n];
  }

  float read(int ix){
     if(ix < 0 || ix>= n_){
      std::cout << "Error in Array::write:: = " << ix << "too large too small with respect to n_=" << n_ << std::endl;
      exit(1);
    }
    return dataP[ix];
  }

  float& write(int ix){
    if(ix < 0 || ix>= n_){
      std::cout << "Error in Array::write:: = " << ix << "too large too small with respect to n_=" << n_ << std::endl;
      exit(1);
    }
    return dataP[ix];
  }
 
  // operator overloading
  float operator[](int ix) const {
     if(ix < 0 || ix>= n_){
      std::cout << "Error in Array::write:: = " << ix << "too large too small with respect to n_=" << n_ << std::endl;
      exit(1);
    }
    return dataP[ix];
  }

  float& operator[](int ix){
    if(ix < 0 || ix>= n_){
      std::cout << "Error in Array::write:: = " << ix << "too large too small with respect to n_=" << n_ << std::endl;
      exit(1);
    }
    return dataP[ix];
  }
  //  array = u === array.operator=(u);
  void operator=(Array array)
  {
    if(n_ != array.n_) {
      std::cout << "ERROR in operator=";
      exit(1);
    }
    for(int ix=0; ix<n_; ix++)
      dataP[ix] = array.dataP[ix];
  }
  
  int  size() {
    return n_;
  }
private: 
  float* dataP;
  int n_ ;
};


  // =================
  // Initial condition
  // ================= 

int main(int argc, char** argv)
{
  
  double a = 1.;   // flow speed
  int n = 50;      // number of spatial steps
  double pi = 3.14159265;
  //double t;
  double xmax = 2;
  double xmin = -2;
  double L = xmax-xmin;  //length of the wave ?
  double Amp = 1.;  //amplitude of the wave

  
  // float* u0 = new float[n];

  Array u0(n);
  Array u1(n);
  
  for(int ix=0; ix<n; ix++) {
    
    double dx = L/n;        // number of x steps
    double lambda = L-2*dx;   
    double dt = dx;
    
    u0[ix] = Amp*sin(2*pi*dx*ix/lambda);
    
    std::cout << "u0[" << ix << "]= " << u0[ix] << std::endl;



  // =================
  // Scheme
  // =================

      for(int ix=1; ix < n-1; ++ix)  {
        u1[ix] = u0[ix] - (a*(dt/dx))*(u0[ix] - u0[ix - 1]);    //a positif

	// std::cout << "oui" << std::endl;
    }
    
  //===================
  //Boundary condition
  //===================

     u1[0] = u1[n-2];
     u1[n-1] = u1[1];

 

  // =================
  // Initial condition
  // ================= 
    
    std::cout << "u1[" << ix << "]= " << u1[ix] << std::endl;
    std::cout << " " << std::endl;


    

  // =================
  // Writting files
  // =================
    
    std::ofstream file("u0.txt");

    file << "#title = sine function scheme" << std::endl;
    file << "#dim = 1" << std::endl;
    file << "#xmin = " << xmin << std::endl;
    file << "#xmax = " << xmax << std::endl;
    file << "#For n = " << n << std::endl;
    file << "  " << std::endl;
    
    for(int ix=0; ix<n; ix++)
      file << ix*dx  << "  " << u0[ix] << "  " << std::endl;

    std::ofstream file2("u1.txt");
    for(int ix=0; ix<n; ix++)
      file2 << ix*dx  << "  " << u1[ix] << "  " << std::endl;

    
  }

}


//gnuplot

// plot "data.txt" with linespoints, "data..." .... pour un deuxieme
//plot "data.txt" with linespoints title 'u0[x]', "data2.txt" with linespoints title 'u1[x]'
