#include <iostream> //header file
#include <stdio.h>
#include <math.h>
#include <fstream>
#include <cmath>
#include <algorithm>

class Array
{
public:

  Array(int n){
    n_ = n;
    dataP = new float[n];
  }

  float read(int i){
     if(i < 0 || i>= n_){
      std::cout << "Error in Array::write:: = " << i << "too large too small with respect to n_=" << n_ << std::endl;
      exit(1);
    }
    return dataP[i];
  }

  float& write(int i){
    if(i < 0 || i>= n_){
      std::cout << "Error in Array::write:: = " << i << "too large too small with respect to n_=" << n_ << std::endl;
      exit(1);
    }
    return dataP[i];
  }
 
  // operator overloading
  float operator[](int i) const {
     if(i < 0 || i>= n_){
      std::cout << "Error in Array::write:: = " << i << "too large too small with respect to n_=" << n_ << std::endl;
      exit(1);
    }
    return dataP[i];
  }

  float& operator[](int i){
    if(i < 0 || i>= n_){
      std::cout << "Error in Array::write:: = " << i << "too large too small with respect to n_=" << n_ << std::endl;
      exit(1);
    }
    return dataP[i];
  }
  //  array = u === array.operator=(u);
  void operator=(Array array)
  {
    if(n_ != array.n_) {
      std::cout << "ERROR in operator=";
      exit(1);
    }
    for(int i=0; i<n_; i++)
      dataP[i] = array.dataP[i];
  }
  
  int  size() {
    return n_;
  }
private: 
  float* dataP;
  int n_ ;
};

int main(int argc, char** argv)
{

 int n = 50;
 //int nx = 20;
 int nt = 5;
 float* arrayP = new float[n];

float* uP = new float[n];

 for(int i = 1; i<n-1; i++){
   uP[i] = i+1;}
 
 // float xmax = 4 * M_PI;
 //float xmin = 0.;
 float lambda = 2*M_PI;

 //double length = (xmax - xmin);
 double dx = lambda/(n-2);
 double a = 1.8;
 double dt = 0.5*dx;
 double A = 1;
 //double L = length - 2*dx;

 //Objected oriented way

 Array u0(n);
 Array array(n);
 Array u1(n);
 Array F0(n);
 Array Fm(n);
 Array Fp(n);
 Array max_m(n);
 Array max_p(n);

 for(int i=0; i<n  ; i++){
   u0[i] = A*sin(2*M_PI*i*dx/lambda);
 }

 std::ofstream in("Initialisation_LF.txt");
   for(int i=0; i<array.size(); i++){
     in << i << "\t" << u0[i]  << std::endl;
   }
 
   //for(int i=0; i<n-1; i++)
   // u0[i] = i*1.1;
 

 for(int t = 0; t<nt ; t++){
   for(int i = 1; i<array.size()-1 ; i++){
     max_m[i] =  std::max(fabs(u0[i-1]),fabs(u0[i]));
     max_p[i] =  std::max(fabs(u0[i]),fabs(u0[i+1]));
     Fm[i] = 0.5*( (u0[i-1]*u0[i-1])/2 + (u0[i]*u0[i])/2) - 0.5*(max_m[i])*(u0[i]-u0[i-1]);
     Fp[i] = 0.5*( (u0[i]*u0[i])/2 + (u0[i+1]*u0[i+1])/2) - 0.5*(max_p[i])*(u0[i+1]-u0[i]);
     
     u1[i] = u0[i] +  (Fm[i] - Fp[i])*(dt/dx);
   }
   u1[0]=u1[n-2];
   u1[n-1]=u1[1];

   u0=u1;
 }

 std::ofstream out("Numerical_scheme_LF.txt");
 for(int i=1; i<n-1; i++){
    out << i << "\t" << u1[i] << std::endl;
 }
 
 std::cout << "size of array = " << u0.size() << std::endl ; 
}
