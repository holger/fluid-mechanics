#include <iostream> // header file
#include <cmath>
#include <fstream>
class Array
{
public:

  // constructor
  Array(int n) {
    n_ = n;
    dataP_ = new float[n];
  }

  // operator overloading
  float operator[](int i) const {
    if(i < 0 || i >= n_) {
      std::cout << "ERROR in Array::write: i=" << i << " too large too small with respect to n_=" << n_ << std::endl;
      exit(1);
    }
    return dataP_[i];
  }

  float& operator[](int i) {
    if(i < 0 || i >= n_) {
      std::cout << "ERROR in Array::write: i=" << i << " too large too small with respect to n_=" << n_ << std::endl;
      exit(1);
    }
    return dataP_[i];
  }
  void operator = (Array array){
    if(n_ != array.n_){
      std::cout << "ERROR in operator";
      exit(1);
    }
    for(int i=0; i<n_; i++)
      dataP_[i]=array.dataP_[i];
  }

  int size() {
    return n_;
  }
  
private:
  int n_;
  float* dataP_;
};

int main(int argc, char** argv)
{ // variable:
  int n = 51;                   // number of points
  double dx = 2*3.14159/(n-1);  // space lenght
  double dt = 0.009;            // interval of time
  int nt = 50;                  // number of time step
  int snapshot = 5;
  // Creation of array
  Array u(n);
  Array ue(n);

  // initial speed
  for(int i=0; i<n; i++)
    u[i] = sin(i*dx);
  
  double B=dt/dx;
  // creation of data
  std::ofstream out("data_bur0.txt");
  for(int i=0; i<n; i++)
    out << i << "\t" << u[i] << std::endl;

  // time loop
  for(int i=1; i<nt; i++){
    int it=i;
  
    for(int j=1; j<u.size()-1; j++){
      
      ue[j]=u[j]+B*((u[j-1]*u[j-1]-u[j+1]*u[j+1])/4 - std::max(u[j-1],u[j])*(u[j]-u[j-1]) + std::max(u[j+1],u[j])*(u[j]-u[j+1]));

      // edge condition
      ue[0]=ue[n-2];
      ue[n-1]=ue[1];
    }  
    u = ue;
    // creation of file data for each time step every snapshot
    if(it % snapshot == 0){
      
      std::string filename;
      filename = "data_bur";
      
      std::string newFilename = filename + std::to_string(it+5);
      newFilename += ".txt";
      
      std::ofstream out(newFilename);
      for(int j=0; j<n; j++)
	out << j << "\t" << u[j] << std::endl;
    }
  }  
}
