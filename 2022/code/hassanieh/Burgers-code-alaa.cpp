#include <iostream> // header file
#include <stdio.h>      /* printf */
#include <math.h>
#include <fstream>
#include <algorithm>

class Array
{
public:

  // constructor
  Array(int n) {
    n_ = n;
    dataP_ = new float[n];
  }


  // operator overloading
  float operator[](int i) const {
    if(i < 0 || i >= n_) {
      std::cout << "ERROR in Array::write: i=" << i << " too large too small with respect to n_=" << n_ << std::endl;
      exit(1);
    }
    return dataP_[i];
  }

  float& operator[](int i) {
    if(i < 0 || i >= n_) {
      std::cout << "ERROR in Array::write: i=" << i << " too large too small with respect to n_=" << n_ << std::endl;
      exit(1);
    }
    return dataP_[i];
  }

//  array = u === array.operator=(u);
  void operator=(Array array)
  {
    if(n_ != array.n_) {
      std::cout << "ERROR in operator=";
      exit(1);
    }
    for(int i=0; i<n_; i++)
      dataP_[i] = array.dataP_[i];
  }

  int size() {
    return n_;
  }
  
private:
  int n_;
  float* dataP_;
};


double Pi= 3.14159265;
void boundary_conditions(Array u) {
  int  n = 20;
  u[0] = u[n-2];
  u[n-1]= u[1];
  
}


int main(int argc, char** argv)
{
  std::cout << "Hello world" << std::endl;

  int n = 20;

  // object oriented way:
  Array u(n);
  Array u1(n);
  Array F(n);
  Array F1(n);
  Array F2(n);

  double dx = 2*Pi/(n);
  //double alpha = 0.5;
  //alpha = a dt/dx ; we can use alpha or a dt/dx
  //double dt = alpha * dx / a ;
  double dt=1;
  for(int i=0; i<n-1; i++)
    u[i]=sin(i*dx);
  //boundary_conditions(u);


  //u1[i] is u^n+1[i]

  for(int i=0; i<n-1; i++)
    F[i]=(u[i]*u[i])*0.5;

  for(int i=1; i<n-1; i++)//we exclude the cell that we dont need but it should be in the boundary condition, so we start at 1 to not use it so we go to n-1 because if we had i+1 it is better to go to n-1
    F[i-1]=(u[i-1]*u[i-1])*0.5;
 
  for(int i=0; i<n-1; i++)
    F[i+1]=(u[i+1]*u[i+1])*0.5;
   
  for (int j=0; j<n-1; j++)
    for(int i=1; i<n-1; i++)
      F1[j]=(F[i-1]+F[i])-0.5*(std::max(std::fabs(u[i]),std::fabs(u[i-1])))*(u[i]-u[i-1]);//flux at f(i-1/2) we decompose a cell into several cells and we calculate the flux, to avoid having a non integer number inside F[] i created a new variable j 

  for (int d=0; d<n-1; d++)
    for(int i=1; i<n-1; i++){
      F2[d]=(F[i]+F[i+1])-0.5*(std::max(std::fabs(u[i+1]),std::fabs(u[i])))*(u[i+1]-u[i]);//flux at f(i+1/2)
  }
 
  std::string filename;
  filename = "hhf";
  filename += "-";
  for(int it=0; it<n-1; ++it){
    for(int i=1; i<n-1; ++i){
      u1[i]= u[i] + (dt/(dx))*(F1[i]- F2[i]);
    }
    boundary_conditions(u1);
    std::string newFilename = filename + std::to_string(it)+".txt";
    std::ofstream out(newFilename);
    for (int ix=0; ix<n-1; ix++){
    out << ix << "\t" << u1[ix] << std::endl;
    }
    u=u1;
  }
}
//the green region is copied from the blue region because the blue region is what we have and the green region is what we are imposing

