//useful package
#include <iostream>
#include <cmath>
#include <fstream>


//creation of functions and variables that will be call during the code
class Array
{
public:
  // member function
  Array(int n) {
    data = new float[n]; // allocation on the heap
    n_ = n;
  }

  // reading operator
  float operator[](int i) const {
    return data[i];
  }

  // writing operator
  float& operator[](int i) {
    if(i>=n_) {
      std::cout << "ERROR\n";
      abort();
    }
    
    return data[i];
  }

  float read(int i) {
    return data[i];
  }

  float& write(int i) {
    if(i>=n_) {
      std::cout << "ERROR\n";
      abort();
    }
    return data[i];
  }

private:
  // member
  float* data;
  int n_;
};


//main part of the code

int main()
{

  
   //test hello world (not neccesary in fact)  
   std::cout << "Hello world" << std::endl;

  // Object oriented way

  int n = 200;       // Total space step
  int nt = 10;       // Total time step 1
  int nt2 = 20;      // total time step 2
  int nt3 = 30;      // total time step 3
  // int nt4 = 40;      // total time step 4
  
  double a = 0.5;    // Velocity
  
  double pi = 3.14159265;

  Array u(n);     // Initial vector  
  Array pos(n);     // Position Vector 
  Array unew(n);    // New vector

  double L = 1;              // Domain length
  double dx = L/(n-1);       // Space step
  double dt = a*dx;          // Time step
  double A = 1.;             // Wave amplitude
  double lambda = (n-2)*dx;  // Wavelength

  
  // Initialisation of the array
  for(int i=0;i<n;i++) {
    pos[i] = i*dx;
    u[i] = A*sin(2*pi*i*dx/lambda); // Initial condition
  }

  // Save the initial condition in a file
  std::ofstream out("data.txt");
  for(int i=0;i<n;i++) {
    out << pos[i] << "\t" << u[i] << std::endl;
  }
  out.close();


  for (int i=0; i<nt; i++){     //Time loop
    for (int j=1; j<n-1; j++){  // Space loop

       unew[j] = 0.5* (u[j+1] + u[j-1] ) - a* 0.5 * dt/dx * (u[j+1]*u[j+1] - u[j-1]*u[j-1]);  //numerical scheme : stabilisation for lax-friedrichs

    }

    // Initial conditions 
    unew[0] = unew[n-2];
    unew[n-1] = unew[1];
    u = unew; 

  }

  // Save the final result in a second file for t=10
  std::ofstream out2("t10.txt");
  for(int i=0;i<n;i++) {
    out2 << pos[i] << "\t" << u[i] << std::endl;
  }


  //============================================== t2

   for (int i=0; i<nt2; i++){     //Time loop
    for (int j=1; j<n-1; j++){  // Space loop

      unew[j] = 0.5* (u[j+1] + u[j-1] ) - a* 0.5 * dt/dx * (u[j+1]*u[j+1] - u[j-1]*u[j-1]);  //numerical scheme : stabilisation for lax-friedrichs
      

    }

    // Initial conditions 
    unew[0] = unew[n-2];
    unew[n-1] = unew[1];
    u = unew; 

  }

  // Save the final result out3.close();
  std::ofstream out3("t20.txt");
  for(int i=0;i<n;i++) {
    out3 << pos[i] << "\t" << u[i] << std::endl;
  }
  //============================================= t3
   for (int i=0; i<nt3; i++){     //Time loop
    for (int j=1; j<n-1; j++){  // Space loop

        unew[j] = 0.5* (u[j+1] + u[j-1] ) - a* 0.5 * dt/dx * (u[j+1]*u[j+1] - u[j-1]*u[j-1]);  //numerical scheme : stabilisation for lax-friedrichs

    }

    // Initial conditions 
    unew[0] = unew[n-2];
    unew[n-1] = unew[1];
    u = unew; 

  }

  // Save the final result for t=30
  std::ofstream out4("t30.txt");
  for(int i=0;i<n;i++) {
    out4 << pos[i] << "\t" << u[i] << std::endl;
  }

  out4.close();

  //test that's it! (not neccesary in fact) 
  std::cout << "That's it !\n";   
} 


//commande unix pour compile le code dans le terminal
//g++ -o burgergood burgergood.cpp
// ./burgergood


// commande pour visualiser les courbes des sinus dans le terminal:
// gnuplot
// plot "data.txt", "t10.txt","t20.txt","t30.txt"  with linespoints
