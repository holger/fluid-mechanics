//useful package for the code
#include <iostream>
#include <cmath>
#include <fstream>


//creation of functions and variables that will be call during the code
class Array
{
public:
  // member function
  Array(int n) {
    data = new float[n]; // allocation on the heap
    n_ = n;//if too much terms, error message
  }

  // reading operator
  float operator[](int i) const {
    return data[i];
  }

  // writing operator
  float& operator[](int i) {
    if(i>=n_) {
      std::cout << "ERROR\n";
      abort();
    }
    
    return data[i];
  }

  float read(int i) {
    return data[i];
  }

  float& write(int i) {
    if(i>=n_) {
      std::cout << "ERROR\n";
      abort();
    }
    return data[i];
  }

private:
  // member
  float* data;
  int n_;
};


//main part of the code

int main()
{

  //test hello world (not neccesary in fact)  
  std::cout << "Hello world" << std::endl;

  // Object orianted way

  int n = 200;       // Total spacial step
  int nt = 10;      // Total time step
  double a = 0.5;    // Advection speed

  double pi = 3.14159265;


  Array u(n);       // Initial u  
  Array pos(n);     // Position Vector 
  Array unew(n);    // New u


  double L = 1;              // Domain lenght
  double dx = L/(n-1);       // Space step
  double dt = a*dx;          // Time step
  double A = 1.;             // Wave amplitude
  double lambda = (n-2)*dx;  // Wavelength

  
  // Initialisation of the array
  for(int i=0;i<n;i++) {
    pos[i] = i*dx;
    u[i] = A*sin(2*pi*i*dx/lambda); // Initial condition
  }

  // Save the initial condition in a file
  std::ofstream out("data.txt");
  for(int i=0;i<n;i++) {
    out << pos[i] << "\t" << u[i] << std::endl;
  }
  out.close();


  for (int i=0; i<nt; i++){     //Time loop
    for (int j=1; j<n-1; j++){  //Space loop

      unew[j] = u[j]  +  a*(dt/dx) * (u[j-1] - u[j]); //numerical scheme : finite volumes method for the flow

    }

    // Initial conditions 
    unew[0] = unew[n-2];
    unew[n-1] = unew[1];
    u = unew; 
  
  }

  // Save the final result in a second file
  std::ofstream out2("data2.txt");
  for(int i=0;i<n;i++) {
    out2 << pos[i] << "\t" << u[i] << std::endl;
  }
  out2.close();

  //test that's it! (not neccesary in fact) 
  std::cout << "That's it !\n";

    
}


//commande unix pour compile le code dans le terminal
//g++ -o codegood codegood.cpp
// ./codegood


// commande pour visualiser les courbes des sinus dans le terminal:
// gnuplot
// plot "data.txt","data2.txt" with linespoints
