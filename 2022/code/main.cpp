#include <iostream> // header file
#include <cmath>
#include <fstream>

class Array
{
public:

  // constructor
  Array(int n) {
    n_ = n;
    dataP_ = new float[n];
  }

  // operator overloading
  float operator[](int i) const {
    if(i < 0 || i >= n_) {
      std::cout << "ERROR in Array::write: i=" << i << " too large too small with respect to n_=" << n_ << std::endl;
      exit(1);
    }
    return dataP_[i];
  }

  float& operator[](int i) {
    if(i < 0 || i >= n_) {
      std::cout << "ERROR in Array::write: i=" << i << " too large too small with respect to n_=" << n_ << std::endl;
      exit(1);
    }
    return dataP_[i];
  }

  //  array = u === array.operator=(u);
  void operator=(Array array)
  {
    if(n_ != array.n_) {
      std::cout << "ERROR in operator=";
      exit(1);
    }
    for(int i=0; i<n_; i++)
      dataP_[i] = array.dataP_[i];
  }

  int size() {
    return n_;
  }
  
private:
  int n_;
  float* dataP_;
};

int main(int argc, char** argv)
{
  std::cout << "Hello world" << std::endl;

  // object oriented way:

  int n = 20;
  double dx = 1;
  
  Array u(n);

  for(int i=0; i<n; i++)
    u[i] = sin(i);
  
  Array array(n);

  for(int i=0; i<array.size()-1; i++)
    array[i] = (u[i+1] - u[i])/dx;

  // for(int i=0; i<n; i++)
  //   std::cout << "array[" << i << "]= " << u[i] << std::endl;
    
  //(read(i+1) - read(i))/dx;



  std::ofstream out("data.txt");
  for(int i=0; i<n; i++)
    out << i << "\t" << u[i] << std::endl;

  array = u;


  std::cout << "size of array = " << u.size() << std::endl;
  for(int i=0; i<n; i++)
    std::cout << i << "  " << u[i] << "\t" << array[i] << std::endl;
  
  for(int i=0; i<n; i++)
    u[i] *= 2;

  std::cout << "size of array = " << u.size() << std::endl;
  for(int i=0; i<n; i++)
    std::cout << i << "  " << u[i] << "\t" << array[i] << std::endl;
}

// visualisation:
// gnuplot
// plot "data.txt" with linespoints
