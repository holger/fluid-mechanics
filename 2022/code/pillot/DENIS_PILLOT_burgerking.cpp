#include <iostream> // header file
#include <cmath>
#include <fstream>

class Array
{
public:

  // constructor
  Array(int n) {
    n_ = n;
    dataP_ = new float[n];
  }

  // operator overloading
  float operator[](int i) const {
    if(i < 0 || i >= n_) {
      std::cout << "ERROR in Array::write: i = " << i << " too large too small with respect to n_=" << n_ << std::endl;
      exit(1);
    }
    return dataP_[i];
  }

  float& operator[](int i) {
    if(i < 0 || i >= n_) {
      std::cout << "ERROR in Array::write: i=" << i << " too large too small with respect to n_=" << n_ << std::endl;
      exit(1);
    }
    return dataP_[i];
  }

  //  array = u === array.operator=(u);
  void operator=(Array array)
  {
    if(n_ != array.n_) {
      std::cout << "ERROR in operator=";
      exit(1);
    }
    for(int i=0; i<n_; i++)
      dataP_[i] = array.dataP_[i];
  }

  int size() {
    return n_;
  }
  
private:
  int n_;
  float* dataP_;
};

int main(int argc, char** argv)
{
  std::cout << "Hello world" << std::endl;

  // object oriented way:

  int n = 256;

  double a = 1;
  
  double tmax = 1000;
  double t0 = 0;
  double sig = 1;
  double pi = M_PI;
  double lmd = 2*pi;

  double dx = lmd/(n-1);
  double dt = 0.5* dx;

  double b = dt/dx;
  
  Array u(n);

  Array x(n);

  for(int i=0;i<n;i++){
    x[i] = x[0] + i*dx;

  }

  double xc = (x[n-1]-x[0])/2;

  


  for(int i=0; i<n; i++){
     u[i] = sin(2*pi*x[i]/lmd)+0.5;
    // u[i] = exp(- (x[i]-xc)*(x[i]-xc)/(sig*sig));
  }

  
  Array array(n);

  Array t(tmax);

  Array f(n);

  Array f2(n);

  std::string filename;
  filename = "burger";
  filename += "-";

  int snapshot = 1;

   for(int k=0;k<tmax;k++){


     for(int i=0;i<n;i++){
        f[i] = u[i]*u[i]/2;
       //  f[i] = u[i]*a;
     }

     for(int i=1;i<n;i++){
       f2[i] = (f[i]+f[i-1])/2 + std::max(fabs(u[i-1]),fabs(u[i]))*(u[i-1] - u[i])/2;    //BURGER
	// f2[i] = (f[i]+f[i-1])/2 + a*(u[i-1] - u[i])/2;     //Advection

     }



   for(int i=1; i<array.size()-1; i++){
      array[i] = u[i] - b*(f2[i+1] - f2[i]);
     //  array[i] = u[i] - a*dt/dx *(u[i] - u[i-1]);
     }

  // u[0] = u[n-2];
  // u[n-1] = u[1];
    array[0] = array[n-2];   // advection
    array[n-1] = array[1];

   // array[0] = array[n-3];
   // array[n-2] = array[1];        // BURGER
  
    
  t[k] = t0 + k*dt;

  if(k % snapshot == 0){

  std::string newfilename = filename + std::to_string(k) + ".txt";
  std::ofstream out(newfilename);


    for(int i=0; i<n; i++)
       out <<  x[i] << "\t" << array[i] << "\t" << t[k] << std::endl;

  }
  u = array;


  
   }
}

// visualisation:
// gnuplot
// plot "data.txt" with linespoints
