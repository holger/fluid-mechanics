#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 11 17:05:37 2022

@author: lmarques
"""

from numpy import loadtxt
from matplotlib.pyplot import plot, xlabel, ylabel,title, show

direc = "/home/lmarques/Downloads/Fundamental Courses/Semester 2/Fluid Mechanics/Coding/Burgers/"
filename = "burgers_simu_"

nt = 50
tmax = 1
tmin = 0
dt = (tmax - tmin)/nt
# dt = 0.02
# nt = int((tmax - tmin)/dt)

for i in range(nt):
    u = loadtxt(direc+filename+str(i)+".txt")
    plot(u[:,0], u[:,1], 'o')
    xlabel("x")
    ylabel('u')
    title("Velocity profile at time step "+str(i*dt))
    show()