#include <iostream> // header file
#include <cmath>
#include <fstream>

class Array
{
public:

  // constructor
  Array(int n) {
    n_ = n;
    dataP_ = new float[n];
  }

  // operator overloading
  //float operator[](int i) const {
  //if(i < 0 || i >= n_) {out.write(&u[0],u.size())
  //std::cout << "ERROR in Array::write: i=" << i << " too large too small with respect to n_=" << n_ << std::endl;
  //exit(1);
  //}
  //return dataP_[i];
  //}

    float& operator[](int i) {
    if(i < 0 || i >= n_) {
      std::cout << "ERROR in Array::write: i=" << i << " not in range n_=" << n_ << std::endl;
      exit(1);
    }
    return dataP_[i];
    }

    //  array = u === array.operator=(u);
  void operator=(Array array)
  {
    if(n_ != array.n_) {
      std::cout << "ERROR : size of operators don't match";
      exit(1);
    }
    for(int i=0; i<n_; i++)
      dataP_[i] = array.dataP_[i];
  }

  int size() {
    return n_;
  }

private:
  int n_;
  float* dataP_;
};

double flux(float  u){
  return u*u/2;
}

int main(int argc, char** argv)
{
  double xmin = 0;
  double xmax = 2*3.14;
  int nx = 50;
  double dx = (xmax - xmin)/nx;
  double a = 0.5;
  int nt = 50;
  double tmax = 1;
  double tmin = 0;
  double dt = a*dx; //limit of CFL criterion adt/dx = 1
  
  
  //for precaution
  if(a*dt/dx > 1) {
    std::cout << "ERROR : scheme won't be stable because CFL criterion not fulfilled" << std::endl;
    exit(1);
  }
  
  // Initialisation

  Array u0(nx+2);
  std::ofstream out("burgers_simu_0.txt");
  for(int i=0; i<nx+2; i++) {
    u0[i] = sin(i*dx);
    out << i << "\t" << u0[i] << std::endl;
  }
  

  // Numerical scheme

  Array u1(nx+2); //2 ghost cells are added, where the values will be imposed and not computed (cannot be computed by the numerical scheme chosen)


  std::string filename;
    filename = "burgers_simu";
    filename += "_";
    
  for(int it=1; it<nt; it++) {
    for(int ix=1; ix<nx+1; ix++) {
      u1[ix] = u0[ix] + (dt/dx) * 1/2 * (flux(u0[ix-1])-flux(u0[ix+1]) - fmax(fabs(u0[ix]),fabs(u0[ix-1]))*(u0[ix]-u0[ix-1]) + fmax(fabs(u0[ix+1]),fabs(u0[ix]))*(u0[ix+1]-u0[ix]));
    }
    //Imposing boundary conditions on the two outer ghost cells
    u1[0] = u1[nx];
    u1[nx+1] = u1[1];

    std::string newFilename = filename + std::to_string(it) + ".txt";
    std::ofstream out(newFilename);
    for(int i=0; i<nx+2; i++) {
      out << i << "\t" << u1[i] << std::endl;
    }

    u0 = u1;

  }

  
}
