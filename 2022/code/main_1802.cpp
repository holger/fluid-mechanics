#include <iostream> // header file

class Array
{
public:

  // constructor
  Array(int n) {
    n_ = n;
    dataP_ = new float[n];
  }

  float read(int i) {
    return dataP_[i];
  }

  float& write(int i) {
    return dataP_[i];
  }

private:
  int n_;
  float* dataP_;
};

int main(int argc, char** argv)
{
  std::cout << "Hello world" << std::endl;

  // allocate a float:
  float* testP = new float;

  std::cout << "testP = " << testP << std::endl;

  *testP = 5;

  std::cout << "*testP = " << *testP << std::endl;

  float test = 5;

  std::cout << "test = " << test << std::endl;
  std::cout << "&test = " << &test << std::endl;

  int n = 10;
  float* arrayP = new float[n];

  for(int i=0; i<n; i++)
    arrayP[i] = i+1;

  for(int i=0; i<n; i++)
    std::cout << "arrayP[" << i << "]=" << arrayP[i] << std::endl;

  std::cout << "arrayP = " << arrayP << std::endl;
  std::cout << "*arrayP = " << *arrayP << std::endl;
  std::cout << "arrayP[0] = " << arrayP[0] << std::endl;
  std::cout << "&arrayP = " << &arrayP << std::endl;

  float* uP = new float[n];
  for(int i=0; i<n; i++)
    uP[i] = i+1;

  double dx = 1;
  
  for(int i=0; i<n-1; i++)
    arrayP[i] = (uP[i+1] - uP[i])/dx;

  // object oriented way:

  Array u(n);

  for(int i=0; i<n; i++)
    u.write(i+1) = i*1.1;

  for(int i=0; i<n; i++)
    std::cout << "u(" << i << ")= " << u.read(i) << std::endl;
    
  //(read(i+1) - read(i))/dx;

  u.n_ = 5;
}
