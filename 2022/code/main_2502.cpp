#include <iostream> // header file

class Array
{
public:

  // constructor
  Array(int n) {
    n_ = n;
    dataP_ = new float[n];
  }

  float read(int i) {
    if(i < 0 || i >= n_) {
      std::cout << "ERROR in Array::write: i=" << i << " too large too small with respect to n_=" << n_ << std::endl;
      exit(1);
    }
    return dataP_[i];
  }

  float& write(int i) {
    if(i < 0 || i >= n_) {
      std::cout << "ERROR in Array::write: i=" << i << " too large too small with respect to n_=" << n_ << std::endl;
      exit(1);
    }
    return dataP_[i];
  }

  // operator overloading
  float operator[](int i) const {
    if(i < 0 || i >= n_) {
      std::cout << "ERROR in Array::write: i=" << i << " too large too small with respect to n_=" << n_ << std::endl;
      exit(1);
    }
    return dataP_[i];
  }

  float& operator[](int i) {
    if(i < 0 || i >= n_) {
      std::cout << "ERROR in Array::write: i=" << i << " too large too small with respect to n_=" << n_ << std::endl;
      exit(1);
    }
    return dataP_[i];
  }


  int size() {
    return n_;
  }
  
private:
  int n_;
  float* dataP_;
};

int main(int argc, char** argv)
{
  std::cout << "Hello world" << std::endl;

  // allocate a float:
  float* testP = new float;

  std::cout << "testP = " << testP << std::endl;

  *testP = 5;

  std::cout << "*testP = " << *testP << std::endl;

  float test = 5;

  std::cout << "test = " << test << std::endl;
  std::cout << "&test = " << &test << std::endl;

  int n = 10;
  float* arrayP = new float[n];

  for(int i=0; i<n; i++)
    arrayP[i] = i+1;

  for(int i=0; i<n; i++)
    std::cout << "arrayP[" << i << "]=" << arrayP[i] << std::endl;

  std::cout << "arrayP = " << arrayP << std::endl;
  std::cout << "*arrayP = " << *arrayP << std::endl;
  std::cout << "arrayP[0] = " << arrayP[0] << std::endl;
  std::cout << "&arrayP = " << &arrayP << std::endl;

  float* uP = new float[n];
  for(int i=0; i<n; i++)
    uP[i] = i+1;

  double dx = 1;
  
  for(int i=0; i<n-1; i++)
    arrayP[i] = (uP[i+1] - uP[i])/dx;

  // object oriented way:

  Array u(n);

  for(int i=0; i<n; i++)
    u.write(i) = i*1.1;
  
  Array array(n);

  for(int i=0; i<array.size()-1; i++)
    array.write(i) = (u.read(i+1) - u.read(i))/dx;

  for(int i=0; i<array.size()-1; i++)
    array(i) = (u[i+1] - u[i])/dx;

  for(int i=0; i<n; i++)
    std::cout << "array[" << i << "]= " << array[i] << std::endl;
    
  //(read(i+1) - read(i))/dx;

  std::cout << "size of array = " << u.size() << std::endl;
}
