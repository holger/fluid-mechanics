#include <iostream> // header file
#include <cmath>
#include <fstream>
#include <math.h>
#include <cstdlib>


//Burger 

//Lax-Friedrichs method




//##################------ARRAY------############################################################ 
class Array
{
public:

  // constructor
  Array(int n) {
    n_ = n;
    dataP_ = new float[n];
    }
  

  float read(int i) {
    if(i < 0 || i >= n_) {
      std::cout << "ERROR in Array::write: i=" << i << " too large too small with respect to n_=" << n_ << std::endl;
      exit(1);
    }
    return dataP_[i];
  }
  
  int size(){
    
    return n_;
  
  }

  float& write(int i) {
    if(i < 0 || i >= n_) {
      std::cout << "ERROR in Array::write: i=" << i << " too large too small with respect to n_=" << n_ << std::endl;
      exit(1);
    }
    return dataP_[i];
  }

  // operator overloading
  float operator[](int i) const {
    if(i < 0 || i >= n_) {
      std::cout << "ERROR in Array::write: i=" << i << " too large too small with respect to n_=" << n_ << std::endl;
      exit(1);
    }
    return dataP_[i];
  }

  float& operator[](int i) {
    if(i < 0 || i >= n_) {
      std::cout << "ERROR in Array::write: i=" << i << " too large too small with respect to n_=" << n_ << std::endl;
      exit(1);
    } //
    
        return dataP_[i];
  }

private:
  int n_;
  float* dataP_; 
};
//#############################################################################


//#####################------FUNCTIONS------###################################

// for the output into a txt file 



void write(std::ofstream& OUTPUT, Array x, Array y0, Array y1,int nx){
	
	for(int i=0; i<nx; i++){

	OUTPUT<< x[i]<<"  "<< y0[i] <<"  "<< y1[i] <<std::endl; //
	
	}
}



void boundaryConditions(Array u1, Array u0,float deltax ){ //
  int n = u1.size(); //
  
  u1[0] = u1[n-2]; // 
  
  
  u1[n-1] = u1[1]; // 

}


//############################################################################


int main(int argc, char** argv)
{	
	// Riemann Solves
	
  	float xmin = 0;
  	
	
	int nx=125; // number of point and dimension of array
	
	float A = 1.; // the amplitude of the initial profile 	
	
	float lambda = 2*M_PI;
	
	float  deltax =  lambda/(nx-2);
	
	Array x(nx);
  
	for(int i=0;i<nx;i++){
	
	x[i] = xmin + (i - 0.5) * deltax; // 


	};

	Array u0(nx); //
	
	// Profile initial t = 0
  	for(int i=0;i<nx;i++){
	
	u0[i] = A * sin((2*M_PI/lambda)*i*deltax);
	
	};
	
	float deltat = deltax;
	
	Array F(nx);
	
	Array u1(nx); //
	
	Array u2(nx);
	
	u2=u0;
	
	int n;
  	std::cout<<"how many time step do you want?"<<std::endl;
  	std::cin>> n;
	
	for(int j= 0; j<n;j++){
	for(int i=1; i<nx-1; i++){ 
	
	// Burger Code Lax Friedrich method
	
	F[i-1]= (1/4)*(u0[i-1]*u0[i-1] +u0[i]*u0[i])-std::abs(std::max(u0[i-1],u0[i]))*(u0[i]-u0[i-1]);
	
	F[i] = (1/4)*(u0[i]*u0[i] + u0[i+1]*u0[i+1])-std::abs(std::max(u0[i],u0[i+1]))*(u0[i+1]-u0[i]);//
	

	u1[i] = u0[i] + (deltat/deltax) * (F[i-1] - F[i]);

	
	}
	
	boundaryConditions(u1,u0,deltax);
	
	u0=u1;
	
	}
	
	std::string Output_Filename="OutPut_File_BURGER_"+std::to_string(n)+"Time_steps.txt";
	
	std::ofstream OUTPUT(Output_Filename);
	
	write(OUTPUT ,x ,u2 ,u1 ,nx);//
  
}
