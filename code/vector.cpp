// g++ -o vector vector.cpp
// ./vector

#include <iostream>
#include <cmath>
#include <fstream>

class Vector
{
public:
  // member function
  Vector(int n) {
    data = new double[n]; // allocation on the heap
    n_ = n;
  }

  // reading operator
  double operator[](int i) const {
    return data[i];
  }

  // writing operator
  double& operator[](int i) {
    if(i>=n_) {
      std::cout << "ERROR\n";
      abort();
    }
    
    return data[i];
  }

  Vector operator=(Vector vec) {
    for(int i=0; i<n_;i++)
      data[i] = vec.data[i];
    return *this;
  }
  
  double read(int i) {
    return data[i];
  }

  double& write(int i) {
    if(i>=n_) {
      std::cout << "ERROR\n";
      abort();
    }
    return data[i];
  }
  
  
private:
  // member
  double* data;
  int n_;
};

int main()
{
  std::cout << "Starting data test\n";

  // Imperative programming
  
  int N = 20;
  double* u0 = new double[N]; // allocation on the heap

  std::cout << "u0 = " << u0 << std::endl;
  for(int i=0;i<N;i++)
    u0[i] = 2*i;

  for(int i=0;i<N;i++)
    std::cout << i << "\t" << u0[i] << std::endl;

  std::cout << "*u0 = " << *u0 << std::endl;
  std::cout << "*(u0+1) = " << *(u0+1) << "\t" << u0[1] << std::endl;
  std::cout << "&u0 = " << &u0 << std::endl;

  //  u0[11] = 5; // unsafe: undefined behauviour !!!

  // Object-oriented programming

  Vector vec(N);
  vec.write(11) = 1.1;
  std::cout << vec.read(0) << std::endl;

  vec[11] = 1.1;  
  std::cout << vec[0] << std::endl;

  Vector pos(N);

  double L = 1; // type safe
  double dx = L/(N-1);
  for(int i=0;i<N;i++) {
    pos[i] = i*dx;
    vec[i] = sin(2*M_PI/L*(i*dx));
  }

  std::ofstream out("data.txt");
  for(int i=0;i<N;i++) {
    std::cout << pos[i] << "\t" << vec[i] << std::endl;
    out << pos[i] << "\t" << vec[i] << std::endl;
  }

  out.close();

  Vector vec_new(N);

  vec_new = vec;

  for(int i=0;i<N;i++) {
    std::cout << pos[i] << "\t" << vec_new[i] << std::endl;
  }


  vec_new[0] = -1;

  std::cout << "vec_new[0] = " << vec_new[0] << std::endl;
  std::cout << "vec[0] = " << vec[0] << std::endl;
} 
